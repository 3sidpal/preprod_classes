public with sharing class Utils {
  
  //Stage Name to showcase Received Column in OLI
public static String stageName='For_Receiving';
    /*
        Description : To fetch given field set of OLI
    */
    public static Schema.FieldSet fetchOLIFieldSet(String oliFldSetNm){
        return SObjectType.Order_Item__c.FieldSets.getMap().get(oliFldSetNm);
    }

    public static String fetchQuery(List<String> flds){
        String query = String.join(flds, ',');
        return query;
    }

    public static Map<String,String> fetchFldsMapFromFldSet(Schema.FieldSet fldSet){

		system.debug('================fldSet'+fldSet);
        Map<String,String> fldMap = new Map<String,String>();
        
        for(Schema.FieldSetMember f : fldSet.getFields()) {
            fldMap.put(f.getLabel(), f.getFieldPath());
        }
        
        return fldMap;
    }

    public static Schema.FieldSet fetchOppFieldSet(String oliFldSetNm){
    	system.debug('================oliFldSetNm'+oliFldSetNm);
      oliFldSetNm = (String.isNotBlank(oliFldSetNm)) ? oliFldSetNm.replace(' ', '_') : '';
      return SObjectType.Order__c.FieldSets.getMap().get(oliFldSetNm);
    }

    /*public static List<Schema.FieldSet> fetchFeildSets() {
      Map<String, Schema.FieldSet> FsMap = Schema.SObjectType.Order__c.fieldSets.getMap();
      return FsMap.values();
    }*/

    /**
  * <P> This method is used to Check the String is Null or empty</p>
  * @param1: String fieldName
  *
  * @return: returns Boolean
  */
  public static Boolean isStringNullOrEmpty(String strData) {

    if (strData == null || strData == '') {
      return true;
    } else {
      return false;
    }
  }    

    /**
    * <p> This method returns all fields of an Object, which can be used in a dynamic select query.
    The main purpose of this function is copying complete object in to another object (same type)</p>
    */
    public static String getQueryString(String obj,boolean allFields){
        SObjectType objToken = Schema.getGlobalDescribe().get(obj);
        DescribeSObjectResult objDef = objToken.getDescribe();
        Map<String, SObjectField> fields = objDef.fields.getMap();
        Set<String> fieldSet = fields.keySet();
        String queryStr='';
        for(String s:fieldSet)
        {
            SObjectField fieldToken = fields.get(s);
            DescribeFieldResult selectedField = fieldToken.getDescribe();

            if(allFields){
                queryStr = queryStr + selectedField.getName() + ',';

            }else if (selectedField.isUpdateable()){
                queryStr = queryStr + selectedField.getName() + ',';
            }

        }
        if(queryStr.length() > =1){
            queryStr=queryStr.substring(0, queryStr.length()-1);
        }
        return queryStr;
    }
}