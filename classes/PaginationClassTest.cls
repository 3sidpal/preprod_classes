@isTest
public class PaginationClassTest {

    public static testmethod void constructorTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        System.Test.stopTest();
        System.assertNotEquals(null, classObj);

    }

    public static testmethod void setSelectedPageTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        System.Test.stopTest();
        classObj.setSelectedPage('Test Data');
        System.assertNotEquals(null, classObj);

    }

    public static testmethod void getSelectedPageTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        System.Test.stopTest();
        String testStr = classObj.getSelectedPage();
        System.assertEquals('0', testStr);

    }

    public static testmethod void setTotalSizeTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        classObj.setTotalSize(1);
        System.Test.stopTest();
        System.assertNotEquals(null, classObj);
    }

    public static testmethod void getTotalSizeTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        Integer testInt = classObj.getTotalSize();
        System.Test.stopTest();
        System.assertEquals(10, testInt);
    }

    public static testmethod void getMyCommandButtonsTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        classObj.getMyCommandButtons();
        System.Test.stopTest();
        System.assertNotEquals(null, classObj);
    }

    public static testmethod void refreshGridTest() {

        System.Test.startTest();
        PaginationClass classObj = new PaginationClass();
        classObj.refreshGrid();
        classObj.Previous();
        classObj.Next();
        classObj.End();
        classObj.getDisablePrevious();
        classObj.getDisableNext();
        classObj.getTotal_size();
        classObj.getPageNumber();
        classObj.getTotalPages();

        System.Test.stopTest();
        System.assertNotEquals(null, classObj.counter);
        System.assertNotEquals(null, classObj.list_size);
    }
}