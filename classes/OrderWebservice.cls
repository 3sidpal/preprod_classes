@RestResource(urlMapping='/OrderWebservice/*')
global with sharing class OrderWebservice {

    @HttpGet
    global static List<WebServiceResponse.ProductResponse> doGet() {
        List<WebServiceResponse.ProductResponse> prodList = new List<WebServiceResponse.ProductResponse>();
        List<Id> productIds = new List<Id>();
        List<Id> uomIds = new List<Id>();

        Map<Id, Custom_Product__c> customProdMap = new Map<Id, Custom_Product__c>([SELECT Id, SellingUOM__c ,Branded_UOM__c, SellingUOM__r.Name, Branded_UOM__r.KG_Conversion_Rate__c, SKU_Code__c,  Name, Category1__c FROM Custom_Product__c where SellingUOM__c in (Select Id from UOM__c)]);
        for(Conversion__c conv : [SELECT Id, Name, Product__c, UOM__c , KG_Conversion_Rate__c FROM Conversion__c where Product__c in :customProdMap.keySet()]){
            if(conv.UOM__c == customProdMap.get(conv.Product__c).SellingUOM__c)
            {
                Custom_Product__c prod = customProdMap.get(conv.Product__c);
                prodList.add(new WebServiceResponse.ProductResponse(prod.Id, prod.SellingUOM__r.Name, conv.KG_Conversion_Rate__c, prod.SKU_Code__c, prod.Name, prod.Category1__c));
            }
        }
        return prodList;
    }

    @HttpPost
    global static WebServiceResponse.OrderResponse doPost() {


        RestRequest  request    = RestContext.request;
        RestResponse response   = RestContext.response;

        String orderItemstr = (request.requestBody).toString();
        System.debug('orderItemstr :'+orderItemstr);
        List<Order_Item__c> orderItems = new List<Order_Item__c>();
        String orderId = '';
        String errorMessage = '';
        WebServiceResponse.OrderResponse orderResponse = null;

        Map<String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(orderItemstr);
        System.debug('==jsonMap==' + jsonMap);
        String orderJsonStr = JSON.serialize(jsonMap.get('order'));
        Order__c orderObj = (Order__c)JSON.deserialize(orderJsonStr, Order__c.class);
        orderObj.Status__c = 'Submitted';

        if(isDuplicatePO(orderObj.PO_No__c, orderObj.Account__c)) {

            orderResponse = new WebServiceResponse.OrderResponse( null
                                                                , new List<Id>()
                                                                , false
                                                                , new List<String>{'Duplicate PO Number, please enter unique PO for this Account.'}
                                                                );

            return orderResponse;

        }

        try{
            upsert orderObj;
        }catch(Exception e) {

            System.debug('exception :'+e);
            errorMessage = e.getMessage();
            orderResponse = new WebServiceResponse.OrderResponse(null, new List<Id>(), false, new List<String>{errorMessage});

            return orderResponse;
        }
        System.debug('orderObj :'+orderObj);
        orderId = orderObj.Id;
        List<Object> orderItemList = (List<Object>)jsonMap.get('orderItems');

        for(Object obj :orderItemList){
            String jsonStr = JSON.serialize(obj);
            System.debug('jsonStr :'+jsonStr);
            Order_Item__c sObj = (Order_Item__c)JSON.deserialize(jsonStr, Order_Item__c.class);
            sObj.Order_Form__c = orderId;
            orderItems.add(sObj);
        }
        System.debug('orderItems'+orderItems);
        System.debug('orderId'+orderId);

        try{
            upsert orderItems;
        }
        catch(Exception ex){
            System.debug('exception :'+ex);
            errorMessage = ex.getMessage();
            orderResponse = new WebServiceResponse.OrderResponse(orderId, new List<Id>(), false, new List<String>{errorMessage});
            return orderResponse;
        }

        List<Order__c> orderSendList = [SELECT Id
                                             , Order_Reference_No__c
                                             , LastModifiedDate
                                             , Status__c
                                             , Offline_Created_Date__c
                                          FROM Order__c
                                         WHERE Id = :orderObj.Id
                                       ];

        Map<Id, sObject> orderMap = new Map<Id, sObject>(orderItems);
        orderResponse = new WebServiceResponse.OrderResponse( orderId
                                                            , new List<Id>(orderMap.keySet())
                                                            , true
                                                            , new List<String>()
                                                            , orderSendList.get(0).Order_Reference_No__c
                                                            , String.valueOf(DateTime.now())
                                                            , String.valueOf(orderSendList.get(0).lastModifiedDate)
                                                            , orderSendList.get(0).Status__c
                                                            , String.valueOf(orderSendList.get(0).Offline_Created_Date__c)
                                                            );
        return orderResponse;
    }

    private static Boolean isDuplicatePO(String poNum, Id accId) {

        boolean hasDuplicatePO = false;
        try {

            System.debug(' order :'+poNum + accId);
            List<Order__c> orderList = [SELECT Id
                                          FROM Order__c
                                         WHERE PO_No__c = :poNum
                                           AND Account__c = :accId
                                           AND Status__c != 'Draft'
                                       ];
            system.debug('orderList :'+orderList);
            if(orderList.size() > 0) {
                hasDuplicatePO = true;
            }
        } catch(Exception e) {
            system.debug('Exception :'+e);
            hasDuplicatePO = false;
        }
        return hasDuplicatePO;

    }
}