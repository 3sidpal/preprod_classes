public with sharing class OrderDetailandOrderItemsList {

    public List<String> fldApiNms {set;get;}
    public List<String> fldApiPoultNms {set;get;}
    public List<String> oppFldApiNms {get;set;}
    public List<Order_Item__c> oppLineItems {set;get;}
    public List<Order_Item__c> oppLineTestItems {set;get;}
    public Map<String,String> oppFldMap;
    public Map<String,String> fldMap {get;set;}
    public String oppRecId;
    public String alertClass {set;get;}
    public String alertMessage {set;get;}
    public List<Account> accList {get;set;}
    public List<oliProdWrapper> oliProdWrapperList {get;set;}
    public Boolean isPoultry {get;set;} //GAAC: 3/21/2018
    public Boolean isDraft {get;set;}
    public String systemDateToday {get;set;}
    public String strJSON {get;set;}
    public DateTime systemDateNow {get;set;}
    
    public Order__c opp {set;get {

        systemDateToday  = String.valueOf(systemDateNow.month())+'/'+String.valueOf(systemDateNow.day())+'/'+String.valueOf(systemDateNow.year())+' ';
        
        if(systemDateNow.hour() > 12) {
            systemDateToday  += String.valueOf(systemDateNow.hour()-12)+':';

            if(systemDateNow.minute() < 10) {
                systemDateToday  += '0'+String.valueOf(systemDateNow.minute())+' PM';
            }
            else {
                systemDateToday  += String.valueOf(systemDateNow.minute())+' PM';
            }
        }
        else {
            systemDateToday  += String.valueOf(systemDateNow.hour())+':';
            
            if(systemDateNow.hour() == 12) {
                if(systemDateNow.minute() < 10) {
                    systemDateToday  += '0'+String.valueOf(systemDateNow.minute())+' PM';
                }
                else {
                    systemDateToday  += String.valueOf(systemDateNow.minute())+' PM';
                }
            }
            else {
                if(systemDateNow.minute() < 10) {
                    systemDateToday  += '0'+String.valueOf(systemDateNow.minute())+' AM';
                }
                else {
                    systemDateToday  += String.valueOf(systemDateNow.minute())+' AM';
                }
            }
        }

        if(SearchWrapperCls != NULL) {
            oppRecId = SearchWrapperCls.selectedOppId;
            system.debug('==================oppRecId'+oppRecId);
        }

        if(null == opp || ( null != opp && opp.Id != oppRecId)) {
            opp = new Order__c();
            fldApiPoultNms = new List<String>();
            String qry = 'SELECT Id,Po_Document__c,Deposit_Slip__c,';
            oppFldMap = new Map<String,String>();
            oppFldMap = Utils.fetchFldsMapFromFldSet(Utils.fetchOppFieldSet(SearchWrapperCls.oliCsDet.OpportunityFieldSetName__c));
            oppFldApiNms = new List<String>();
            oppFldApiNms = oppFldMap.values();

            qry+= Utils.fetchQuery(oppFldApiNms);
            
            if(!qry.contains(',Status__c')) {
                qry+= ',Status__c';
            }
            
            fldMap = new Map<String,String>();
            fldMap = Utils.fetchFldsMapFromFldSet(Utils.fetchOLIFieldSet(SearchWrapperCls.oliCsDet.OLIFieldSetName__c));

            fldApiNms = fldMap.values();

            for(String f : fldApiNms) {
                if(f == 'Order_Quantity__c') {
                    fldApiPoultNms.add(f);
                    fldApiPoultNms.add('Size_Range__c');
                    fldApiPoultNms.add('Catch_Weight__c');
                }
                else {
                    fldApiPoultNms.add(f);
                }
            }

            qry += ',(SELECT Received__c, Product__r.Brand__r.Name, Estimated_Amount__c, Product__r.Catch_Weight_UOM__c,'

                +Utils.fetchQuery(fldApiPoultNms )

            +'  FROM Order_Items__r ORDER BY Product__r.Brand__r.Name ASC)'

            +', CreatedBy.Name,In_50_KG__c FROM Order__c'
            +' WHERE Id =:oppRecId';

            //List<Order__c> oppList = Database.query(qry);
            List<Order__c> oppList = new List<Order__c>();
            system.debug('CHECKING:'+qry);
            for(Order__c ord : Database.query(qry)) {
                oppList.add(ord);
            }
            
            if(null != oppList && oppList.size() > 0){
                opp = oppList.get(0);
                
                if(opp.Sales_Org__c != NULL) {
                    if((opp.Sales_Org__c.toLowerCase()).Contains('poultry')) {
                        isPoultry = TRUE;
                    }
                    else {
                        isPoultry = FALSE;
                    }
                }
                else {
                    isPoultry = FALSE;
                }
                
                oppLineItems = opp.Order_Items__r;
                
                if(null == oppLineItems) {
                    oppLineItems = new List<Order_Item__c>();
                }
                else {

                    oliProdWrapperList = new List<oliProdWrapper>();

                    if(oppLineItems != null && oppLineItems.size() > 0) {
                        Set<Id> custProdIdSet = new Set<Id>();
                        for(Order_Item__c oi : oppLineItems) {
                            custProdIdSet.add(oi.Product__c);
                        }

                        List<Custom_Product__c> custProdList = new List<Custom_Product__c>();
                        custProdList = [SELECT Id,
                                               Name,
                                               Brand__c,
                                               Scale__c,
                                               Product_Group__c,
                                               Brand__r.Name,
                                               Scale__r.Name,
                                               Product_Group__r.Name,
                                               SKU_Code__c,
                                               Price__c,
                                               SellingUOM__c,
                                               Catch_Weight_UOM__c
                                        FROM Custom_Product__c
                                        WHERE Id IN: custProdIdSet ORDER BY Brand__r.Name ASC];
                        system.debug('custProdList'+custProdList);
                        if(custProdList != null && !custProdList.isEmpty()) {
                            Integer x = 0;

                            for(Order_Item__c oi : oppLineItems) {
                                x++;
                                for(Custom_Product__c cp : custProdList) {

                                    if(oi.Product__c == cp.Id) {

                                        oliProdWrapper oliProdWrapRec = new oliProdWrapper();
                                        oliProdWrapRec.brand = cp.Brand__r.Name;
                                        oliProdWrapRec.scale = cp.Scale__r.Name;
                                        oliProdWrapRec.prodGroup = cp.Product_Group__r.Name;
                                        oliProdWrapRec.prodName = cp.Name;
                                        oliProdWrapRec.matCode = cp.SKU_Code__c;
                                        oliProdWrapRec.itemNumber = String.valueOf(x);
                                        oliProdWrapRec.qty = Integer.valueOf(oi.Order_Quantity__c);
                                        oliProdWrapRec.uom = oi.SellingUOM__c;
                                        oliProdWrapRec.amt = oi.Estimated_Amount__c;
                                        if(oliProdWrapRec.amt != null){
                                            oliProdWrapRec.amt.setScale(2);
                                        }
                                        //oliProdWrapRec.amt.setScale(2);
                                        oliProdWrapperList.add(oliProdWrapRec);
                                    }
                                }
                            }
                            x = 0;
                            system.Debug('***oliProdWrapperList: '+oliProdWrapperList);
                        }
                    }
                }

                //AIRP: 11/29/2017 - Get AR Balance values into the AR Balance table.
                accList = new List<Account>();

                ExtractionUpdateUtility eupUtility = new ExtractionUpdateUtility();
                String whereClause = 'Id = \''+opp.Account__c+'\'' ;
                accList = eupUtility.dynamicQuery('Account', new Set<String>(), whereClause, new List<String>());
                //AIRP: 11/29/2017 - END

                if(opp.Status__c == 'Draft') {
                    isDraft = TRUE;
                }
            }
        }
        return opp;
    }}

    public OrderDetailandOrderItemsList() {
        isDraft = FALSE;
        systemDateNow = OrderUtils.getSystemNow();
        system.debug('TESTINGL'+systemDateNow);
    }

    public Transient SearchWrapper SearchWrapperCls {set;get;}

    //Stage Name to showcase Received Column in OLI
    public String stageName{set;get {
        return Utils.stageName;
    }}

    public void SaveOppLineItems() {

       System.debug('CHECKING:'+oppLineTestItems );
       System.debug('strJSON---'+strJSON);

       Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(strJSON);

       Boolean flag = false;
       Boolean allReceivedFlag = true;

       List<Order_Item__c> oiList = new List<Order_Item__c>();
       List<sObject> soiList = new List<sObject>();
       Map<Id,Order_Item__c> mapOrderItem = new Map<Id,Order_Item__c>();

        try {
            for(Order_Item__c oi : oppLineItems) {

                sObject sObj = Schema.getGlobalDescribe().get('Order_Item__c').newSObject();
                if (results.get(oi.Id) != null) {
                    sObj = (Order_Item__c)JSON.deserialize(JSON.serialize(results.get(oi.Id)),Order_Item__c.class);
                    /*if(oi.test__c != null) {
                        if(oi.test__c <= 0) { // test__c is Quantity Remaining
                            //oi.Received__c = true;
                            sObj.put('Received__c',true);
                        }
                    }*/
                    sObj.put('Id',oi.Id);
                    if(sObj != null) {
                        //oiList.add((Order_Item__c)sObj);
                        mapOrderItem.put(oi.Id,(Order_Item__c)sObj);
                    }
                }
            }

            if(!flag) {
                for (Order_Item__c objLineItem : oppLineItems) {
                    if (!mapOrderItem.isEmpty() && mapOrderItem.containsKey(objLineItem.Id)) {
                        objLineItem.Id = mapOrderItem.get(objLineItem.Id).Id;
                        objLineItem.Catch_Weight__c = (mapOrderItem.get(objLineItem.Id).Catch_Weight__c != null ? mapOrderItem.get(objLineItem.Id).Catch_Weight__c : 0);
                        objLineItem.Remarks__c = (mapOrderItem.get(objLineItem.Id).Remarks__c != null ? mapOrderItem.get(objLineItem.Id).Remarks__c : '');
                        objLineItem.Quantity_Received__c = (mapOrderItem.get(objLineItem.Id).Quantity_Received__c != null ? mapOrderItem.get(objLineItem.Id).Quantity_Received__c : 0);
                        objLineItem.Received__c = (objLineItem.Order_Quantity__c == mapOrderItem.get(objLineItem.Id).Quantity_Received__c) ? true : false;
                        oiList.add(objLineItem);
                    }
                }
                update oiList;

                alertClass = 'alert-success';
                alertMessage = label.OrderItemsSuccessMessage;
            }
           for(Order_Item__c oi :oppLineItems ) {
                if(!oi.Received__c)
                 allReceivedFlag = false;
            }
            if(allReceivedFlag) {
                TriggerUtility.BY_PASS_ORDER_FILECOUNTER_UPDATE = true;
                Order__c order = new Order__c(id=opp.Id,Status__c='Closed');
                update order;
                alertMessage+=' and Changed Order Status to Closed';
            }
        }
        catch(DMLException e){
            alertClass = 'alert-danger';
            alertMessage = e.getMessage();
        }
    }

    public PageReference btnEdit() {

        PageReference pr = new PageReference('/apex/OrderBookingPage?order_id='+oppRecId);
        return pr;
    }

    public PageReference btnBack() {

        searchWrapperCls.selectedOppId = null;
        searchWrapperCls.oliCsDet = null;
        PageReference pr = new PageReference('/apex/Order_Summary');
        return pr;
    }

    public PageReference btnProceed() {

        opp.Status__c = 'Submitted';
        isDraft = FALSE;

       
        Update opp;       
        Update oppLineItems;
        
        PageReference pr = new PageReference('/'+opp.Id);
        return pr;
    }

    public class oliProdWrapper {

        public String brand {get;set;}
        public String scale {get;set;}
        public String prodGroup {get;set;}
        public String prodName {get;set;}
        public String matCode {get;set;}
        public String itemNumber {get;set;}
        public Integer qty {get;set;}
        public String uom {get;set;}
        public Decimal amt {get;set;}
    }
}