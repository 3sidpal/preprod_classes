public with sharing class TP_OrderSummaryController {
    
    public String searchText {get; set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public String strStartDate {get; set;}
    public String strEndDate {get; set;}
    public String sortField {get; set;}
    public String sortOrder {get; set;}
    public String activeTab {get; set;}
    public String renderingPanelName {get; set;}
    public Map<String, SearchWrapper> searchWrapperMap {get; set;}
    //Code Added by Abhilash
    public String OpportunityDetailId{get;set;}
    public SearchWrapper searchWrapperCls{get;set;}
    public List<Schema.FieldSet> orderFieldSet ;
    //code change by Aditi to get total rowsize
    
    public List<SelectOption> orderStatusOptionList{get;set;}
    public String selectedOrderStat{get;set;}
    public Order__c tempOrder {get;set;}
    
    //GAAC
    public String orderPOId {get;set;}
    public String genPOURL {get;set;}
    public String sfUrl {get;set;}
    public String systemDateToday {get;set;}
    
    public static final Integer PRINTLIMIT =1000;
  
    //code change ends

    public TP_OrderSummaryController() {
    
        startDate = System.today()-Integer.valueOf(System.label.FromNumberOfDaysInSearch);
        enddate = System.today();
        sortField = 'Createddate';
        sortOrder = 'DESC';
        activeTab = 'Submitted';
        searchWrapperMap = new Map<String, SearchWrapper>();
        
        systemDateToday  = String.valueOf(system.now().month())+'/'+String.valueOf(system.now().day())+'/'+String.valueOf(system.now().year())+' ';
        
        if(system.now().hour() > 12) {
            systemDateToday  += String.valueOf(system.now().hour()-12)+':';

            if(system.now().minute() < 10) {
                systemDateToday  += '0'+String.valueOf(system.now().minute())+' PM';
            }
            else {
                systemDateToday  += String.valueOf(system.now().minute())+' PM';
            }
        }
        else {
            systemDateToday  += String.valueOf(system.now().hour())+':';

            
            if(system.now().hour() == 12) {
                if(system.now().minute() < 10) {
                    systemDateToday  += '0'+String.valueOf(system.now().minute())+' PM';
                }
                else {
                    systemDateToday  += String.valueOf(system.now().minute())+' PM';
                }
            }
            else {
                if(system.now().minute() < 10) {
                    systemDateToday  += '0'+String.valueOf(system.now().minute())+' AM';
                }
                else {
                    systemDateToday  += String.valueOf(system.now().minute())+' AM';
                }
            }
        }
        
        //GAAC
        sfUrl = URL.getSalesforceBaseUrl().getHost();
         
        // Added by: JM - 07.04.2017
        strStartDate = startDate.format();
        strEnddate = enddate.format();
        RecordTypeInfo purchaseOrderRecordType = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order');
        tempOrder = new Order__c();
        
        if(purchaseOrderRecordType != null){
            tempOrder.RecordTypeId = purchaseOrderRecordType.getRecordTypeId();
        }else{
            System.assertEquals(null, 'Purchase Order Record Type does not exist');
        }
        
        orderStatusOptionList = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Order__c.Status__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            orderStatusOptionList.add(new SelectOption(f.getLabel() == 'For Receiving' ? 'For_Receiving' : f.getLabel(), f.getValue()));
            searchWrapperMap.put(f.getValue() == 'For Receiving' ? 'For_Receiving' : f.getValue(), new SearchWrapper('',startDate,endDate,sortField,sortOrder));            
        }   
    }

    public PageReference performSearch() {

        String compareText = (String.isNotBlank(searchText) && searchText.contains('*')) ? searchText.remove('*') : searchText;
        
        if((String.isNotBlank(compareText) && compareText.length() < 2) || (searchText == '*')) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,Label.Search_Error));
        }
        else {
            
            if(searchWrapperMap.containsKey(activeTab)) {
                //showOpportunityDetails();
                searchWrapper srcWrp = searchWrapperMap.get(activeTab);

                srcWrp.searchStr = searchText;
                srcWrp.stDate = startDate;
                srcWrp.enDate = endDate;
                srcWrp.sortedField = sortField;
                srcWrp.sortedOrder = sortOrder;
                srcWrp.selectedOppId = null;   //Code Added by Abhilash for Detail Page
                srcWrp.offsetValue = null; //code changes by Aditi
                srcwrp.counter = 0; //Code changes by Aditi
                searchWrapperMap.put(activeTab,srcWrp);
            }
        }
        return null;
    }
   
     public void activeTab_Submit(){

        activeTab = activeTab.replace(' ', '_');
        showHoldingValues(activeTab);
        searchWrapperCls = searchWrapperMap.get(activeTab);  
    }
    
    @TestVisible private void putRenderingPanelName(){
        renderingPanelName = activeTab + 'Panel' + ', searchPanel';
    }

    public void showHoldingValues(String selectedTab){
    
        if(searchWrapperMap.containsKey(selectedTab)) {
                
            searchWrapper srcWrp = searchWrapperMap.get(selectedTab);
            searchText = srcWrp.searchStr;
            startDate = srcWrp.stDate;
            endDate = srcWrp.enDate;
            sortField = srcWrp.sortedField;
            sortOrder = srcWrp.sortedOrder;
        }
    }
    
    public void showOpportunityDetails(){
        searchWrapperCls = searchWrapperMap.get(activeTab);
        searchWrapperCls.selectedOppId = OpportunityDetailId;
        searchWrapperCls.oliCsDet = OrderSummarySettings__c.getAll().get(activeTab);
        searchWrapperMap.put(activeTab,searchWrapperCls);

        //GAAC
        genPOURL = 'https://'+sfUrl+'/apex/OrderPDF?Id='+OpportunityDetailId;
    }
    
    public void backToListView(){
        searchWrapperCls.selectedOppId = null;
        searchWrapperCls.oliCsDet = null;
        searchWrapperCls = searchWrapperMap.get(activeTab);
        searchWrapperMap.put(activeTab,searchWrapperCls);
    }

    public PageReference renderAsMethod() {
        //convertToPdf = 'Pdf';
        PageReference pg = Page.Order_SummaryList;
        searchWrapper srcWrp = searchWrapperMap.get(activeTab);
        srcWrp.printPDFmode=true;
        srcWrp.activeTab=activeTab;
        pg.getParameters().put('searchWrapper',json.serialize(srcWrp));
        return pg;
        //return null;
    }
    
    public void updateToDate() {
        
        endDate = startDate+7;
    }
}