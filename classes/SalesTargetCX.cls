/**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : CONTROLLER EXTENSION CLASS OF SALES TARGET VISUALFORCE PAGE(REDESIGNED).
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.9.2016 - CREATED(REDESIGNED).
**/
public with sharing class SalesTargetCX {
    public Order__c orderRecord{get;set;}
    public String returnUrl = '';
    //Account Variables
    public Id accountId;
    public Map<Id,AccountSelectionWrapper> accountSelected = new Map<Id,AccountSelectionWrapper>();
    public List<AccountSelectionWrapper> accountList {get;set;}
    public List<Account> acctSearchResultList;
    public String acctNameFilter{get;set;}
    public String acctBUFilter{get;set;}
    //PRODUCT VARIABLES
    public Map<Id,ProductSelectionWrapper> productSelected = new Map<Id,ProductSelectionWrapper>();
    public List<ProductSelectionWrapper> productList{get;set;}
    public List<Custom_Product__c> productSearchResultList;
    public String prodNameFilter{get;set;}
    public String prodBUFilter{get;set;}
    public String prodBrandFilter{get;set;}
    public String prodCategoryFilter{get;set;}
    Map<Id,List<SelectOption>> optionPerProductMap = new Map<Id,List<SelectOption>>();
    //PAGINATION SETTINGS
    //Accounts pagination setings
    ApexPages.StandardSetController standardSetCon;
    public Integer pageSize = 10;
    public Integer pageNumber = 1;
    public Boolean hasNext{get;set;}
    public Boolean hasPrevious{get;set;}
    //Products Pagination settings
    ApexPages.StandardSetController standardSetConProducts;
    public Integer prodPageSize = 10;
    public Integer prodPageNumber = 1;
    public Boolean hasNextProd{get;set;}
    public Boolean hasPreviousProd{get;set;}
    
    
    public SalesTargetCX(ApexPages.standardController controller){
        accountList = new List<AccountSelectionWrapper>();
        productList = new List<ProductSelectionWrapper>();
        orderRecord = new Order__c();
        accountId = ApexPages.currentPage().getParameters().get('accId');
        returnUrl = ApexPages.currentPage().getParameters().get('retURL');
        if(String.isBlank(returnUrl) || String.isEmpty(returnUrl)){
            returnUrl = '/home/home.jsp';
        }
        if(accountId != null){
            returnUrl = '/' + accountId;
            for(Account acc : [SELECT Id, Name, Market__c, AccountNumber, Branded__c, BU_Feeds__c, BU_Poultry__c, GFS__c FROM Account WHERE Active__c = True AND Id = : accountId]){
                AccountSelectionWrapper acctWrap = new AccountSelectionWrapper();
                acctWrap.isSelected = false;
                acctWrap.accountRecord = acc;
                accountList.add(acctWrap);
            }
            if(accountList.size()==0){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Account is not valid or not active. Please use search to find valid accounts.'));
            }
        }else{
            searchAccounts();
        }
        
        searchProducts();
    }
    
    public void searchAccounts(){
        acctSearchResultList = new List<Account>();
        String accountStrQuery = 'SELECT Id, Name, AccountNumber,Market__c FROM Account WHERE Active__c = True';
        if(String.isNotBlank(acctNameFilter)){
            accountStrQuery += ' AND Name LIKE \'%'+acctNameFilter+ '%\'';
        }
        if(String.isNotBlank(acctBUFilter)){
            // if(acctBUFilter.toUpperCase() == 'BRANDED'){
            //     accountStrQuery += ' AND Branded__c = true';
            // }
            // if(acctBUFilter.toUpperCase() == 'FEEDS'){
            //     accountStrQuery += ' AND Feeds__c = true';
            // }
            // if(acctBUFilter.toUpperCase() == 'POULTRY'){
            //     accountStrQuery += ' AND Poultry__c = true';
            // }
            // if(acctBUFilter.toUpperCase() == 'GFS'){
            //     accountStrQuery += ' AND GFS__c = true';
            // }
            accountStrQuery += 'AND Market__r.Name LIKE \'%' + acctBUFilter + '%\'';
        }
        accountStrQuery += ' ORDER BY Name LIMIT 1000';
        acctSearchResultList = Database.query(accountStrQuery);
        standardSetCon = new ApexPages.StandardSetController(acctSearchResultList);
        standardSetCon.setPageSize(pageSize);
        standardSetCon.setpageNumber(pageNumber);
        if(standardSetCon.getHasNext()){
            hasNext = true;
        }else{
            hasNext = false;
        }
        if(standardSetCon.getHasPrevious()){
            hasPrevious = true;
        }else{
            hasPrevious = false;
        }
        if(accountList.size()>0){
            for(AccountSelectionWrapper acctwrap : accountList){
                if(acctwrap.isSelected == true){
                    accountSelected.put(acctwrap.accountRecord.Id,acctwrap);
                }else{
                    if(accountSelected.containsKey(acctwrap.accountRecord.Id)){
                        accountSelected.remove(acctwrap.accountRecord.Id);
                    }
                }
            }    
        }
        accountList = new List<AccountSelectionWrapper>();
        for(Account acc : (List<Account>)standardSetCon.getRecords()){
            AccountSelectionWrapper accWrap = new AccountSelectionWrapper();
            if(accountSelected.containsKey(acc.Id)){
                accWrap = accountSelected.get(acc.Id);
            }else{
                accWrap.isSelected = false;
                accWrap.accountRecord = acc;
            }
            accountList.add(accWrap);
        }
    }
    
    
    public void searchProducts(){
        Set<Id> productIds = new Set<Id>();
        productSearchResultList = new List<Custom_Product__c>();
        String productStrQuery = 'SELECT Id, Name, Business_Unit__c,Brand__c,Category1__c,Product_Group__c, Product_Group__r.Name, Brand_Name__c, Category_Name__c FROM Custom_Product__c WHERE Active__c = True';
        if(String.isNotBlank(prodNameFilter)){
            productStrQuery += ' AND Name LIKE \'%'+prodNameFilter+ '%\'';
        }
        if(String.isNotBlank(prodBrandFilter)){
            productStrQuery += ' AND Brand_Name__c LIKE \'%'+prodBrandFilter+ '%\'';
        }
        if(String.isNotBlank(prodCategoryFilter)){
            productStrQuery += ' AND Category_Name__c LIKE \'%'+prodCategoryFilter+ '%\'';
        }
        
        if(String.isNotBlank(prodBUFilter)){
            // if(prodBUFilter.toUpperCase() == 'BRANDED'){
            //     productStrQuery += ' AND Branded__c = true';
            // }
            // if(prodBUFilter.toUpperCase() == 'FEEDS'){
            //     productStrQuery += ' AND Feeds__c = true';
            // }
            // if(prodBUFilter.toUpperCase() == 'POULTRY'){
            //     productStrQuery += ' AND Poultry__c = true';
            // }
            // if(prodBUFilter.toUpperCase() == 'GFS'){
            //     productStrQuery += ' AND GFS__c = true';
            // }
            productStrQuery += ' AND Market__r.Name LIKE \'%' + prodBUFilter + '%\'';
        }
        productStrQuery += ' ORDER BY Name LIMIT 1000';
        for(Custom_Product__c prod : Database.query(productStrQuery)){
            productIds.add(prod.Id);
            productSearchResultList.add(prod);
        }
        optionPerProductMap = new Map<Id,List<SelectOption>>();
        for(Conversion__c uom : [Select Id,Name, Product__c From Conversion__c Where Product__c IN :productIds]){
            if(optionPerProductMap.containsKey(uom.Product__c)){
                List<SelectOption> optList = optionPerProductMap.get(uom.Product__c);
                optList.add(new SelectOption(uom.Id,uom.Name));
                optionPerProductMap.put(uom.Product__c,optList);
            }else{
                List<SelectOption> optList = new List<SelectOption>();
                optList.add(new SelectOption(uom.Id,uom.Name));
                optionPerProductMap.put(uom.Product__c,optList);
            }   
        }
        standardSetConProducts = new ApexPages.StandardSetController(productSearchResultList);
        standardSetConProducts.setPageSize(prodPageSize);
        standardSetConProducts.setpageNumber(prodPageNumber);
        if(standardSetConProducts.getHasNext()){
            hasNextProd = true;
        }else{
            hasNextProd = false;
        }
        if(standardSetConProducts.getHasPrevious()){
            hasPreviousProd = true;
        }else{
            hasPreviousProd = false;
        }
        if(productList.size()>0){
            for(ProductSelectionWrapper pwrap : productList){
                if(pwrap.isSelected == true){
                    productSelected.put(pwrap.productRecord.Id,pwrap);
                }else{
                    if(productSelected.containsKey(pwrap.productRecord.Id)){
                        productSelected.remove(pwrap.productRecord.Id);
                    }
                }
            }
        }
        
        productList = new List<ProductSelectionWrapper>();
        for(Custom_Product__c prod : (List<Custom_Product__c>)standardSetConProducts.getRecords()){
            ProductSelectionWrapper prodWrap = new ProductSelectionWrapper();
            if(productSelected.containsKey(prod.Id)){
                prodWrap = productSelected.get(prod.Id);
            }else{
                prodWrap.isSelected = false;
                prodWrap.productRecord = prod;
                if(optionPerProductMap.containsKey(prod.Id)){
                    prodWrap.uomOptionList = optionPerProductMap.get(prod.Id);
                }
            }
            productList.add(prodWrap);
        }
        
        
        
    }
    
    public PageReference saveOrder(){
        Boolean nullOrderDate = false;
        Boolean noAccount = false;
        Boolean noProducts = false;
        if(accountList.size()>0){
            for(AccountSelectionWrapper acctwrap : accountList){
                if(acctwrap.isSelected == true){
                    accountSelected.put(acctwrap.accountRecord.Id,acctwrap);
                }else{
                    if(accountSelected.containsKey(acctwrap.accountRecord.Id)){
                        accountSelected.remove(acctwrap.accountRecord.Id);
                    }
                }
            }    
        }
        if(productList.size()>0){
            for(ProductSelectionWrapper pwrap : productList){
                if(pwrap.isSelected == true){
                    productSelected.put(pwrap.productRecord.Id,pwrap);
                }else{
                    if(productSelected.containsKey(pwrap.productRecord.Id)){
                        productSelected.remove(pwrap.productRecord.Id);
                    }
                }
            }
        }
        if(orderRecord.Invoice_Date__c == null){
            nullOrderDate = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Order date is required.'));
        }
        if(accountSelected.size()==0){
            noAccount = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select an account.'));    
        }
        if(productSelected.size()==0){
            noProducts = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select a product.'));    
        }
        
        if(!nullOrderDate && !noAccount && !noProducts){
            SavePoint sp = Database.setSavepoint();
            try{
                Id orderRecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Sales Target').getRecordTypeId();
                Id itemRecordTypeId = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Sales Target').getRecordTypeId();
                List<Order__c> orderForInsert = new List<Order__c>();
                List<Order_Item__c> oliList = new List<Order_Item__c>();
                for(AccountSelectionWrapper acctSel : accountSelected.values()) {
                    Order__c neworder = new Order__c();
                    neworder.Invoice_Date__c = Date.valueof(orderRecord.Invoice_Date__c);
                    neworder.Account__c = acctSel.accountRecord.Id;
                    neworder.RecordTypeId = orderRecordTypeId;
                    orderForInsert.add(neworder);
                }
                if(orderForInsert.size()>0){
                    insert orderForInsert;
                }
                
                for(Order__c ord : orderForInsert){
                    for(ProductSelectionWrapper ps : productSelected.values()){
                        Order_Item__c neworderitem = new Order_Item__c();
                        neworderitem.Order_Form__c = ord.id;
                        neworderitem.Product__c = ps.productRecord.Id;
                        neworderitem.Target_Brand__c = ps.productRecord.Brand__c;
                        neworderitem.Target_Category__c = ps.productRecord.Category1__c;
                        neworderitem.Custom_Product_Group__c = ps.productRecord.Product_Group__c;
                        neworderitem.Target_Sales_Quantity__c = ps.targetSalesQuantity != null ? ps.targetSalesQuantity : 0;
                        neworderitem.Target_Sales__c = ps.targetSalesAmount != null ? ps.targetSalesAmount : 0;
                        neworderitem.RecordTypeID = itemRecordTypeId;
                        neworderitem.Conversion__c = ps.selectedUOM;
                        oliList.add(neworderitem);
                    }    
                }
                
                if(oliList.size()>0){
                    insert oliList;
                }
                PageReference p = new PageReference(returnUrl);
                p.setRedirect(true);
                return p;
            }catch(Exception ex){
                Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,ex.getMessage()));    
            }
            
            
        }
        
        return null;
    }
    
    public void nextAccountList(){
        if(standardSetCon.getHasNext()){
            for(AccountSelectionWrapper acctwrap : accountList){
                if(acctwrap.isSelected == true){
                    accountSelected.put(acctwrap.accountRecord.Id,acctwrap);
                }else{
                    if(accountSelected.containsKey(acctwrap.accountRecord.Id)){
                        accountSelected.remove(acctwrap.accountRecord.Id);
                    }
                }
            }
            standardSetCon.next();
            accountList = new List<AccountSelectionWrapper>();
            for(Account acc : (List<Account>)standardSetCon.getRecords()){
                AccountSelectionWrapper accWrap = new AccountSelectionWrapper();
                if(accountSelected.containsKey(acc.Id)){
                    accWrap = accountSelected.get(acc.Id);
                }else{
                    accWrap.isSelected = false;
                    accWrap.accountRecord = acc;
                }
                accountList.add(accWrap);
            }
        }
        
        if(standardSetCon.getHasNext()){
            hasNext = true;
        }else{
            hasNext = false;
        }
        if(standardSetCon.getHasPrevious()){
            hasPrevious = true;
        }else{
            hasPrevious = false;
        }
    }
    
    public void previousAccountList(){
        if(standardSetCon.getHasPrevious()){
            for(AccountSelectionWrapper acctwrap : accountList){
                if(acctwrap.isSelected == true){
                    accountSelected.put(acctwrap.accountRecord.Id,acctwrap);
                }else{
                    if(accountSelected.containsKey(acctwrap.accountRecord.Id)){
                        accountSelected.remove(acctwrap.accountRecord.Id);
                    }
                }
            }
            standardSetCon.previous();
            accountList = new List<AccountSelectionWrapper>();
            for(Account acc : (List<Account>)standardSetCon.getRecords()){
                AccountSelectionWrapper accWrap = new AccountSelectionWrapper();
                if(accountSelected.containsKey(acc.Id)){
                    accWrap = accountSelected.get(acc.Id);
                }else{
                    accWrap.isSelected = false;
                    accWrap.accountRecord = acc;
                }
                accountList.add(accWrap);
            }
        }
        
        if(standardSetCon.getHasNext()){
            hasNext = true;
        }else{
            hasNext = false;
        }
        if(standardSetCon.getHasPrevious()){
            hasPrevious = true;
        }else{
            hasPrevious = false;
        }
    }
    
    public void nextProductList(){
        if(standardSetConProducts.getHasNext()){
            for(ProductSelectionWrapper pwrap : productList){
                if(pwrap.isSelected == true){
                    productSelected.put(pwrap.productRecord.Id,pwrap);
                }else{
                    if(productSelected.containsKey(pwrap.productRecord.Id)){
                        productSelected.remove(pwrap.productRecord.Id);
                    }
                }
            }
            standardSetConProducts.next();
            productList = new List<ProductSelectionWrapper>();
            for(Custom_Product__c prod : (List<Custom_Product__c>)standardSetConProducts.getRecords()){
                ProductSelectionWrapper prodWrap = new ProductSelectionWrapper();
                if(productSelected.containsKey(prod.Id)){
                    prodWrap = productSelected.get(prod.Id);
                }else{
                    prodWrap.isSelected = false;
                    prodWrap.productRecord = prod;
                    if(optionPerProductMap.containsKey(prod.Id)){
                        prodWrap.uomOptionList = optionPerProductMap.get(prod.Id);
                    }
                }
                productList.add(prodWrap);
            }
        }
        
        if(standardSetConProducts.getHasNext()){
            hasNextProd = true;
        }else{
            hasNextProd = false;
        }
        if(standardSetConProducts.getHasPrevious()){
            hasPreviousProd = true;
        }else{
            hasPreviousProd = false;
        }
    }
    
    public void previousProductList(){
        if(standardSetConProducts.getHasPrevious()){
            for(ProductSelectionWrapper pwrap : productList){
                if(pwrap.isSelected == true){
                    productSelected.put(pwrap.productRecord.Id,pwrap);
                }else{
                    if(productSelected.containsKey(pwrap.productRecord.Id)){
                        productSelected.remove(pwrap.productRecord.Id);
                    }
                }
            }
            standardSetConProducts.previous();
            productList = new List<ProductSelectionWrapper>();
            for(Custom_Product__c prod : (List<Custom_Product__c>)standardSetConProducts.getRecords()){
                ProductSelectionWrapper prodWrap = new ProductSelectionWrapper();
                if(productSelected.containsKey(prod.Id)){
                    prodWrap = productSelected.get(prod.Id);
                }else{
                    prodWrap.isSelected = false;
                    prodWrap.productRecord = prod;
                    if(optionPerProductMap.containsKey(prod.Id)){
                        prodWrap.uomOptionList = optionPerProductMap.get(prod.Id);
                    }
                }
                productList.add(prodWrap);
            }
        }
        
        if(standardSetConProducts.getHasNext()){
            hasNextProd = true;
        }else{
            hasNextProd = false;
        }
        if(standardSetConProducts.getHasPrevious()){
            hasPreviousProd = true;
        }else{
            hasPreviousProd = false;
        }
    }
    
    public void resetAccountSearch(){
        acctNameFilter = '';
        acctBUFilter = '';
        searchAccounts();
    }
    
    public void resetProductSearch(){
        prodNameFilter = '';
        prodBUFilter = '';
        prodBrandFilter = '';
        prodCategoryFilter = '';
        searchProducts();
    }
    
    
    
    public class AccountSelectionWrapper{
        public Boolean isSelected{get;set;}
        public Account accountRecord{get;set;}
    }
    
    public class ProductSelectionWrapper{
        public Boolean isSelected{get;set;}
        public Custom_Product__c productRecord{get;set;}
        public Id selectedUOM{get;set;}
        public List<SelectOption> uomOptionList{get;set;}
        public Decimal targetSalesAmount{get;set;}
        public Decimal targetSalesQuantity{get;set;}
    }
}