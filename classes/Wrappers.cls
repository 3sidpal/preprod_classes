global class Wrappers {

    global class TaskResponseWrapper {
        global Boolean      success;
        global String       jsonStr;
        global List<String> errorMsg;
        global List<Task>   taskList;
        global DateTime     timeStamp;
        global Map<String, String> eformNameVsEformPrefix;
    }

}