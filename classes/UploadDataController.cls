public class UploadDataController {

    public Boolean actionPollerActive{get;set;}
    public Map<String,Map<String,String>> statusMap{get;set;}
    public Map<String,ContentDocument> contentVersionMap{get;set;}
    public String uploadJobId{get;set;}
    //Constructor
    public UploadDataController(ApexPages.StandardController stdController) {
        actionPollerActive = true;
        uploadJobId = '';
        statusMap = new Map<String,Map<String,String>>();
        if(!ApexPages.currentPage().getParameters().containsKey('Id')) {
            uploadJobId = ApexPages.currentPage().getParameters().get('Id');
        }
    }
    
    // This method will be called initially when page is loaded.
    // It will insert a new Upload Job Record
    public PageReference  init() {
        if(!ApexPages.currentPage().getParameters().containsKey('Id') &&
            ApexPages.currentPage().getParameters().get('objectName')!=null) {
            Upload_Job__c uploadJob = new Upload_Job__c(
                Object_Name__c = String.valueOf(ApexPages.currentPage().getParameters().get('objectName')));
            insert uploadJob;            
            PageReference pgRefInst = new PageReference('/apex/Upload_Data?'+'objectName='+ApexPages.currentPage().getParameters().get('objectName'));
            pgRefInst.getParameters().put('Id',uploadJob.Id);
            pgRefInst.setRedirect(true);
            return pgRefInst;
        }
        return null;
    }
    
    public void pollStatus() {
        String uploadJobId = ApexPages.currentPage().getParameters().get('Id');
        List<Upload_Job__c > uploadJobInt = [SELECT Status__c
                                             FROM Upload_Job__c 
                                             WHERE Id = :uploadJobId];
        if(uploadJobInt.isEmpty() == false && 
            String.isNotBlank(uploadJobInt[0].Status__c)) {
            statusMap = (Map<String,Map<String,String>>) Json.deserialize(uploadJobInt[0].Status__c,
                Map<String,Map<String,String>>.class);
            contentVersionMap = new Map<String,ContentDocument>();
            for(ContentVersion conver : [SELECT ContentDocument.Title 
                                         FROM ContentVersion 
                                         WHERE Id IN : statusMap.keySet()]) {
            
                contentVersionMap.put(conver.Id, conver.ContentDocument);
            }
        }
        
    }
    

}