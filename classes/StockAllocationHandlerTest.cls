/**
* ==================================================================================
*    Version        Date                          Comment
* ==================================================================================
*    Initial       6-9-2018        Test class for Stock Allocation
*                                   functionality
*/
@isTest
private class StockAllocationHandlerTest {

    @isTest static void testStockAllocationWithoutStockRecords() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1,accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
            );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 1);
            Test.startTest();
            insert orderItems;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Not Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Not Placed', updatedOrdersItems[0].Status__c);
            List<Account_Product_Allocation__c> accountProductAllocation = [SELECT Id FROM Account_Product_Allocation__c];
            System.assertEquals(true, accountProductAllocation.isEmpty());
        }
    }

    @isTest static void testRegularOrderPlaced() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1,accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
            );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 2, 5, 10, products, 'Regular', '0000', 'D000'
                );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 1);
            Test.startTest();
            insert orderItems;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
        }
    }

    @isTest static void testPromoOrderPlaced() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1,accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
            );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 2, 5, 10, products, 'Promo', '0000', 'D000'
                );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 1);
            Test.startTest();
            insert orderItems;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
        }
    }

    @isTest static void testOrderPlaced() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1,accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
            );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 2, 5, 10, products, 'Regular', '0000', 'D000'
                );
            stockAllocations.addAll(
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 2, 5, 10, products, 'Promo', '0000', 'D000'
                )
            );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            Test.startTest();
            insert orderItems;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
            List<Account_Product_Allocation__c> accountProductAllocation = [SELECT Id FROM Account_Product_Allocation__c];
            System.assertEquals(false, accountProductAllocation.isEmpty());
            System.assertEquals(2, accountProductAllocation.size());
        }
    }

    @isTest static void testPromoOrderUnPlace() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1, accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
            );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 10, 5, 10, products, 'Promo', '0000', 'D000'
                );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            Test.startTest();
            insert orderItems;
            Test.stopTest();

            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Not Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Not Placed', updatedOrdersItems[0].Status__c);
        }
    }

    @isTest static void testPromoOrderPartiallyPlace() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1, accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
                );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 2, 10, 10, products, 'Promo', '0000', 'D000'
                );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            Test.startTest();
            insert orderItems;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Partially Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Partially Placed', updatedOrdersItems[0].Status__c);
        }
    }

    // Test case where Order has some already unplaced and partially place order item present
    @isTest static void testOrderSubstitutionFunctionality() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Custom_Product__c> sunstituteProducts =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'MILK', '1000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, sunstituteProducts);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1, accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
                );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 10, 10, 10, products, 'Promo', '0000', 'D000'
                );
            stockAllocations.addAll(
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 0, 10, 10, sunstituteProducts, 'Promo', '1000', 'D000'
                )
            );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            insert orderItems;
            List<Order_Item__c> orderItemsToProcess =
                StockAllocationHandlerTestDataFactory.createOrderItem(sunstituteProducts, orders[0].id, 'Purchase Order', 10);
            Test.startTest();
            insert orderItemsToProcess;
            Test.stopTest();
            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Partially Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Id =:orderItemsToProcess];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
        }
    }

    // Test re-allocation functionality
    @isTest static void testReAllocationFunctionality() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1, accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
                );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 30, 0, 10, 10, products, 'Promo', '0000', 'D000'
                );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            orderItems.addAll(
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10)
            );
            orderItems.addAll(
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10)
            );

            Test.startTest();
            insert orderItems;
            Test.stopTest();

            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Partially Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
            System.assertEquals('Placed', updatedOrdersItems[1].Status__c);
            System.assertEquals('Not Placed', updatedOrdersItems[2].Status__c);
        }
    }

    //Test default service code present in Order itself Order Trigger
    @isTest static void testOrderUpdateFunctionality() {
        User user = [SELECT Id FROM User WHERE Branded__c =: TRUE AND Poultry__c =: FALSE AND IsActive =: TRUE LIMIT 1];
        List<User> userList = StockAllocationHandlerTestDataFactory.createAndInsertUser(1);
        System.runAs(user) {
            StockAllocationHandlerTestDataFactory.createAndInsertTriggerFlagControl();
            List<Account> accounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'D000');
            List<Account> childAccounts =
                StockAllocationHandlerTestDataFactory.createAndInsertAccount(1, true, 'Distributor/ Direct', 'E000');
            List<Contact> contacts =
                StockAllocationHandlerTestDataFactory.createAndInsertContact(1, accounts[0].Id, 'SAS', userList);
            insert contacts;
            List<Ship_Sold_To__c> shipSoldToList =
                StockAllocationHandlerTestDataFactory.createAndInsertShipSoldTo(accounts, childAccounts[0]);
            List<Market__c> markets =
                StockAllocationHandlerTestDataFactory.createAndInsertMarket(1, true);
            List<Custom_Product__c> products =
                StockAllocationHandlerTestDataFactory.createAndInsertProducts(1, markets[0].id, 'SWINE BLOOD', '0000');
            StockAllocationHandlerTestDataFactory.createAndInsertPlantDetermination(1, accounts, products);
            List<Order__c> orders =
                StockAllocationHandlerTestDataFactory.createOrder(
                    1, accounts[0].id, contacts[0].id, shipSoldToList[0].id, 'Purchase Order','12GO'
                );
            insert orders;
            // List<Serving_Plant__c> servicePlants =
            //     StockAllocationHandlerTestDataFactory.createServingPlants(1);
            // insert servicePlants;
            List<Stock_Allocation__c> stockAllocations =
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 10, 5, 10, 10, products, 'Promo', '0000', 'D000'
                );
            stockAllocations.addAll(
                StockAllocationHandlerTestDataFactory.createStockAllocations(
                    accounts, 20, 5, 10, 10, products, 'Promo', '0000', 'E000'
                )
            );
            insert stockAllocations;
            List<Order_Item__c> orderItems =
                StockAllocationHandlerTestDataFactory.createOrderItem(products, orders[0].id, 'Purchase Order', 10);
            insert orderItems;
            List<Order__c> ordersToProcess = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Partially Placed', ordersToProcess[0].Status__c);
            for (Integer i=0; i < orders.size(); i++) {
                orders[i].Order_Serving_Plant__c = 'E000' + i;
            }
            Test.startTest();
            update orders;
            Test.stopTest();

            List<Order__c> updatedOrders = [SELECT Id, Status__c FROM Order__c WHERE Id =:orders];
            System.assertEquals('Placed', updatedOrders[0].Status__c);
            List<Order_Item__c> updatedOrdersItems = [SELECT Id, Status__c FROM Order_Item__c WHERE Order_Form__c =:orders];
            System.assertEquals('Placed', updatedOrdersItems[0].Status__c);
        }
    }
}