public class ParentChildRequisition
{
    public ListCollection Paging { get; set; }
    public Boolean isMobile { get; set; } // USER_AGENT Protocol in determining if User is running on Mobile or Web.
    public Boolean isParentRequisition { get; set; } // Validate of Requisition Record is Parent.
    public Boolean isOrphanedRequisition { get; set; } // TMG User decided to update the Requisition and uncheck Child Requisition.
    public String ParentRequisitionStatus { get; set; } // Display the Approval Status of Parent Requisition to Child Requisitions.
    public String ParentRequisitionStatusSeverity { get; set; } // Display what type of apex page severity should be displayed.
    
    // <For Constructor> get Requisition Details of the current form without the need to query.
    private final ePAW_Form__c requisition;
    
    // From the URL, get the ID of the Requisition from the URL.
    private String RequisitionId = ApexPages.currentPage().getParameters().get('Id');
    
    public ParentChildRequisition(ApexPages.StandardController controller)
    {
        if(!Test.isRunningTest()) controller.addFields(new List<String>{'Mother_ePAW_Reference__c', 'Parent_Requisition__c', 'Child_Requisition__c'}); // Add the needed values so that you do not need to query.
        this.requisition = (ePAW_Form__c)controller.getRecord();
        isMobile = UserInfo.getUiTheme() == 'Theme4t'; // Returns True or False if User is running on Mobile or Web.
        isParentRequisition = this.requisition.Parent_Requisition__c;
        isOrphanedRequisition = this.requisition.Child_Requisition__c;
        
        try 
        {
            Paging = new ListCollection();
            Paging.PageSize = 10;
            Paging.Requisitions = (isParentRequisition) ? queryRequisitions(requisition.Mother_ePAW_Reference__c) : queryParentRequisition(requisition.Mother_ePAW_Reference__c);
            ParentRequisitionStatus = Paging.Requisitions[0].Approval_Status__c;
            
            if(!ParentRequisitionStatus.equals('Rejected'))
            {
                ParentRequisitionStatusSeverity = (ParentRequisitionStatus.equals('Approved')) ?  'confirm' : (ParentRequisitionStatus.equals('Cancelled')) ? 'warning' : 'info';
            }
            else 
            {
                ParentRequisitionStatusSeverity = 'error';
            }
            
        }
        catch(Exception unexpectedException)
        {
            ApexPages.addMessages(unexpectedException);
        }
    }
    
    // Query Methods 
    private List<ePAW_Form__c> queryRequisitions(String GetMyChildRequisitions)
    {
        return [SELECT Name,
                       Customer_Group__c,
                       Account__c,
                       Reporting_Total_Amount_Child__c,
                       Approval_Status__c,
                       ROPS__c,
                       Expense_Sales_Ratio_w_Promo__c    
                FROM   ePAW_Form__c 
                WHERE  Mother_ePAW_Reference__c = :GetMyChildRequisitions 
                AND    Child_Requisition__c = true];
    }
    
    private List<ePAW_Form__c> queryParentRequisition(String MotherReference)
    {
        return [SELECT Name,
                       Customer_Group__c,
                       Account__c,
                       Reporting_Total_Amount_Mother__c,
                       Approval_Status__c,
                       ROPS__c,
                       Expense_Sales_Ratio_w_Promo__c,
                       Reporting_Total_Amount_Child__c
                FROM   ePAW_Form__c
                WHERE  Mother_ePAW_Reference__c = :MotherReference
                AND    Parent_Requisition__c = true];        
    }

    // Internal Classes
    public class ListCollection extends Pageable
    {
        public List<ePAW_Form__c> Requisitions { get; set; }
        
        public override integer getRecordCount()
        {
            return (Requisitions == null ? 0 : Requisitions.size());
        }
    }
}