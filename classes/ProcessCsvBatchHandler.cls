public class ProcessCsvBatchHandler {
    
    public static void populateSobject(Sobject sobjInst, Schema.DescribeFieldResult fldResult, String rowData) {
        system.debug('==rowData===='+rowData);
        Set<Schema.SOAPType> stringFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.ID,
                                                                           Schema.SOAPType.String,
                                                                           Schema.SOAPType.Time};
        Set<Schema.SOAPType> booleanFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.Boolean};
        Set<Schema.SOAPType> dateFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.Date};
        Set<Schema.SOAPType> dateTimeFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.DateTime};
        Set<Schema.SOAPType> doubleFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.Double};
        Set<Schema.SOAPType> integerFldDataTypes = new Set<Schema.SOAPType>{Schema.SOAPType.Integer};
        
        if(stringFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),String.valueOf(rowData));
        }
        else if(booleanFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),Boolean.valueOf(rowData));
        }
        else if(dateFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),Date.valueOf(rowData));
        }
        else if(dateTimeFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),DateTime.valueOf(rowData));
        }
        else if(doubleFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),Decimal.valueOf(rowData));
        }
        else if(integerFldDataTypes.contains(fldResult.getSoapType())) {
            sobjInst.put(fldResult.getName(),Integer.valueOf(rowData));
        }
        else {
            sobjInst.put(fldResult.getName(),String.valueOf(rowData));
        }
    }
    public static void processRecords(List<String> scope, List<String> headersLst, 
        List<String>errorReasonCsv, String objectName, Boolean processParents,
        Map<String,String> primaryStockallocationMap) {
        Integer srNoIndex = -1;
        Integer parentProductIndex = -1;
        try {
            system.debug('======headersLst======'+headersLst);
            system.debug('======Heap limit======'+Limits.getHeapSize());
            System.debug('======scope=====' + scope);
            
            // removed headers from the scope for first time
            if(String.join(headersLst,',') == scope[0]) {
                scope.remove(0);
            }
            
            //fetch Sr No column index from headers
            for(Integer i=0; i<headersLst.size(); i++) {
                system.debug('==headersLst[i]==='+headersLst[i]);
                if(headersLst[i].trim() == 'Sr No') {
                    srNoIndex = i;
                    break;
                }
            }
            
            //fetch Primary Material Code column index from headers
            for(Integer i=0; i<headersLst.size(); i++) {
                system.debug('==headersLst[i]==='+headersLst[i]);
                if(headersLst[i].trim() == 'Primary Material Code' && objectName == 'Stock_Allocation__c') {
                    parentProductIndex = i;
                    break;
                }
            }
            
            if((parentProductIndex == -1 && objectName == 'Stock_Allocation__c') || srNoIndex == -1) {
                String errorMessage = '';
                if(parentProductIndex == -1 && objectName == 'Stock_Allocation__c') {
                    errorMessage = 'Parent Product Column is required in the CSV.';
                }
                else if(srNoIndex == -1) {
                    errorMessage += 'Sr. No. Column is required in the CSV.';
                }
                if(errorReasonCsv.isEmpty()) {
                    errorReasonCsv.add('Sr No,Error Reason'+ProcessCsvBatch.CR_LF);
                }
                errorReasonCsv[0] += 'All of the data'+',' + errorMessage + ProcessCsvBatch.CR_LF;
                return;
            }
            
            // fetch hcsv header mappings and correlate them with CSV headers
            Map<String,String> csvHeaderToApiName = new Map<String,String>();
            for(CSV_Headers_Mapping__mdt csvheader : [SELECT DeveloperName,
                                                             SF_Field_API_Name__c,
                                                             Csv_Header_Name__c,
                                                             Object_Name__c
                                                      FROM CSV_Headers_Mapping__mdt
                                                      WHERE Object_Name__c = :objectName]) {
                csvHeaderToApiName.put(csvheader.Csv_Header_Name__c, csvheader.SF_Field_API_Name__c);
            }
            
            Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
            if(globalDescribe.containsKey(objectName) && !csvHeaderToApiName.isEmpty()) {
                Map<String, Schema.SObjectField> fieldMap = globalDescribe.get(objectName).getDescribe().fields.getMap();
                Map<String, Schema.DescribeFieldResult> fieldDescribeMap = new Map<String, Schema.DescribeFieldResult>();
                for(String strFld : fieldMap.keySet()) {
                    fieldDescribeMap.put(strFld , fieldMap.get(strFld).getDescribe());
                }
                fieldMap.clear();
                
                List<sObject> sobjLst = new List<sObject>();
                List<String> srNoLst = new List<String>();
                List<Primary_Substitute_Junction__c> primSubsJunc = new List<Primary_Substitute_Junction__c>();
                Map<String,String> mapSubstitutePrimarymaterialCode = new Map<String,String>();
                for(String row : scope) {
                    List<String> rowData;
                    try {
                        system.debug('==row =='+row);
                        rowData = row.split(',');
                        if((parentProductIndex >-1 && String.isBlank(rowData[parentProductIndex]) && processParents == true) ||
                           (parentProductIndex >-1 && String.isNotBlank(rowData[parentProductIndex]) && processParents == false) ||
                           objectName != 'Stock_Allocation__c') {
                            Sobject sobj = globalDescribe.get(objectName).newSObject();
                            for(Integer i=0; i<rowData.size(); i++) {
                                String csvHeader = headersLst[i].trim();
                                String sfApiName = csvHeaderToApiName.get(csvHeader);
                                system.debug('==csvHeader===='+csvHeader);
                                system.debug('==sfApiName===='+sfApiName);
                                system.debug('=fieldDescribeMap====='+json.serialize(fieldDescribeMap.keySet()));
                                if(sfApiName != null && fieldDescribeMap.containsKey(sfApiName.toLowerCase()) && !String.isBlank(rowData[i].trim())) {
                                    populateSobject(sobj,fieldDescribeMap.get(sfApiName.toLowerCase()), rowData[i]);
                                }
                            }
                            srNoLst.add(rowData[srNoIndex]);
                            if(objectName == 'Stock_Allocation__c' && processParents == false) {
                                mapSubstitutePrimarymaterialCode.put(String.valueOf(sobj.get('Material_Code__c')),rowData[parentProductIndex]);
                            }
                            sobjLst.add(sobj);
                            system.debug('===sobj=='+sobj);
                            system.debug('===srNoLst=='+srNoLst);
                        }
                    }
                    catch(Exception exp) {
                        if(errorReasonCsv.isEmpty()) {
                            errorReasonCsv.add('Sr No,Error Reason'+ProcessCsvBatch.CR_LF);
                        }
                        errorReasonCsv[0] += rowData[srNoIndex]+','+exp.getMessage()+ProcessCsvBatch.CR_LF;
                    }
                }
                system.debug('=====mapSubstitutePrimarymaterialCode===='+mapSubstitutePrimarymaterialCode);
                if(sobjLst.isEmpty() == false) {
                    Database.saveResult[] saveResult = Database.insert(sobjLst, false) ;
                    for(Integer i=0; i<saveResult.size(); i++) {
                        if(saveResult[i].isSuccess() == false) {
                            if(errorReasonCsv.isEmpty()) {
                                errorReasonCsv.add('Sr No,Error Reason'+ProcessCsvBatch.CR_LF);
                            }
                            errorReasonCsv[0] += srNoLst[i]+','+String.valueOf(saveResult[i].getErrors()) +ProcessCsvBatch.CR_LF;
                        }
                        else if(objectName == 'Stock_Allocation__c'){
                            if(processParents == false) {
                                primSubsJunc.add(new Primary_Substitute_Junction__c(Secondary_Stock_Allocation__c = saveResult[i].getId(),
                                    Primary_Stock_Allocation__c = primaryStockallocationMap.get(mapSubstitutePrimarymaterialCode.get(String.valueOf(sobjLst[i].get('Material_Code__c'))))));
                            }
                            else {
                                primaryStockallocationMap.put(String.valueOf(sobjLst[i].get('Material_Code__c')),saveResult[i].getId());
                            }
                        }
                    }
                }
                system.debug('=====primaryStockallocationMap===='+primaryStockallocationMap);
                if(primSubsJunc.isEmpty() == false) {
	                List<sObject> stockAllocationDelete = new List<sObject>();
	                Database.saveresult[] saveResJuncn = Database.insert (primSubsJunc, false);
	                for(Integer i=0; i<saveResJuncn.size(); i++) {
	                    if(saveResJuncn[i].isSuccess() == false) {
	                        if(errorReasonCsv.isEmpty()) {
                                errorReasonCsv.add('Sr No,Error Reason'+ProcessCsvBatch.CR_LF);
                            }
                            errorReasonCsv[0] += srNoLst[i]+','+String.valueOf(saveResJuncn[i].getErrors()) +ProcessCsvBatch.CR_LF;
                            stockAllocationDelete.add(sobjLst[i]);
	                    }
	                }
	                if(stockAllocationDelete.isEmpty() == false) {
	                    delete stockAllocationDelete;
	                }
                }
            }
            /*******************error out for no mappings?********/
        }
        catch(Exception exp) {
           for(String row : scope) {
               List<String> rowData = row.split(',');
               if(errorReasonCsv.isEmpty()) {
                    errorReasonCsv.add('Sr No,Error Reason'+ProcessCsvBatch.CR_LF);
               }
               errorReasonCsv[0] += rowData[srNoIndex]+','+exp.getMessage()+ProcessCsvBatch.CR_LF;
           }
        }
    }
    

}