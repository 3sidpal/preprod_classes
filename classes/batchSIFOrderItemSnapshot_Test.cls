/**********************************************************
* @Author       Rozelle Villanueva
* @Date         10-20-2016
* @Description  Test class for batchSIFOrderItemSnapshot 
* @Revision(s)
**********************************************************/
@isTest
public class batchSIFOrderItemSnapshot_Test {
    static testmethod void testSingleSIF(){
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        TestDataFactory tdf = new TestDataFactory();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        system.debug('ACCOUNT DETAIL:'+accList);
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;        
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);
        
         //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestOrder(1, accList[0].Id, contList[0], contList[1]);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_Date__c = invoiceDate;
        sif[0].Invoice_No__c = String.valueOf(system.Today().day()+system.Today().year());
        update sif;
        
        // product record
        list <Custom_Product__c> prodList = tdf.createTestProducts(1);
        //insert prodList;        
        
        // order item record
        list<Order_Item__c> oItem = tdf.createTestOrderItem(prodList, sif[0].Id);
        oItem[0].RecordTypeId = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        update oItem;
        
        Test.startTest();
           batchSIFOrderItemSnapshot bc = new batchSIFOrderItemSnapshot(last3Month, System.Today().year());   
           Database.executeBatch(bc, 2000); 
        Test.stopTest(); 
        System.assertEquals(accList.size() > 0, true);   
        System.assertEquals(contList.size() > 0, true);   
        System.assertEquals(sif.size() > 0, true);   
        System.assertEquals(prodList.size() > 0, true);   
        System.assertEquals(oItem.size() > 0, true);     
    }   
   
    static testmethod void testSnapshotSIF(){
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        TestDataFactory tdf = new TestDataFactory();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;        
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);

         //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestOrder(1, accList[0].Id, contList[0], contList[1]);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_No__c = String.valueOf(system.Today().day()+system.Today().year());
        sif[0].Invoice_Date__c = invoiceDate;
        sif[0].Monthly_order__c = true;
        update sif;
        
        // product record
        list <Custom_Product__c> prodList = tdf.createTestProducts(1);
        //insert prodList;        
        
        // order item record
        list<Order_Item__c> oItem = tdf.createTestOrderItem(prodList, sif[0].Id);
        oItem[0].RecordTypeId = Schema.SObjectType.Order_Item__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        update oItem;
        
        Test.startTest();
           batchSIFOrderItemSnapshot bc = new batchSIFOrderItemSnapshot(last3Month, System.Today().year());   
           Database.executeBatch(bc, 2000); 
        Test.stopTest(); 
        System.assertEquals(accList.size() > 0, true);   
        System.assertEquals(contList.size() > 0, true);   
        System.assertEquals(sif.size() > 0, true);   
        System.assertEquals(prodList.size() > 0, true);   
        System.assertEquals(oItem.size() > 0, true);     
    }   
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    static testmethod void testShedule() {
       Test.startTest();

       // Schedule the test job
       String jobId = System.schedule('scheduleSIFOrderItemSnapshot',
                        CRON_EXP, 
                        new scheduleSIFOrderItemSnapshot ());
         
       // Get the information from the CronTrigger API object
       CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                         NextFireTime
                         FROM CronTrigger WHERE id = :jobId];

       // Verify the expressions are the same
       System.assertEquals(CRON_EXP, 
          ct.CronExpression);

       // Verify the job has not run
       System.assertEquals(0, ct.TimesTriggered);

       // Verify the next time the job will run
       System.assertEquals('2022-03-15 00:00:00', 
       String.valueOf(ct.NextFireTime));
       Test.stopTest();
    }
    
}