@isTest
public class InsertTaskDataserviceTest {

    public static testMethod void insertTaskDataservicePositiveTest() {


        Task taskObj = new Task();
        taskObj.Subject = 'Call';
        insert taskObj;

        List<User> userList = [SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator'];

        String jsonStr=        '{'+
        '   "task": ['+
        '       {'+
        '           "Title__c": "Meeting with Manhattan",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        //'           "User__c": "0050l000001K1N9AAK"'+
        '           "User__c": "'+userList[0].Id+'"'+
        //'           "External_System_Id__c": "0050l000001K1N9AA5"'+
        '       },'+
        '       {'+
        '           "Title__c": "Meeting with Terminator Arnold",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        '           "User__c": "'+userList[0].Id+'",'+
        //'           "User__c": "0050l000001K1N9AAK",'+
        //'           "External_System_Id__c": "0050l000001K1N9AA4",'+
        '           "Id": "'+taskObj.Id+'"'+
        '       }'+
        '   ]'+
        '}';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertTaskData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            Wrappers.TaskResponseWrapper resultRes = InsertTaskDataservice.doPost();
        System.Test.stopTest();


        System.assertEquals(true, resultRes.success);
        System.assertEquals(null, resultRes.errorMsg);

    }


    public static testMethod void insertTaskDataserviceNegetiveTest() {


        List<User> userList = [SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator'];



        String jsonStr=        '{'+
        '   "task": ['+
        '       {'+
        '           "Title__c": "Meeting with Manhattan",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        '           "User__c": "0050l000001K1N9AAK"'+
        //'           "External_System_Id__c": "0050l000001K1N9AA5"'+
        '       },'+
        '       {'+
        '           "Title__c": "Meeting with Terminator Arnold",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        '           "User__c": "0050l000001K1N9AAK",'+
        //'           "External_System_Id__c": "0050l000001K1N9AA4",'+
        '           "Id": "00T0l000006nrPh"'+
        '       }'+
        '   ]'+
        '}';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertTaskData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            Wrappers.TaskResponseWrapper resultRes = InsertTaskDataservice.doPost();
        System.Test.stopTest();


        System.assertEquals(false, resultRes.success);

    }


    public static testMethod void insertTaskDataserviceJSONFailTest() {


        List<User> userList = [SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator'];

        String jsonStr=        '{'+
        '   "task": ['+
        '       {'+
        '           "Title__c": "Meeting with Manhattan",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        '           "User__c": "0050l000001K1N9AAK"'+
        //'           "External_System_Id__c": "0050l000001K1N9AA5"'+
        '       },'+
        '       {'+
        '           "Title__c": "Meeting with Terminator Arnold",'+
        '           "StartDateTime__c": "2018-03-03T00:00:00",'+
        '           "EndDateTime__c": "2018-03-04T00:00:00",'+
        //'           "All_Day__c": "true",'+
        '           "PriceBook__c": "MNN",'+
        '           "User__c": "0050l000001K1N9AAK",'+
        //'           "External_System_Id__c": "0050l000001K1N9AA4",'+
        '           "Id": "00T0l000006nrPh",'+
        '       }'+
        '   ]'+
        '}';

        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertTaskData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request = req;
        RestContext.response= res;

        System.Test.startTest();
            Wrappers.TaskResponseWrapper resultRes = InsertTaskDataservice.doPost();
        System.Test.stopTest();


        System.assertEquals(false, resultRes.success);

    }

}