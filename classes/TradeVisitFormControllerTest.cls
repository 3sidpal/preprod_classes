@isTest
private class TradeVisitFormControllerTest {
    @isTest
    static void testgetExistingTrade() {
        Id tradeId = DIL_TestDataFactory.getTradeVisit(1,true)[0].Id;
        Trade_Visit__c objTrade;
        Test.startTest();
            objTrade = TradeVisitFormController.getExistingTrade(tradeId);
        Test.stopTest();
        System.assertNotEquals(new Trade_Visit__c(), objTrade);
    }

    @isTest
    static void testNegativegetExistingTrade() {
        Trade_Visit__c objTrade;
        Test.startTest();
            objTrade = TradeVisitFormController.getExistingTrade(null);
        Test.stopTest();
        System.assertEquals(new Trade_Visit__c(), objTrade);
    }
}