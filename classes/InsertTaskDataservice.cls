@RestResource(urlMapping='/insertTaskDatas/*')
global with sharing class InsertTaskDataservice {

    @HttpPost
    global static Wrappers.TaskResponseWrapper doPost() {

        RestRequest  request    = RestContext.request;
        RestResponse response   = RestContext.response;

        List<Task> taskUpsertList = new List<Task>();

        String taskStr = (request.requestBody).toString();

        Map<String, Object> jsonMap;

        try {
            jsonMap = (Map<String, Object>)JSON.deserializeUntyped(taskStr);
        } catch(Exception e) {

            return createErrorMsg(e.getMessage(), null, taskStr);

        }
        
        System.debug('----->>>>' + jsonMap);
        //List<Task> taskJsonObjList = (List<Task>) JSON.deserialize(jsonMap.get('task'), List<Task>.class);


        for(Object obj : (List<Object>) jsonMap.get('task')) {

            String jsonTempStr = JSON.serialize(obj);
            System.debug('jsonTempStr :' + jsonTempStr);
            Task sObj = (Task) JSON.deserialize(jsonTempStr, Task.class);
            taskUpsertList.add(sObj);

        }

        try {

            StaticResources.changesMadeByWebService = true;
            upsert taskUpsertList;
            StaticResources.changesMadeByWebService = false;

            Wrappers.TaskResponseWrapper wrapperResObj  = new Wrappers.TaskResponseWrapper();
            wrapperResObj.success                       = true;
            wrapperResObj.jsonStr                       = taskStr;
            wrapperResObj.timeStamp                     = DateTime.now();

            for(Task taskItr : taskUpsertList) {

                if(null == wrapperResObj.taskList) {
                    wrapperResObj.taskList = new List<Task>{taskItr};
                    continue;
                }

                wrapperResObj.taskList.add(taskItr);

            }

            return wrapperResObj;

        } catch(Exception e) {

            System.debug('exception :' + e);

        return createErrorMsg(e.getMessage(), null, taskStr);

        }

    }

    private static Wrappers.TaskResponseWrapper createErrorMsg(String msgStr, List<Task> taskListPar, String jsonStrPar) {

        Wrappers.TaskResponseWrapper wrapperResObj  = new Wrappers.TaskResponseWrapper();
        wrapperResObj.success                       = false;
        wrapperResObj.jsonStr                       = jsonStrPar;
        wrapperResObj.errorMsg                      = new List<String>{msgStr};
        wrapperResObj.taskList                      = taskListPar;
        wrapperResObj.timeStamp                     = DateTime.now();

        return wrapperResObj;

    }

}