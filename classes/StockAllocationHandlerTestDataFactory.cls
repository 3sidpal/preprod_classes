/**
* ==================================================================================
*    Version        Date                          Comment
* ==================================================================================
*    Initial       6-9-2018        Test class for Stock Allocation
*                                   functionality
*/
public with sharing class StockAllocationHandlerTestDataFactory {

    public static List<User> createAndInsertUser(Integer count) {
        Id profileId = [SELECT Id FROM Profile WHERE Name =: 'System Administrator'].Id;
        Id roleId = [SELECT Id FROM UserRole WHERE Name =: 'Executive'].Id;
        List<User> userList = new List<User>();
        for(Integer x = 0; x < count ; x++){
            User u = new User(
                ProfileId = profileId,
                LastName = 'smpfctestuser'+ String.valueOf(x),
                Email = 'smpfcTestUser'+ String.valueOf(x) +'@smpfc.com',
                Username = 'smpfcTestUser@smpfc.com' + String.valueOf(x) + System.currentTimeMillis(),
                CompanyName = 'SMPFC',
                Title = 'testTitle',
                Alias = 'smcU',
                TimeZoneSidKey = 'Asia/Manila',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                isActive = true,
                UserRoleId = roleId,
                EmployeeNumber = 'EMP :' + String.valueOf(x)
            );
            userList.add(u);
        }
        insert userlist;
        return userlist;
    }

    public static TriggerFlagControl__c createAndInsertTriggerFlagControl() {
        TriggerFlagControl__c settings = new TriggerFlagControl__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            reflectPicklistValue__c = true
        );
        insert settings;
        return settings;
    }

    public static List<Account> createAndInsertAccount(
        Integer count,
        Boolean isActive,
        String recordTypeName,
        String defaultServicePlan
    ) {
        List<Account> accounts = new List<Account>();
        Id recordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();

        for(Integer i = 0; i < count ; i++) {
            Account account = new Account();
            account.Name = 'Test ' + recordTypeName + ' Account ' + i + '-' + System.today() + System.currentTimeMillis();
            account.RecordTypeID = recordTypeId;
            account.Active__c = isActive;
            account.BU_Feeds__c = true;
            account.Branded__c = true;
            account.BU_Poultry__c = true;
            account.BU_Meats__c= true;
            account.GFS__c = true;
            account.BU_Poultry__c = true;
            account.BillingStreet = 'B Street';
            account.BillingCity='B City';
            account.ShippingStreet ='S Street';
            account.ShippingCity = 'S City';
            account.AccountNumber = '1234'+System.currentTimeMillis()+i;
            account.Poultry_Sales_Group__c = 'intercompany';
            //account.Default_Serving_Plant__c = defaultServicePlan + i;
            accounts.add(account);
        }
        insert accounts;
        return accounts;
    }

    public static List<Contact> createAndInsertContact(
        Integer count,
        Id accountId,
        String recordTypeName,
        List<User> users
    ) {
        Id recordTypeId =
            Schema.SObjectType.Contact.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        List<Contact> contacts = new List<Contact>();
        for(Integer x=0; x < count; x++) {
            Contact contact = new Contact();
            contact.RecordTypeId = recordTypeId;
            contact.AccountId = accountId;
            contact.LastName = 'Test Contact'+ x;
            contact.Birthdate = system.today();
            contact.HomePhone = '1';
            contact.Fax = '1';
            contact.Email = '1@1.com';
            contact.Active__c = true;
            contact.Employee_Number__c = users[0].EmployeeNumber;
            contacts.add(contact);
        }
        return contacts;
    }

    public static List<Market__c> createAndInsertMarket(Integer count, Boolean isActive) {
        List<Market__c> markets = new List<Market__c>();
        for (Integer i = 0; i < count; i++) {
            Market__c market = new Market__c();
            market.Name = 'FEEDS';
            market.Active__c = isActive;
            market.Market_Code__c = '11C';
            markets.add(market);
        }
        insert markets;
        return markets;
    }

    public static List<Ship_Sold_To__c> createAndInsertShipSoldTo(
        List<Account> accounts,
        Account childAccount
    ) {
        List<Ship_Sold_To__c> shipSoldToList = new List<Ship_Sold_To__c>();
        for(Account account : accounts) {
            Ship_Sold_To__c spTo = new Ship_Sold_To__c(
                AccountMaster__c = account.Id,
                AccountChild__c = childAccount != null ? childAccount.Id : account.Id,
                Ship_Sold_To__c = 'SP',
                Dataloader_ID__c = account.Id+'SP'
            );
            shipSoldToList.add(spTo);
        }
        insert shipSoldToList;
        return shipSoldToList;
    }

    public static List<Custom_Product__c> createAndInsertProducts(
        Integer count,
        Id marketId,
        String productName,
        String materialCode
    ) {
        List<Custom_Product__c> products = new List<Custom_Product__c>();
        for(Integer i=0; i<count; i++) {
            Custom_Product__c product = new Custom_Product__c();
            product.Name = 'Test Custom Product ' + productName + ' ' + i + '-' +
                System.today() + System.currentTimeMillis();
            product.Active__c = true;
            product.Feeds__c = true;
            product.GFS__c = true;
            product.Poultry__c = true;
            product.Branded__c = true;
            product.Loading_Group_Description__c = 'Premix';
            product.Material_Type__c = 'Material type';
            product.Market__c = marketId;
            product.SKU_Code__c = string.valueof(materialCode + i);
            products.add(product);
        }
        insert products;
        return products;
    }

    public static List<Plant_Determination__c> createAndInsertPlantDetermination(
        Integer count,
        List<Account> accounts,
        List<Custom_Product__c> products
    ) {
        List<Plant_Determination__c> plantDeterminations = new List<Plant_Determination__c>();
        for (Integer i = 0; i < count; i++) {
            Plant_Determination__c pDetermination = new Plant_Determination__c(
                Account__c = accounts[i].Id,
                CustomProduct__c = products[i].Id,
                Product_Status__c = 'Delisted'
            );
            plantDeterminations.add(pDetermination);
        }
        insert plantDeterminations;
        return plantDeterminations;
    }

    public static List<Order__c> createOrder(
        Integer count,
        Id accountId,
        Id sasId,
        Id shipSoldToId,
        String recordTypeName,
        String salesOrg
    ) {
        Id recordTypeId =
            Schema.sObjectType.Order__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        List<Order__c> orders = new List<Order__c>();
        Category__c category = new Category__c();
        category.Name = 'ANIMAL';
        category.Business_Unit__c = 'Feeds';
        category.SAP_Code__c = 'ANIMAL';
        insert category;

        for(Integer i=0; i<count; i++) {
            Order__c order = new Order__c();
            order.PO_No__c = '000' + i;
            order.RecordTypeId = recordTypeId;
            order.Account__c = accountId;
            order.SAS__c = sasId;
            order.Ship_Sold_To__c = shipSoldToId;
            order.Product_Category__c = category.id;
            order.Sales_Org__c = salesOrg;
            orders.add(order);
        }
        return orders;
    }

    public static List<Order_Item__c> createOrderItem(
        List<Custom_Product__c> products,
        Id orderId,
        String recordTypeName,
        Integer orderQuantity
    ) {
        Id recordTypeId =
            Schema.sObjectType.Order_Item__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        List<Order_Item__c> orderItemList = new List<Order_Item__c>();
        for(Integer i=0; i<products.size(); i++) {
            Order_Item__c orderItem = new Order_Item__c();
            orderItem.RecordTypeId = recordTypeId;
            orderItem.Order_Form__c = orderId;
            orderItem.Product__c = products[i].Id;
            orderItem.Price__c = 100;
            orderItem.Order_Quantity__c = orderQuantity;
            orderItem.Invoice_Quantity__c = 1;
            orderItem.Case_Fill_Rate_Target__c = 50;
            orderItem.Estimated_Amount__c = 100;
            orderItemList.add(orderItem);
        }
        return orderItemList;
    }

    public static List<Stock_Allocation__c> createStockAllocations(
        List<Account> accounts,
        Integer amount,
        Integer allocatedAmount,
        Integer capSize,
        Integer reAllocationLimit,
        List<Custom_Product__c> products,
        String productType,
        String materialCode,
        String servicePlantsCode
    ) {
        List<Stock_Allocation__c> listStockAllocation = new List<Stock_Allocation__c>();
        for (Integer i = 0; i < accounts.size(); i++)  {
            Stock_Allocation__c objStockAllocation = new Stock_Allocation__c();
            //objStockAllocation.Account__c = accounts[i].Id;
            objStockAllocation.Quantity__c = amount;
            objStockAllocation.Quantity_Allocated__c = allocatedAmount;
            //objStockAllocation.Brand_Name__c = 'Test Brand Name 0'+i;
            objStockAllocation.Product__c = products[i].Id;
            //objStockAllocation.Product_Type__c = productType;
            //objStockAllocation.Cap_Size__c = capSize;
            objStockAllocation.Start_Date__c = System.Today().addMonths(-1);
            objStockAllocation.End_Date__c = System.Today().addMonths(1);
            //objStockAllocation.Re_Allocation_Limit__c = reAllocationLimit;
            objStockAllocation.Material_Code__c = string.valueof(materialCode + i);
            // objStockAllocation.Serving_Plant__c = servicePlants[0].Id;
            objStockAllocation.Serving_Plant_Code__c = string.valueof(servicePlantsCode + i);
            listStockAllocation.add(objStockAllocation);
        }
        return listStockAllocation;
    }

    // public static List<Serving_Plant__c> createServingPlants(Integer noOfRecords) {
    //     List<Serving_Plant__c> listServingPlant = new List<Serving_Plant__c>();
    //     for (Integer i = 0; i < noOfRecords; i++) {
    //         Serving_Plant__c objServingPlant = new Serving_Plant__c();
    //         objServingPlant.Name = 'Test Plant' +i;
    //         listServingPlant.add(objServingPlant);
    //     }
    //     return listServingPlant;
    // }
}