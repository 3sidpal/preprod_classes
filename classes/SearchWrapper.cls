public class SearchWrapper{
        public String searchStr {get; set;}
        public Date stDate {get; set;}
        public Date enDate {get; set;}
        public String sortedField {get; set;}
        public String sortedOrder {get; set;}
        public String selectedOppId {get; set;}
        //public String fldApiNmsToEdit{get;set;}
        public String activeTab{get;set;}
        public OrderSummarySettings__c oliCsDet{set;get;}
        public Integer offsetValue{get;set;}
        public boolean printPDFmode{set;get;}
        //code change Aditi - 21/6/2017
        public Integer totalRowSize{get;set;}
        //code change ends
        public String fieldsetName{set;get;}
        public integer counter{get;set;}
        public integer pageSize{get;set;}

        public SearchWrapper() {
            offsetValue= 0;
            printPDFmode = false;
            counter= 0;
            pageSize = Integer.valueOf(Label.pageSize);
        }

        public SearchWrapper(String searchString, Date startDate, Date endDate, String sortField, String sortOrder) {
            searchStr = searchString ;
            stDate = startDate ;
            enDate = endDate ;
            sortedField = sortField ;
            sortedOrder = sortOrder ;
            offsetValue = 0;
            printPDFmode=false;
            counter= 0;
            pageSize = Integer.valueOf(Label.pageSize);
            //oliCsDet = OLI_Fields_To_Editable__c.getAll().get(searchStr);
            /*List<OLI_Fields_To_Editable__c> olifieldsToEdit = [select id,name,Field_Names__c from OLI_Fields_To_Editable__c];
            if(olifieldsToEdit.size()>0){

            }*/
           // fldApiNmsToEdit = 'Quantity,UnitPrice';
            
        }
        
        /*public SearchWrapper(String searchString, Date startDate, Date endDate, String sortField, String sortOrder, String activeTb) {
            searchStr = searchString ;
            stDate = startDate ;
            enDate = endDate ;
            sortedField = sortField ;
            sortedOrder = sortOrder ;
            activeTab = activeTb ;
        }*/
        
    }