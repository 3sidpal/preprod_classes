@isTest

public class InsertAuditLogDataserviceTest {


    public static testMethod void  doPostTest() {
        
        String jsonStr = '[{"attributes":{"type":"Audit_Log__c","url":"/services/data/v45.0/sobjects/Audit_Log__c/a1Sp0000000qAZNEA2"},"Account__c":"0012800001V88VEAAZ","Android_Version__c":"28","CreatedById":"0050I000008DLmjQAG","CreatedDate":"2018-12-19T09:47:14.000+0000","IsDeleted":false,"Item_Count__c":30,"LastModifiedById":"0050I000008DLmjQAG","LastModifiedDate":"2018-12-19T09:47:14.000+0000","Make__c":"HUAWEI","Model__c":"HMA-L29","Name":"AL-00000010","Order_Date__c":"2018-12-19T00:00:00.000+0000","OwnerId":"0050I000008DLmjQAG","Purchase_Order_Number__c":"rdd03","Requested_Delivery_Date__c":"2018-12-21","Request_Time__c":"2018-12-19T17:47:09.000+0000","Response_Time__c":"2018-12-19T17:47:14.000+0000","Source_of_Sync__c":"Landing Page","Source__c":"SMS","SystemModstamp":"2018-12-19T09:47:14.000+0000","User__c":"0050I000008DLmjQAG"},{"attributes":{"type":"Audit_Log__c","url":"/services/data/v45.0/sobjects/Audit_Log__c/a1Sp0000000qAZSEA2"},"Account__c":"0012800001V88VEAAZ","Android_Version__c":"28","CreatedById":"0050I000008DLmjQAG","CreatedDate":"2018-12-19T09:47:46.000+0000","IsDeleted":false,"Item_Count__c":30,"LastModifiedById":"0050I000008DLmjQAG","LastModifiedDate":"2018-12-19T09:47:46.000+0000","Make__c":"HUAWEI","Model__c":"HMA-L29","Name":"AL-00000011","Order_Date__c":"2018-12-19T00:00:00.000+0000","OwnerId":"0050I000008DLmjQAG","Purchase_Order_Number__c":"rad001","Requested_Delivery_Date__c":"2018-12-21","Request_Time__c":"2018-12-19T17:47:40.000+0000","Response_Time__c":"2018-12-19T17:47:46.000+0000","Source_of_Sync__c":"Landing Page","Source__c":"SMS","SystemModstamp":"2018-12-19T09:47:46.000+0000","User__c":"0050I000008DLmjQAG"}]';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertAuditLogData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;
        
        System.Test.startTest();
            InsertAuditLogDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Audit_Log__c].size());

    }

    public static testMethod void  doPostTestFail() {

        String jsonStr=        'wrong json';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertAuditLogData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;

        System.Test.startTest();
            InsertAuditLogDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Audit_Log__c].size());

    }

    public static testMethod void  doPostTestFail2() {

        String jsonStr = '[{"attributes":{"type":"Audit_Log__c","url":"/services/data/v45.0/sobjects/Audit_Log__c/a1Sp0000000qAZNEA2"},"Account__c":"0012800001V88VEAAZ","Android_Version__c":"28","CreatedById":"0050I000008DLmjQAG","CreatedDate":"2018-12-19T09:47:14.000+0000","IsDeleted":false,"Item_Count__c":30,"LastModifiedById":"0050I000008DLmjQAG","LastModifiedDate":"2018-12-19T09:47:14.000+0000","Make__c":"HUAWEI","Model__c":"HMA-L29","Name":"AL-00000010","Order_Date__c":"2018-12-19T00:00:00.000+0000","OwnerId":"0050I000008DLmjQAG","Purchase_Order_Number__c":"rdd03","Requested_Delivery_Date__c":"2018-12-21","Request_Time__c":"2018-12-19T17:47:09.000+0000","Response_Time__c":"2018-12-19T17:47:14.000+0000","Source_of_Sync__c":"Landing Page","Source__c":"SMS","SystemModstamp":"2018-12-19T09:47:14.000+0000","User__c":"0050I000008DLmjQAG"},{"attributes":{"type":"Audit_Log__c","url":"/services/data/v45.0/sobjects/Audit_Log__c/a1Sp0000000qAZSEA2"},"Account__c":"0012800001V88VEAAZ","Android_Version__c":"28","CreatedById":"0050I000008DLmjQAG","CreatedDate":"2018-12-19T09:47:46.000+0000","IsDeleted":false,"Item_Count__c":30,"LastModifiedById":"0050I000008DLmjQAG","LastModifiedDate":"2018-12-19T09:47:46.000+0000","Make__c":"HUAWEI","Model__c":"HMA-L29","Name":"AL-00000011","Order_Date__c":"2018-12-19T00:00:00.000+0000","OwnerId":"0050I000008DLmjQAG","Purchase_Order_Number__c":"rad001","Requested_Delivery_Date__c":"2018-12-21","Request_Time__c":"2018-12-19T17:47:40.000+0000","Response_Time__c":"2018-12-19T17:47:46.000+0000","Source_of_Sync__c":"Landing Page","Source__c":"SMS","SystemModstamp":"2018-12-19T09:47:46.000+0000","User__c":"wrongUser"}]';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertAuditLogData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;

        System.Test.startTest();
            InsertAuditLogDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Audit_Log__c].size());

    }
}