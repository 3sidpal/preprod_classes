global class McpSubmissionReportController {
    
    @remoteAction
    global static List<User> getMcpNotSubmittedUsers(){
        /*
            Schema.FieldSet fs1 = Schema.SObjectType.User.fieldSets.getMap().get('MCP_Not_Submitted_User_Details');
            
            String query = 'Seelct Id';
            for(Schema.FieldSetMember objFldMem : fs1.getFields()){
                query += ','+objFldMem.getFieldPath();
            }
            query += ' From User Where Submitted_for_Approval__c = False';
            
            return Database.query(query);
        */
        
        return [SELECT Name,Email FROM User 
                                  WHERE Id IN ( SELECT 
                                                OwnerId 
                                                FROM Month__c 
                                                WHERE Submitted_for_Approval__c = False) 
                                  AND ManagerId = :System.UserInfo.getUserId()               //Id = :System.UserInfo.getUserId() OR
                ];// 
    }
}