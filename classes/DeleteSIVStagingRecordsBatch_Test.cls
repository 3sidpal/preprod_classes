/**
    * AUTHOR        : LENNARD PAUL M SANTOS(DELOITTE)
    * DESCRIPTION   : TEST CLASS FOR BATCH THAT DELETES SIV STAGING RECORDS.
    * HISTORY       : (ANY UPDATES TO THE CODE MUST BE LOGGED HERE)
                    : JUN.24.2016 - CREATED.
**/
@isTest
private class DeleteSIVStagingRecordsBatch_Test{
    @testSetup static void setupData(){
        List<SIV_Staging__c> sivStagingForInsertList = new List<SIV_Staging__c>();
        for(integer i = 0; i<100; i++){
            SIV_Staging__c siv = new SIV_Staging__c(
                UOM__c = 'KG',
                Reference_No__c = System.now().millisecond() + '-' + i,
                Customer__c = 'test' , Material__c = 'test', COPA__c = 'test' + i);
            sivStagingForInsertList.add(siv);
        }
        insert sivStagingForInsertList;
        
    }

    static testMethod void testDeleteSIVStagingRecords() {
        Test.StartTest();   
            Id batchprocessid = Database.executeBatch(new DeleteSIVStagingRecords_Batch ());
        Test.StopTest();
   
    }
    
    static testMethod void testSchedulableInterface(){
        Test.startTest();
            String schedStr = '0 0 0 15 3 ? 2022';
            String jobId = System.schedule('TestSchedDeleteSIVStagingRecords_Batch',schedStr,new DeleteSIVStagingRecords_Batch());
        Test.stopTest();
    }
}