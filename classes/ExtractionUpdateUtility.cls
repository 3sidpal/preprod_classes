/*-------------------------------------------------------------------------------------------
Author       :   Kimiko Roberto
Created Date :   05.11.2017
Definition   :   Controller for SalesOrder page
History      :   05.11.2017 - Kiko Roberto:  Created
-------------------------------------------------------------------------------------------*/
public class ExtractionUpdateUtility{
    public String headerFields(String strVar){
        String strFields = 
            'SALES TYPE, '+
            'SIF ID, '+
            'BUSINESS UNIT ID, '+
            'BUSINESS UNIT NAME, '+
            'DISTRIBUTOR ID, '+
            'DISTRIBUTOR, '+ 
            'ACCOUNT ID, '+
            'ACCOUNT, '+
            'SAS ID, '+
            'SAS, '+
            'DSP ID, '+
            'DSP, '+
            'SALES ORDER NUMBER, '+
            'ORDER BOOKING DATE, '+
            'REQUESTED DELIVERY DATE, '+
            'PAYMENT TERM, '+
            'ORDER ITEM ID, '+
            'ORDER NUMBER, '+
            'PRODUCT CODE, '+
            'PRODUCT, '+
            'UOM, '+
            'CONVERSION, '+
            'ORDER QUANTITY, '+
            'ORDER PRICE, '+
            'ORDER GROSS AMOUNT, '+
            'ORDER DISCOUNT, '+
            'ORDER VAT, '+
            'ORDER NET AMOUNT, '+
            'INVOICE NUMBER, '+
            'INVOICE DATE, '+
            'INVOICE QUANTITY, '+
            'INVOICE PRICE, '+
            'INVOICE GROSS AMOUNT, '+
            'INVOICE DISCOUNT, '+
            'INVOICE VAT, '+
            
            'INVOICE NET AMOUNT';
        if(strVar != '')  strFields += ', '+strVar;
            
            strFields += ', BUSINESS UNIT ID';
            /*strFields += ', REASON FOR OVER INVOICE';*/
            
        
        return strFields;
    }
    
    public Integer getHeaderCount(){
        return headerFields('').split(',').size();
    } 
    
    public List<SObject> dynamicQuery(String objName, Set<String> recordIds, String whereClause, List<String> addFields) {
        
        List<SObject> objectQuery = database.query(createQuery(objName, recordIds, whereClause, addFields));
        return objectQuery;
    }
    
    public String createQuery(String objName, Set<String> recordIds, String whereClause, List<String> addFields) {

        String query = 'SELECT ';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        for(String s : objectFields.keySet()) {
        
            if(!s.Contains('__pc')) {
                query += ' ' + s + ',';
            }
        }
        
        if(!addFields.isEmpty()) {
            integer ctr=0;
            for(String r: addFields) {
                ctr++;
                query += r;
                if(addFields.size() != ctr)
                query += ', ';
            }
        }

        if (query.subString(query.Length()-1,query.Length()) == ',') {
            query = query.subString(0,query.Length()-1);
        }
        query += ' FROM ' + objName;
        if(whereClause != '')
        query += ' WHERE '+ whereClause;

        if(!recordIds.isEmpty()){
            query += ' IN: recordIds';
        }
        
        if(objName == 'Custom_Product__c') {
            if(!query.contains('ORDER BY')) {
                query += ' ORDER BY WetMarket_Shortlist__c DESC, WetMarketShortlistSequence__c';
            }
        }

        return query;
    }
    
    public User getUserBU(){
        User u = new User();
        u = [SELECT Branded__c, Poultry__c, Feeds__c, GFS__c, Meats__c, EmployeeNumber, Business_Unit__c FROM User WHERE Id =: UserInfo.getUserId()]; //GAAC: 1/6/2018 - Added EmployeeNumber
        return u;
    }
    
    // Generate filter criteria based on Account BU
    public String getAccountBU(Id accntId){
        Account accnt = new Account();
        String whereAs = '';
        accnt = [SELECT Branded__c, BU_Feeds__c, GFS__c, BU_Poultry__c, BU_Meats__c FROM Account WHERE Id =: accntId];
        whereAs += accnt.Branded__c ? 'AND (Branded__c = TRUE' : '';
        whereAs += whereAs != '' ? accnt.BU_Feeds__c ? ' OR Feeds__c = TRUE' : '' : accnt.BU_Feeds__c ? 'AND (Feeds__c = TRUE' : '';
        whereAs += whereAs != '' ? accnt.GFS__c ? ' OR GFS__c = TRUE' : '' : accnt.GFS__c ? 'AND (GFS__c = TRUE' : '';
        whereAs += whereAs != '' ? accnt.BU_Poultry__c ? ' OR Poultry__c = TRUE' : '' : accnt.BU_Poultry__c ? 'AND (Poultry__c = TRUE' : '';   
        whereAs += whereAs != '' ? accnt.BU_Meats__c ? ' OR Meats__c = TRUE' : '' : accnt.BU_Meats__c ? 'AND (Meats__c = TRUE' : '';       
        whereAs += ') ';

        return whereAs;
    }
    
    
    public String getUserProfile(){
        return [SELECT Name FROM Profile WHERE Id =: UserInfo.getProfileId()].Name;
    }

    public DateTime getDateTimeValue(DateTime cutDT){
        Integer cdtYear = cutDT.year();
        Integer cdtMonth = cutDT.month();
        Integer cdtDay = cutDT.day();
        Integer cdtHour = cutDT.hour();
        Integer cdtMinute = cutDT.minute();
        Integer cdtSecond = cutDT.second();
        return DateTime.newInstanceGMT(cdtYear, cdtMonth, cdtDay, cdtHour, cdtMinute, cdtSecond);
    }
}