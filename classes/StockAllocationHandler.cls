/**
* ==================================================================================
*    Version        Date                          Comment
* ==================================================================================
*    Initial       27-8-2018        Handler class for Stock Allocation
*                                   functionality
*/

public class StockAllocationHandler {
    public static boolean isRunning = false;

    /**
     * method to validate and process Serving Plant Change Orders
     */
    public void validateServingPlantChange(List<Order__c> listNewOrder, Map<Id, Order__c> mapOldOrder) {
        //StockAllocationHelperClass objStockAllocationHelperClass = new StockAllocationHelperClass();
        //objStockAllocationHelperClass.validateServingPlantChange(listNewOrder, mapOldOrder);
    }

    /**
     * future method to validate Serving Plant
     */
    public static void validateServingPlantChangeFuture(Set<Id> newOrderIds, Set<Id> oldOrderIds){
        //StockAllocationHelperClass.validateServingPlantChangeFuture(newOrderIds, oldOrderIds);
    }

    /**
     * method to process Order Items
     */
    public void processOrderItems(
        List<Order_Item__c> listOrderItems,
        Boolean processForOrderItemTrigger
    ) {
        //StockAllocationHelperClass objStockAllocationHelperClass = new StockAllocationHelperClass();
        //objStockAllocationHelperClass.processOrderItems(listOrderItems, processForOrderItemTrigger);
    }

    /**
     * future method getting called from this method.
     */
    public static void processOrderItemsFuture(Set<Id> orderItemIds){
        //isRunning = true;
        //StockAllocationHelperClass.processOrderItemsFuture(orderItemIds);
    }
}