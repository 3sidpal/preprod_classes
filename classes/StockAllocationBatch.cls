public class StockAllocationBatch implements Database.Batchable<SObject>, Schedulable, Database.Stateful{
    
    public class AllocationStatusWrapper {
        Order_Item__c orderItem;
        String StatusMessage;
        Boolean isValid;
        Decimal stocksConsumed;
        public AllocationStatusWrapper(Order_Item__c orderItem) {
            isValid = false;
            this.orderItem = orderItem;
        }
    }
    
    /*
     *Stateful Variables
    */
    List<Order__c> orderInst;
    Map<String,Stock_Allocation__c> stockAllocationsMap;
    Map<String,AllocationStatusWrapper> stockAllocationsWrapperMap;
    Map<String,Map<String,Conversion__c>> conversionsMap;
    Map<String,Stock_Allocation_Limit__c> mapAllocationLimit;
    /*
     *Static Final Variables
    */
    public static final String READY = 'Ready';
    public static final String PROCESSING = 'Processing';
    public static final String CONV_ERROR = 'Cannot find/invalid Conversion record.';
    public static final String UNAVAILABLE_ERROR = 'Stocks not available';
    
    //Constructor
    public StockAllocationBatch(Stock_Allocation_Queue__c queues) {
        orderInst = [SELECT Id,
                            Account__c,
                            Account__r.AccountNumber,
                            Account__r.CustomerGroup1__c,
                            Account__r.CustomerGroup1__r.Name,
                            Account__r.CustomerGroup2__c,
                            Account__r.CustomerGroup2__r.Name,
                            Account__r.General_Trade__c,
                            Account__r.Modern_Trade__c,
                            Account__r.Intercompany__c,
                            Account__r.Institutional__c,
                            Account__r.Export__c
                     FROM Order__c
                     WHERE Id = :queues.Order__c];
        
        List<Order_Item__c> orderItems;
        if(orderInst.isEmpty() == false) {
            orderItems = [SELECT Id,
                                 Product__c,
                                 Product__r.SellingUOM__c,
                                 Product__r.SKU_Code__c,
                                 Product__r.Product_Hierarchy_Level_1__c,
                                 Product__r.Product_Hierarchy_Level_2__c,
                                 Product__r.Product_Hierarchy_Level_3__c,
                                 Product__r.Product_Hierarchy_Level_4__c,
                                 Product__r.Product_Hierarchy_Level_5__c,
                                 Product__r.Product_Hierarchy_Level_6__c
                          FROM Order_Item__c
                          WHERE Order_Form__c = :orderInst[0].Id];
        }
        
        if(orderItems.isEmpty() == false) {
            stockAllocationsMap = new Map<String,Stock_Allocation__c>();
            stockAllocationsWrapperMap = new Map<String,AllocationStatusWrapper>();
            for(Order_Item__c orderItem : orderItems) {
                stockAllocationsMap.put(orderItem.Product__c,null);
                stockAllocationsWrapperMap.put(orderItem.Id, new AllocationStatusWrapper(orderItem));
            }
        }
        
    }
    public Database.querylocator start(Database.BatchableContext bc){
        if(stockAllocationsWrapperMap.isEmpty() == false && stockAllocationsMap.isEmpty() == false) {
            //check cap limit
            //check availability
            stockAvailabilityCheck(bc.getJobId());
            //fetch allocation limit record per order item
            populateAllocationlimitRecord();
            
            return Database.getQueryLocator(formQuery());
        }
        else {
            return null;
        }
    }
    public void execute(Database.BatchableContext bc,List<Account_Product_Allocation__c> scope){
        
    }
    public void finish(Database.BatchableContext bc){
        
    }
    
    public void execute(SchedulableContext sc) {
        Set<String> queueStatusSet = new Set<String>{READY,PROCESSING};
        List<Stock_Allocation_Queue__c> queues = [SELECT Order__c,
                                                         Status__c
                                                  FROM Stock_Allocation_Queue__c
                                                  WHERE Status__c IN :queueStatusSet
                                                  ORDER BY Name DESC
                                                  LIMIT 2];
        if(queues.isEmpty() == false && 
           queues.size() != 2 || 
           (queues.size() == 1 && queues[0].Status__c != PROCESSING)) {
            System.scheduleBatch(new StockAllocationBatch(queues[0]), 'StockAllocationBatch'+System.Now(), 1);
        }
    }
    
    public String formQuery() {
        Account accountInst = orderInst[0].Account__r;
        
        Map<String, List<String>> allocLimitPriorityMap = new Map<String, List<String>>();
        fetchAllocationLimitPriorities(allocLimitPriorityMap);
        
        String queryStr = 'SELECT Id,Account__c,Account__r.AccountNumber,Allocated_Quantity__c,Allocation_Date__c,'+
                                  'Custom_Product__c,Custom_Product__r.SKU_Code__c,Product_Type__c,Stock_Allocation__c,Key__c '+
                          'FROM Account_Product_Allocation__c '+
                          'WHERE ';
        if(mapAllocationLimit.isEmpty() == false) {
            if(mapAllocationLimit.containskey('Priority1')) {
                List<String> lstMaterialCodes = new List<String>();
                for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
                    Order_Item__c ordLn = stockAllocationsWrapperMap.get(strOrderId).orderItem;
                    lstMaterialCodes.add(ordLn.Product__r.SKU_Code__c);
                }
                queryStr += 'Account__r.AccountNumber =\''+accountInst.AccountNumber+'\' AND ';
                queryStr += 'Custom_Product__r.SKU_Code__c IN (\''+String.join(lstMaterialCodes,'\',\'')+'\')';
            }
            else if(mapAllocationLimit.containskey('Priority2') && allocLimitPriorityMap.containsKey('product_hierarchy')) {
                List<String> lstProdHierarchies = new List<String>();
                for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
                    Order_Item__c ordLn = stockAllocationsWrapperMap.get(strOrderId).orderItem;
                    for(String fld : allocLimitPriorityMap.get('product_hierarchy')) {
                        if(ordLn.Product__r.get(fld) != null) {
                            lstProdHierarchies.add(String.valueOf(ordLn.Product__r.get(fld)));
                        }
                    }
                }
                queryStr += 'Account__r.AccountNumber =\''+accountInst.AccountNumber+'\' AND (';
                for(String fld : allocLimitPriorityMap.get('product_hierarchy')) {
                    queryStr += ' Custom_Product__r.'+fld+' IN (\''+String.join(lstProdHierarchies,'\',\'')+'\') OR ';
                }
                queryStr = queryStr.removeEnd('OR');
                queryStr += ')';
            }
            else if(mapAllocationLimit.containskey('Priority3')) {
                queryStr += 'Account__r.CustomerGroup1__r.Name =\''+accountInst.AccountNumber+'\' AND (';
            }
            else if(mapAllocationLimit.containskey('Priority4')) {
                
            }
            else if(mapAllocationLimit.containskey('Priority5')) {
                
            }
            else if(mapAllocationLimit.containskey('Priority6')) {
                
            }
        }
        else {
            queryStr += ' Custom_Product__c IN (\''+String.Join(new List<String>(stockAllocationsMap.keySet()),'\',\'')+'\') AND ';
            queryStr += ' Account__c = \''+accountInst.Id+'\'';
        }
        return queryStr;
    }
    public void collectCombinationKeys(Set<String> combinationKeys) {
        Account accountInst = orderInst[0].Account__r;
        for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
            Order_Item__c ordLn = stockAllocationsWrapperMap.get(strOrderId).orderItem;
            /**********************priority 1**********************/
            combinationKeys.add(accountInst.AccountNumber+'_'+ordLn.Product__r.SKU_Code__c);
            /**********************priority 2**********************/
            getKey(accountInst.AccountNumber, ordLn.Product__r, combinationKeys);
            if(accountInst.CustomerGroup1__c != null) {
                /**********************priority3**********************/
                combinationKeys.add(accountInst.CustomerGroup1__r.Name+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority 4**********************/
                getKey(accountInst.CustomerGroup1__r.Name, ordLn.Product__r, combinationKeys);
            }
            if(accountInst.CustomerGroup2__c != null) {
                /**********************priority3**********************/
                combinationKeys.add(accountInst.CustomerGroup2__r.Name+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority 4**********************/
                getKey(accountInst.CustomerGroup2__r.Name, ordLn.Product__r, combinationKeys);
            }
            
            if(accountInst.General_Trade__c == true) {
                /**********************priority5**********************/
                combinationKeys.add('General Trade'+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority6**********************/
                getKey('General Trade', ordLn.Product__r, combinationKeys);
            }
            if(accountInst.Modern_Trade__c == true) {
                /**********************priority5**********************/
                combinationKeys.add('Modern Trade'+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority6**********************/
                getKey('Modern Trade', ordLn.Product__r, combinationKeys);
            }
            if(accountInst.Intercompany__c == true) {
                /**********************priority5**********************/
                combinationKeys.add('Intercompany'+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority6**********************/
                getKey('Intercompany', ordLn.Product__r, combinationKeys);
            }
            if(accountInst.Institutional__c == true) {
                /**********************priority5**********************/
                combinationKeys.add('Institutional'+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority6**********************/
                getKey('Institutional', ordLn.Product__r, combinationKeys);
            }
            if(accountInst.Export__c == true) {
                /**********************priority5**********************/
                combinationKeys.add('Export'+'_'+ordLn.Product__r.SKU_Code__c);
                /**********************priority6**********************/
                getKey('Export', ordLn.Product__r, combinationKeys);
            }
            
        }
    }
    public void fetchPriorityWiseAllocationLimits(Map<String, Stock_Allocation_Limit__c> mapPriority1,
        Map<String, Stock_Allocation_Limit__c> mapPriority2,
        Map<String, Stock_Allocation_Limit__c> mapPriority3,
        Map<String, Stock_Allocation_Limit__c> mapPriority4,
        Map<String, Stock_Allocation_Limit__c> mapPriority5,
        Map<String, Stock_Allocation_Limit__c> mapPriority6,
        Set<String> combinationKeys) {
        
        List<Stock_Allocation_Limit__c> stockAllocationLimits = [SELECT Id,
                                                                        Allocation_Limit__c,
                                                                        Percentage_for_Re_Allocation__c,
                                                                        UOM__c
                                                                 FROM Stock_Allocation_Limit__c
                                                                 WHERE Combination_Key_Priority_1__c IN: combinationKeys OR
                                                                    Combination_Key_Priority_2__c IN: combinationKeys OR
                                                                    Combination_Key_Priority_3__c IN: combinationKeys OR
                                                                    Combination_Key_Priority_4__c IN: combinationKeys OR
                                                                    Combination_Key_Priority_5__c IN: combinationKeys OR
                                                                    Combination_Key_Priority_6__c IN: combinationKeys];
        for(Stock_Allocation_Limit__c stockAllocationLimitInst : stockAllocationLimits) {
            if(stockAllocationLimitInst.Combination_Key_Priority_1__c != null) {
                mapPriority1.put(stockAllocationLimitInst.Combination_Key_Priority_1__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
            if(stockAllocationLimitInst.Combination_Key_Priority_2__c != null) {
                mapPriority2.put(stockAllocationLimitInst.Combination_Key_Priority_2__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
            if(stockAllocationLimitInst.Combination_Key_Priority_3__c != null) {
                mapPriority3.put(stockAllocationLimitInst.Combination_Key_Priority_3__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
            if(stockAllocationLimitInst.Combination_Key_Priority_4__c != null) {
                mapPriority4.put(stockAllocationLimitInst.Combination_Key_Priority_4__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
            if(stockAllocationLimitInst.Combination_Key_Priority_5__c != null) {
                mapPriority5.put(stockAllocationLimitInst.Combination_Key_Priority_5__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
            if(stockAllocationLimitInst.Combination_Key_Priority_6__c != null) {
                mapPriority6.put(stockAllocationLimitInst.Combination_Key_Priority_6__c.toLowerCase(),
                    stockAllocationLimitInst);
            }
        }
    }
    
    public void fetchAllocationLimitPriorities(Map<String, List<String>> allocLimitPriorityMap) {
        List<Stock_Allocations_Priority__mdt> allocnPriorities = [SELECT Id,
                                                                         DeveloperName,
                                                                         Field_API_Names__c
                                                                  FROM Stock_Allocations_Priority__mdt
                                                                  WHERE Field_API_Names__c != null];
        
        for(Stock_Allocations_Priority__mdt allocPriorityObj : allocnPriorities) {
            allocLimitPriorityMap.put(allocPriorityObj.DeveloperName.toLowerCase(),
                new List<String>());
            for(String strFld : allocPriorityObj.Field_API_Names__c.split(',')) {
                allocLimitPriorityMap.get(allocPriorityObj.DeveloperName.toLowerCase()).add(strFld.trim());
            }
        }
    }
    
    public void populateAllocationlimitRecord() {
        Account accountInst = orderInst[0].Account__r;
        Set<String> combinationKeys = new Set<String>();
        collectCombinationKeys(combinationKeys);
        Map<String, Stock_Allocation_Limit__c> mapPriority1 = new Map<String, Stock_Allocation_Limit__c>();
        Map<String, Stock_Allocation_Limit__c> mapPriority2 = new Map<String, Stock_Allocation_Limit__c>();
        Map<String, Stock_Allocation_Limit__c> mapPriority3 = new Map<String, Stock_Allocation_Limit__c>();
        Map<String, Stock_Allocation_Limit__c> mapPriority4 = new Map<String, Stock_Allocation_Limit__c>();
        Map<String, Stock_Allocation_Limit__c> mapPriority5 = new Map<String, Stock_Allocation_Limit__c>();
        Map<String, Stock_Allocation_Limit__c> mapPriority6 = new Map<String, Stock_Allocation_Limit__c>();
        
        fetchPriorityWiseAllocationLimits(mapPriority1,mapPriority2,mapPriority3,
            mapPriority4,mapPriority5,mapPriority6,combinationKeys);
        
        Map<String, List<String>> allocLimitPriorityMap = new Map<String, List<String>>();
        fetchAllocationLimitPriorities(allocLimitPriorityMap);
        system.debug('=====allocLimitPriorityMap====='+allocLimitPriorityMap);
        mapAllocationLimit = new Map<String,Stock_Allocation_Limit__c>();
        
        for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
            
            Order_Item__c ordLn = stockAllocationsWrapperMap.get(strOrderId).orderItem;
            
            /**********************priority 1**********************/
            String combinationKey = accountInst.AccountNumber+'_'+ordLn.Product__r.SKU_Code__c;
            combinationKey = combinationKey.toLowerCase();
            if(mapPriority1.containsKey(combinationKey)) {
                mapAllocationLimit.put('Priority1', mapPriority1.get(combinationKey));
                continue;
            }
            /**********************priority 2**********************/
            if(allocLimitPriorityMap.containsKey('product_hierarchy')) {
                Boolean continueBool = false;
                combinationKey = '';
                for(String strFld : allocLimitPriorityMap.get('product_hierarchy')) {
                    combinationKey = (accountInst.AccountNumber + '_' +ordLn.Product__r.get(strFld)).toLowerCase();
                    if(mapPriority2.containsKey(combinationKey)) {
                        continueBool = true;
                        mapAllocationLimit.put('Priority2', mapPriority2.get(combinationKey));
                        break;
                    }
                }
                if(continueBool) {
                    continue;
                }
            }
            
            /**********************priority 3 & 4**********************/
            
            if(allocLimitPriorityMap.containsKey('customer_group')) {
                combinationKey = '';
                Boolean continueBool;
                for(String strFld : allocLimitPriorityMap.get('customer_group')) {
                    if(accountInst.getSobject(strFld) != null) {
                        combinationKey = accountInst.getSobject(strFld).get('Name')+'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    if(mapPriority3.containsKey(combinationKey)) {
                        mapAllocationLimit.put('Priority3', mapPriority3.get(combinationKey));
                        continueBool = true;
                        break;
                    }
                    
                    if(allocLimitPriorityMap.containsKey('product_hierarchy')) {
                        continueBool = false;
                        combinationKey = '';
                        for(String strFldProdHierarchy : allocLimitPriorityMap.get('product_hierarchy')) {
                            if(accountInst.getSobject(strFld) != null) {
                                combinationKey = (accountInst.getSobject(strFld).get('Name') + '_' +ordLn.Product__r.get(strFldProdHierarchy));
                                combinationKey = combinationKey.toLowerCase();
                            }
                            
                            if(mapPriority4.containsKey(combinationKey)) {
                                continueBool = true;
                                mapAllocationLimit.put('Priority4', mapPriority4.get(combinationKey));
                                break;
                            }
                        }
                        if(continueBool) {
                            break;
                        }
                    }
                }
                if(continueBool) {
                    continue;
                }
            }
            /*************************priority5 & 6****************************/
            if(allocLimitPriorityMap.containsKey('distribution_channel')) {
                combinationKey = '';
                Boolean continueBool;
                for(String strFld : allocLimitPriorityMap.get('distribution_channel')) {
                    String keyPrefix = '';
                    if(strFld == 'General_Trade__c' && accountInst.get(strFld) == true) {
                        keyPrefix = 'General Trade';
                        combinationKey = keyPrefix +'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    else if(strFld == 'Modern_Trade__c' && accountInst.get(strFld) == true) {
                        keyPrefix = 'Modern Trade';
                        combinationKey = keyPrefix +'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    else if(strFld == 'Intercompany__c' && accountInst.get(strFld) == true) {
                        keyPrefix = 'Intercompany';
                        combinationKey = keyPrefix +'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    else if(strFld == 'Institutional__c' && accountInst.get(strFld) == true) {
                        keyPrefix = 'Institutional';
                        combinationKey = keyPrefix +'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    else if(strFld == 'Export__c' && accountInst.get(strFld) == true) {
                        keyPrefix = 'Export';
                        combinationKey = keyPrefix +'_'+ordLn.Product__r.SKU_Code__c;
                        combinationKey = combinationKey.toLowerCase();
                    }
                    if(mapPriority5.containsKey(combinationKey)) {
                        continueBool = true;
                        mapAllocationLimit.put('Priority5', mapPriority5.get(combinationKey));
                        break;
                    }
                    if(allocLimitPriorityMap.containsKey('product_hierarchy')) {
                        continueBool = false;
                        combinationKey = '';
                        for(String strFldProdHierarchy : allocLimitPriorityMap.get('product_hierarchy')) {
                            combinationKey = (keyPrefix + '_' + ordLn.Product__r.get(strFldProdHierarchy)).toLowerCase();
                            if(mapPriority6.containsKey(combinationKey)) {
                                continueBool = true;
                                mapAllocationLimit.put('Priority6', mapPriority6.get(combinationKey));
                                break;
                            }
                        }
                    }
                    if(continueBool) {
                        break;
                    }
                    
                }
                if(continueBool) {
                    continue;
                }
            }
        }
    }
    public void getKey(String strPrefix, Custom_Product__c cp, Set<String> combinationKeys) {
        if(cp.Product_Hierarchy_Level_1__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_1__c);
        }
        if(cp.Product_Hierarchy_Level_2__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_2__c);
        }
        if(cp.Product_Hierarchy_Level_3__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_3__c);
        }
        if(cp.Product_Hierarchy_Level_4__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_4__c);
        }
        if(cp.Product_Hierarchy_Level_5__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_5__c);
        }
        if(cp.Product_Hierarchy_Level_6__c != null) {
            combinationKeys.add(strPrefix+'_'+cp.Product_Hierarchy_Level_6__c);
        }
    }
    // Availability Check
    public void stockAvailabilityCheck(String batchId) {
        getStockAllocaionsMap();
        fetchConversions();
        for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
            Order_Item__c orderItem = stockAllocationsWrapperMap.get(strOrderId).orderItem;
            if(orderItem.Product__r.SellingUOM__c != null &&
                conversionsMap.isEmpty() == false &&
                conversionsMap.containsKey(orderItem.Product__c) &&
                conversionsMap.get(orderItem.Product__c).isEmpty() == false &&
                conversionsMap.get(orderItem.Product__c).containsKey(orderItem.Product__r.SellingUOM__c) &&
                
                stockAllocationsMap.isEmpty() == false &&
                stockAllocationsMap.containsKey(orderItem.Product__c) &&
                stockAllocationsMap.get(orderItem.Product__c).UOM__c != null &&
                conversionsMap.get(orderItem.Product__c).containsKey(stockAllocationsMap.get(orderItem.Product__c).UOM__c)
            ) {
                Boolean isAvailable = conversionAndAvailability(conversionsMap.get(orderItem.Product__c).get(stockAllocationsMap.get(orderItem.Product__c).UOM__c),
                        conversionsMap.get(orderItem.Product__c).get(orderItem.Product__r.SellingUOM__c),
                        stockAllocationsMap.get(strOrderId).Quantity_Left__c, orderItem.Order_Quantity__c);
                if(isAvailable) {
                    stockAllocationsWrapperMap.get(strOrderId).isValid = true;
                }
                else {
                    stockAllocationsWrapperMap.get(strOrderId).isValid = false;
                    stockAllocationsWrapperMap.get(strOrderId).StatusMessage = UNAVAILABLE_ERROR;
                }
            }
            else {
                stockAllocationsWrapperMap.get(strOrderId).isValid = false;
                stockAllocationsWrapperMap.get(strOrderId).StatusMessage = CONV_ERROR;
            }
        }
        
    }
    
    public Boolean conversionAndAvailability(Conversion__c inventoryConversion, Conversion__c orderItemConversion,
        Decimal inventoryQuantity, Decimal orderQuantity) {
        system.debug('===conversionAndAvailability===');
        Decimal inventoryQuantityInKg = inventoryQuantity * inventoryConversion.KG_Conversion_Rate__c;
        system.debug('====orderQuantity==='+orderQuantity);
        system.debug('====orderItemConversion==='+orderItemConversion);
        if(orderQuantity == null) {
            orderQuantity = 0;
        }
        Decimal orderQuantityInKg = orderQuantity * orderItemConversion.KG_Conversion_Rate__c;
        if(orderQuantityInKg <= inventoryQuantityInKg) {
            return true;
        }
        return false;
    }
    
    public void fetchConversions() {
        conversionsMap = new Map<String,Map<String,Conversion__c>>();
        Set<String> uomIds = new Set<String>();
        for(String strOrderId : stockAllocationsWrapperMap.keySet()) {
            if(stockAllocationsWrapperMap.containsKey(strOrderId) &&
                stockAllocationsWrapperMap.get(strOrderId).orderItem != null &&
                stockAllocationsWrapperMap.get(strOrderId).orderItem.Product__c != null &&
                stockAllocationsWrapperMap.get(strOrderId).orderItem.Product__r.SellingUOM__c != null) {
                uomIds.add(stockAllocationsWrapperMap.get(strOrderId).orderItem.Product__r.SellingUOM__c);
            }
        }
        for(String strProductId : stockAllocationsMap.keySet()) {
            if(stockAllocationsMap.get(strProductId).UOM__c != null) {
                uomIds.add(stockAllocationsMap.get(strProductId).UOM__c);
            }
        }
        if(uomIds.isEmpty() == false && stockAllocationsMap.keySet().isEmpty() == false) {
            for(Conversion__c converObj : [SELECT Id,
                                                  KG_Conversion_Rate__c,
                                                  CS_Conversion_Rate__c,
                                                  Product__c, UOM__c
                                           FROM Conversion__c
                                           WHERE Product__c IN : stockAllocationsMap.keySet() AND
                                                UOM__c IN :uomIds]) {
                if(!conversionsMap.containsKey(converObj.Product__c)) {
                    conversionsMap.put(converObj.Product__c, new Map<String,Conversion__c>());
                }
                conversionsMap.get(converObj.Product__c).put(converObj.UOM__c, converObj);
            }
        }
    }
    
    // Cap Limit Check
    public void capLimitCheck(Id batchId) {
        Boolean capExceeded = false;
        /*for(Order_Item__c orderItemInst : orderItems) {
            
        }*/
        if(capExceeded) {
            System.abortJob(batchId);
        }
    }
    
    public void getStockAllocaionsMap() {
        for(Primary_Substitute_Junction__c stockAllocJuncn : [SELECT Id,
                                                                     Name,
                                                                     Primary_Stock_Allocation__r.Allocation_Priority__c,
                                                                     Primary_Stock_Allocation__r.End_Date__c,
                                                                     Primary_Stock_Allocation__r.Material_Code__c,
                                                                     Primary_Stock_Allocation__r.Product__c,
                                                                     Primary_Stock_Allocation__r.Product__r.Name,
                                                                     Primary_Stock_Allocation__r.Quantity__c,
                                                                     Primary_Stock_Allocation__r.Quantity_Allocated__c,
                                                                     Primary_Stock_Allocation__r.Quantity_Left__c,
                                                                     Primary_Stock_Allocation__r.Serving_Plant_Code__c,
                                                                     Primary_Stock_Allocation__r.Start_Date__c,
                                                                     Primary_Stock_Allocation__r.UOM__c,
                                                                     Primary_Stock_Allocation__r.UOM__r.Name
                                              FROM Primary_Substitute_Junction__c
                                              WHERE
                                                Primary_Stock_Allocation__c != null AND
                                                Primary_Stock_Allocation__r.Product__c IN :stockAllocationsMap.keySet()
                                              ORDER BY CreatedDate
                                              ]) {
            
            if(!stockAllocationsMap.containsKey(stockAllocJuncn.Primary_Stock_Allocation__r.Product__c)) {
                stockAllocationsMap.put(stockAllocJuncn.Primary_Stock_Allocation__r.Product__c,
                    stockAllocJuncn.Primary_Stock_Allocation__r);
            }
        }
        system.debug('=======stockAllocationsMap===='+stockAllocationsMap.keySet());
    }
    

}