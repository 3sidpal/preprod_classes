@isTest
public class AttachmentFile_Test 
{
    private static Map<String, BUCatMapping__c> businessUnitMapping;
    static Market__c businessUnit;
    static Account accountCreation;
    static Category__c prodCategory;
    static Activity_ePAW__c majorActivity;
    static ePAW_Budget2__c budget;
    static ePAW_Form__c newEpawForm;
    static Chains__c customerGroup;
    
    public static testMethod Void testFileAttachment()
    {
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='MT Sales User'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id, Title='TRADE MARKETING MANAGER',
        TimeZoneSidKey='America/Los_Angeles',
        UserName=uniqueUserName);
        
        System.runAs(u) 
        {
            // Creation of Business Unit
            businessUnit = SMC_TestFactoryData.createBusinessUnit('MAGNOLIA')[0];
            System.debug('[NO DML] Business Unit List: ' + businessUnit);
            
            insert businessUnit;
            System.debug('[DML: INSERT] Business Unit List: ' + businessUnit);
            System.debug('Business Unit ID: ' + businessUnit.Id);
            // Nothing Follows: Creation of Business Unit
            
            // Creation of Account 
            accountCreation = SMC_TestFactoryData.createDistributorDirectAccount(businessUnit.Id)[0];
            System.debug('[NO DML] Account List: ' + accountCreation);
            
            insert accountCreation;
            System.debug('[DML: INSERT] Account List: ' + accountCreation);
            System.debug('Account ID: ' + accountCreation.Id);
            // Nothing Follows: Creation of Account
            
            // #####
            // ##Creation of Budget 
            // #####
            
            // Need the following Criteria:
            // [1] Major Activity 
            // [2] Product Category
            majorActivity = SMC_TestFactoryData.createMajorActivity()[0];
            System.debug('[NO DML] Major Activity List: ' + majorActivity);
            
            insert majorActivity;
            System.debug('[DML: INSERT] Major Activity List: ' + majorActivity);
            System.debug('Major Activity ID: ' + majorActivity.Id);
            
            prodCategory = SMC_TestFactoryData.createProductCategory('ICE CREAM')[0];
            System.debug('[NO DML] Product Category: ' + prodCategory);
            
            insert prodCategory;
            System.debug('[DML: INSERT] Product Category: ' + prodCategory);
            System.debug('Product Category ID: ' + prodCategory.Id);
    
            // Creating the Budget 
            //  createBudget() method expects the following arguments.
            //  [1] Id businessUnitId 
            //  [2] Id prodCatId 
            //  [3] Id accountId 
            //  [4] Id majorActivityId
            //
            //  Result: createBudget(Id businessUnitId, Id prodCatId, Id accountId, Id majorActivityId)
            budget = SMC_TestFactoryData.createBudgetControl(businessUnit.Id, prodCategory.Id, accountCreation.Id, majorActivity.Id, u.Id)[0];
            System.debug('[NO DML] ePAW Budget: ' + budget);
            
            insert budget;
            System.debug('[DML: INSERT] ePAW Budget: ' + budget);
            System.debug('ePAW Budget ID: ' + budget.Id);
            // No need for this SOQL Query because this is just a Check Point Breaker to validate if the Match Key Code was created.
            // And to avoid hitting the governor limits.
            // System.debug('MATCH KEY CODE: ' + [SELECT Match_Key_Code__c FROM ePAW_Budget2__c WHERE Id = :budget.Id]);
            // #####
            // ##Nothing Follows: Creation of Budget
            // #####
            
            // #####
            // ## Creation of ePAW Form 
            // ####
            newEpawForm                        = SMC_TestFactoryData.createEpawForm(accountCreation.Id)[0];
            System.debug('[NO DML] ePAW Form: ' + newEpawForm);
            
            insert newEpawForm;
            System.debug('[DML: INSERT] ePAW Form: ' + newEpawForm);
            System.debug('ePAW Form ID: ' + newEpawForm.Id);
            // #####
            // ##Nothing Follows: ePAW Form
            // #####
            
            // Create the Attachment File 
            Blob bodyBlob             = Crypto.generateAesKey(128);
            Attachment fileAttachment = new Attachment(Name = 'PD-20200620032633', ParentId = newEpawForm.Id, Body = bodyBlob);
            insert fileAttachment;
            
            // TEST DELETE THE ATTACHMENT:
            Id attParentId = fileAttachment.ParentId;
            List<Attachment> attValues = [SELECT Name FROM Attachment WHERE ParentId = :attParentId];
            System.debug('File Attachment Values are : ' + attValues);
            
            delete attValues; 
        }
    }
}