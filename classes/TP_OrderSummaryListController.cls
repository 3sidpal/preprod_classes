public with sharing class TP_OrderSummaryListController {

    public   List<Order__c> valuePropOppList {get; set;}
    public TP_OrderSummaryController mainCntrl {get; set;}
    //public String sortingColumn {get; set;}
    //public SearchWrapper searchWrapper;
    private Integer count=1;   //to track the function calling
    //private Integer counter=0;   //to keep track of offset
    private Integer list_size=10; //to set the page size to show the rows/records
    public Integer total_size{get;set;} //used to show user the total size of the list  
    public boolean printMode{get;set;}
    public SearchWrapper wrapperFromReqParam ;
    public SearchWrapper searchWrapperObj{get;set;}
    public Integer limitValue;
    public boolean checkLimit;
    public static final Integer PRINTLIMIT =1000;
    public static final Integer RECLIMIT = 2000;
    public Map<Integer, Map<String,String>> orderFieldSetMap {get; set;} //added by JC Aduan 3/14/2018
   // public static final Integer PAGESIZE = 100;
    Transient List<Order__c> totalOrderList;

    public String sortBy {
        get {
            return  searchWrapperObj.sortedField;
        }
        set{
            if (value ==  searchWrapperObj.sortedField)
                sortDir = (sortDir == 'desc') ? 'asc' : 'desc';
            else
                sortDir = 'asc';
                searchWrapperObj.sortedField = value;
        }
    }

    public String sortDir {
        get {
            return searchWrapperObj.sortedOrder;
        }
        set {
            searchWrapperObj.sortedOrder = value;
        }
    }
    
    public TP_OrderSummaryListController(TP_OrderSummaryController cntrl) {
    
        mainCntrl = cntrl;
        totalOrderList = new List<Order__c>();
        searchWrapperObj = new SearchWrapper();
        printMode=false;
        limitValue = RECLIMIT;
        checkLimit = false;
        searchWrapperObj = mainCntrl.searchWrapperMap.get(mainCntrl.activeTab);
        searchWrapperObj = mainCntrl.searchWrapperMap.get(mainCntrl.activeTab);
        searchWrapperObj.counter =0;
        searchWrapperObj.pageSize = Integer.valueOf(Label.pageSize);
        orderFieldSetMap = new Map<Integer, Map<String,String>>();

        String parameterDetails=ApexPages.currentPage().getParameters().get('searchWrapper');
        if(null!=parameterDetails && !String.isEmpty(parameterDetails)){
            wrapperFromReqParam=(SearchWrapper)JSON.deserialize(parameterDetails,SearchWrapper.Class)  ;
            printMode=wrapperFromReqParam.printPDFmode;
            
        }
        // added by JC Aduan 3/14/2018
        //orderFieldSetMap = getOrderFielsetMap(mainCntrl.activeTab, printMode);

        if(printMode){
            
            checkLimit = true;
            limitValue = PRINTLIMIT;
            reloadPage();
        }
        totalOrderList = fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder,null);
        
        total_size = null != totalOrderList && totalOrderList.size()>0 ?totalOrderList.size() :0;
        searchWrapperObj.totalRowSize = total_size;
        
        //total_size = [select count() from Order__c where RecordType.DeveloperName ='Actual_Sales' and  Status__c =: mainCntrl.activeTab ];
    }
    
    public PageReference reloadPage(){

        valuePropOppList = new List<Order__c>();
        searchWrapperObj = mainCntrl.searchWrapperMap.get(mainCntrl.activeTab);

        if(null!=wrapperFromReqParam){
            searchWrapperObj=wrapperFromReqParam;
            mainCntrl.activeTab=wrapperFromReqParam.activeTab;
        }
        limitValue = RECLIMIT;
        totalOrderList = fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder,null);
        
        total_size = null != totalOrderList && totalOrderList.size()>0 ?totalOrderList.size() :0;
        searchWrapperObj.totalRowSize = total_size;
        //searchWrapperObj.offsetValue =0;
        //searchWrapperObj.counter=0;
        limitValue = searchWrapperObj.pageSize;
        if(printMode)
        {
            checkLimit = true;
            searchWrapperObj.offsetValue = null;
            searchWrapperObj.counter =0;
            limitValue = PRINTLIMIT;
        }
        
        orderFieldSetMap = getOrderFielsetMap(mainCntrl.activeTab, printMode);

        valuePropOppList = fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder,searchWrapperObj.offsetValue);

            return null;
    }

    //START added by JC Aduan 3/14/2018
    public Map<Integer,Map<String, String>> getOrderFielsetMap(String stageName, Boolean isPrintMode) {
        
        Schema.FieldSet fieldSetMap = Utils.fetchOppFieldSet(stageName);
        Map<Integer, Map<String, String>> fieldValuesMapMap = new Map<Integer, Map<String,String>>();
        Integer cntKey = 0;
        
        for(Schema.FieldSetMember f : fieldSetMap.getFields()) {
            Map<String, String> tempFieldValuesMap = new Map<String,String>();
                                
                if(isPrintMode == false){
                    if(excludedOrderSummaryView(f.getFieldPath())){
                        tempFieldValuesMap.put(f.getLabel(), f.getFieldPath());
                        fieldValuesMapMap.put(cntKey, tempFieldValuesMap);
                        cntKey++;   
                    } 
                }else{
                    tempFieldValuesMap.put(f.getLabel(), f.getFieldPath());
                    fieldValuesMapMap.put(cntKey, tempFieldValuesMap);
                    cntKey++;
                }
        }

        return fieldValuesMapMap;
        
    }
    
    public Boolean excludedOrderSummaryView(String field){
            
        List<String> exludedFieldAPIList = new List<String>();
        
        if(mainCntrl.activeTab != 'All') {
            exludedFieldAPIList.add('Status__c');
        }
        Boolean isTrue = true;
        
        for(String fld : exludedFieldAPIList){
            if(fld == field){
                isTrue = false;
            }
        }

        return isTrue;
    }
    //END 3/14/2018

    public List<Order__c> fetchOrders(String searchVar, String stageName, Date startDate, Date enddate, String oppRecordType, String sortField, String sordOrder , Integer offsetValue) {
        
        Id currentId = UserInfo.getUserId();
        Id currentProfileId = UserInfo.getProfileId();
        String currentUserBU = [SELECT Business_Unit__c FROM User WHERE Id =:currentId].Business_Unit__c;
        Profile currentProfile = [select Name from Profile where Id = :currentProfileId];
        
        String searchQueryString ;
        if(searchWrapperObj.selectedOppId != null){
            return new List<Order__c>();
        }
        Time endTime = Time.newInstance(23,59,59,100);
        DateTime endDateTime;
        if (NULL != enddate) endDateTime = Datetime.newInstanceGmt(enddate, endTime);
        
        if(stageName.contains('_')) {       
            stageName = stageName.replace('_',' ');     
        }
        
        String searchingVar ;
        if( searchVar == null || searchVar == '' ) {
            if(mainCntrl.activeTab.contains('_')) {
                searchingVar = mainCntrl.activeTab.replace('_',' ');
            }
            else {
                searchingVar = mainCntrl.activeTab;
            }
        }
        else {
            searchingVar = searchVar;
        }
        
        //searchQueryString = 'SELECT Id, Order_Reference_No__c, Account_Name__c, SAS_Name__c, Sub_Status__c, SAP_Sales_Order__c, PO_No__c, PO_Date__c, Requested_Delivery_Date__c, Ship_Sold_To__c, Delivery_Time__c, Product_Category_Name__c ' ;
        searchQueryString = 'SELECT ' ;

        searchQueryString += Utils.getQueryString('Order__c',true); 
        
            if(!Utils.isStringNullOrEmpty(oppRecordType))
            {
                //searchQueryString += ' FROM Order__c WHERE RecordType.DeveloperName = '+ '\'' +oppRecordType+'\' ';
                searchQueryString += ' FROM Order__c WHERE RecordTypeId = '+ '\'' +Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Purchase Order').getRecordTypeId()+'\' ';
            }
            if(!Utils.isStringNullOrEmpty(stageName)) {
            
                if(stageName != 'All') { //GAAC: 4/20/2018 - 
                    searchQueryString += ' AND Status__c = '+ '\'' +stageName+'\' ';
                }
            }

            if(startDate!=null) searchQueryString += ' AND Requested_Delivery_Date__c >=: startDate' ;
            if(endDateTime!=null) searchQueryString += ' AND Requested_Delivery_Date__c <=:enddate' ;

            if(!currentProfile.Name.endsWith('Administrator')) {
             searchQueryString += ' AND CreatedById = :currentId';
            }
            else if(currentProfile.Name == 'BU Administrator') {
             searchQueryString += ' AND Product_Category__r.Business_Unit__c INCLUDES (:currentUserBU)';
            }
            
            if(!Utils.isStringNullOrEmpty(sortField))
            {
                searchQueryString += ' ORDER BY ' + sortBy + ' ' + sortDir ;
            }
            if(sortDir =='DESC')
            {
                searchQueryString += ' NULLS LAST ';
            }
            if(checkLimit){
                searchQueryString += ' LIMIT ' + PRINTLIMIT;
            }
            else{
                searchQueryString += ' LIMIT ' + limitValue ;
            }

            if(null != offsetValue){
                searchQueryString += ' OFFSET ' + offsetValue;
            }


        List<Order__c> orderResult;
        try {

            orderResult = Database.query(searchQueryString);
        }
        catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,ex.getMessage()));
            return NULL;
        }

        return orderResult;
    }

    /*
     * Method for Sorting. Called when user clicks on column headers
     */
    public PageReference doSorting() {

        searchWrapperObj.sortedField = sortBy;
        searchWrapperObj.sortedOrder = sortDir;

        return reloadPage();
    }

    public PageReference Beginning() {  //when the user clicked the beginning button
        searchWrapperObj.offsetValue = 0;
        searchWrapperObj.counter=0;
        valuePropOppList=fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder,searchWrapperObj.offsetValue);
        return null;
    }

    public PageReference Previous() { //user clicked the previous button
         searchWrapperObj.counter -= searchWrapperObj.pageSize;
         searchWrapperObj.offsetValue -= searchWrapperObj.pageSize;

        if(count==1){
            valuePropOppList=fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder ,searchWrapperObj.offsetValue);
            count++;
        }
        else 
            valuePropOppList=fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder ,searchWrapperObj.offsetValue);
        return null;
    }
    
    public PageReference Next() {  
      //user clicked the Next button
        if(searchWrapperObj.offsetValue==null)
            searchWrapperObj.offsetValue = 0;
        searchWrapperObj.offsetValue += searchWrapperObj.pageSize;
        searchWrapperObj.counter += searchWrapperObj.pageSize;

        valuePropOppList=fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder ,searchWrapperObj.offsetValue);
        return null;
        
    }

    public PageReference End() { 
        searchWrapperObj.counter = searchWrapperObj.totalRowSize - math.mod(searchWrapperObj.totalRowSize, searchWrapperObj.pageSize);     //user clicked the End button
        searchWrapperObj.offsetValue = searchWrapperObj.counter;
        
        valuePropOppList=fetchOrders(searchWrapperObj.searchStr,mainCntrl.activeTab ,searchWrapperObj.stDate ,searchWrapperObj.enDate,'Purchase_Order',searchWrapperObj.sortedField,searchWrapperObj.sortedOrder,searchWrapperObj.offsetValue);
        return null;
    }
    
     public Boolean getDisabledPrevious() {           //this will disable the previous and beginning buttons

        if(searchWrapperObj.counter>0)
            return false;
         else 
            return true;
    }


     public Boolean getDisabledNext() {  

       //this will disable the next and end buttons
        if (searchWrapperObj.counter + searchWrapperObj.pageSize < searchWrapperObj.totalRowSize) 
            return false; 
        else 
            return true;
    }
    
    public Integer getTotal_size() {

        return searchWrapperObj.totalRowSize;
    }
    public Integer getPageNumber() {
        return searchWrapperObj.counter/searchWrapperObj.pageSize + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(searchWrapperObj.totalRowSize, searchWrapperObj.pageSize) > 0) {
            return searchWrapperObj.totalRowSize/searchWrapperObj.pageSize + 1;
        } 
      else {
            return (searchWrapperObj.totalRowSize/searchWrapperObj.pageSize);
        }
    } 
}