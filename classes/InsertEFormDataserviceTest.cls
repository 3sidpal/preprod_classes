@isTest
public class InsertEFormDataserviceTest {


    public static testMethod void  doPostTest() {
        
        List<Task> taskList = DIL_TestDataFactory.getTasks(1, true);

        String jsonStr=        '['+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/'+taskList.get(0).Id+'"'+
        '    },'+
        '    "Id": "'+taskList.get(0).Id+'",'+
        '    "Title__c": "My Task   new",'+
        '    "StartDateTime__c": "2018-07-31T16:31:00.000+0000",'+
        '    "EndDateTime__c": "2018-07-31T16:51:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/'+taskList.get(0).Id+'"'+
        '    },'+
        '    "Id": "'+taskList.get(0).Id+'",'+
        '    "Title__c": "Visit merchant for sample demo",'+
        '    "StartDateTime__c": "2018-08-08T09:46:00.000+0000",'+
        '    "EndDateTime__c": "2018-08-08T12:46:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007S3WXEA0"'+
        '    },'+
        //'    "Id": "00T0l000007S3WXEA0",'+
        '    "Title__c": "Las Vegas Account",'+
        '    "StartDateTime__c": "2018-10-09T05:43:00.000+0000",'+
        '    "EndDateTime__c": "2018-10-10T05:43:00.000+0000"'+
        '  }'+
        ']';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertEFormData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;

        System.Test.startTest();
            InsertEFormDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Task].size());

    }

    public static testMethod void  doPostTestFail() {

        String jsonStr=        '['+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007EuyMEAS"'+
        '    },'+
        '    "Id": "00T0l000007EuyMEAS",'+
        '    "Title__c": "My Task   new",'+
        '    "StartDateTime__c": "2018-07-31T16:31:00.000+0000",'+
        '    "EndDateTime__c": "2018-07-31T16:51:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007GNwOEAW"'+
        '    },'+
        '    "Id": "00T0l000007GNwOEAW"'+
        '    "Title__c": "Visit merchant for sample demo",'+
        '    "StartDateTime__c": "2018-08-08T09:46:00.000+0000",'+
        '    "EndDateTime__c": "2018-08-08T12:46:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007S3WXEA0"'+
        '    },'+
        //'    "Id": "00T0l000007S3WXEA0",'+
        '    "Title__c": "Las Vegas Account",'+
        '    "StartDateTime__c": "2018-10-09T05:43:00.000+0000",'+
        '    "EndDateTime__c": "2018-10-10T05:43:00.000+0000"'+
        '  }'+
        ']';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertEFormData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;

        System.Test.startTest();
            InsertEFormDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Task].size());

    }

    public static testMethod void  doPostTestFail2() {

        String jsonStr=        '['+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007EuyMEAS"'+
        '    },'+
        '    "Id": "00T0l000007EuyMEAS",'+
        '    "Title__c": "My Task   new",'+
        '    "StartDateTime__c": "2018-07-31T16:31:00.000+0000",'+
        '    "EndDateTime__c": "2018-07-31T16:51:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007GNwOEAW"'+
        '    },'+
        '    "Id": "00T0l000007GNwOEAW",'+
        '    "Title__c": "Visit merchant for sample demo",'+
        '    "StartDateTime__c": "2018-08-08T09:46:00.000+0000",'+
        '    "EndDateTime__c": "2018-08-08T12:46:00.000+0000"'+
        '  },'+
        '  {'+
        '    "attributes": {'+
        '      "type": "Task",'+
        '      "url": "/services/data/v44.0/sobjects/Task/00T0l000007S3WXEA0"'+
        '    },'+
        //'    "Id": "00T0l000007S3WXEA0",'+
        '    "Title__c": "Las Vegas Account",'+
        '    "StartDateTime__c": "2018-10-09T05:43:00.000+0000",'+
        '    "EndDateTime__c": "2018-10-10T05:43:00.000+0000",'+
        '    "User__c": "2018-10-10T05:43:00.000+0000"'+
        '  }'+
        ']';


        RestRequest req  = new RestRequest();
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/insertEFormData';  //Request URL
        req.httpMethod = 'POST'; //HTTP Request Type
        req.requestBody = Blob.valueof(jsonStr);
        RestContext.request  = req;
        RestContext.response = res;

        System.Test.startTest();
            InsertEFormDataservice.doPost();
        System.Test.stopTest();

        System.assertEquals(0, [SELECT Id FROM Task].size());

    }
}