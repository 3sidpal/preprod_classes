public class StockAllocationTriggerHandler {
    
    /*
    *Following method will populate the Custom product lookup fields based on 
    *the respective Material_Code__c fields
    */
    public static void populateProductLookups() {
        
        Map<String,String> mapMaterialCodeProdId = new Map<String,String>();
        
        for(Stock_Allocation__c stockAlloc : (List<Stock_Allocation__c>) Trigger.new) {
            if(stockAlloc.Material_Code__c != null) {
                mapMaterialCodeProdId.put(stockAlloc.Material_Code__c.toLowerCase(),null);
            }
        }
        system.debug('====mapMaterialCodeProdId===='+json.serializePretty(mapMaterialCodeProdId));
        
        if(mapMaterialCodeProdId.isEmpty() == false) {
            for(Custom_Product__c custProd : [SELECT Id,
                                                     SKU_Code__c
                                              FROM Custom_Product__c
                                              WHERE SKU_Code__c IN : mapMaterialCodeProdId.keySet()]) {
                mapMaterialCodeProdId.put(custProd.SKU_Code__c.toLowerCase(), custProd.Id);
            }
        }
        
        system.debug('====mapMaterialCodeProdId===='+json.serializePretty(mapMaterialCodeProdId));
        
        for(Stock_Allocation__c stockAlloc : (List<Stock_Allocation__c>) Trigger.new) {
            if(stockAlloc.Material_Code__c != null && 
                mapMaterialCodeProdId.get(stockAlloc.Material_Code__c.toLowercase()) != null) {
                stockAlloc.Product__c = mapMaterialCodeProdId.get(stockAlloc.Material_Code__c.toLowercase());
            }
            else if(stockAlloc.Material_Code__c != null) {
                stockAlloc.addError('Product '+stockAlloc.Material_Code__c+' not found.');
            }
            system.debug('====stockAlloc ===='+json.serializePretty(stockAlloc ));
        }
        
    }
    
    
    /*
    *Following method will populate the UOM on stock allocations
    */
    public static void populateStockUOMLookups() {
        Map<String,Id> mapUomNameId = new Map<String,Id>();
        for(Uom__c uomInst : [SELECT Id, Name
                              FROM Uom__c]) {
                                  
            mapUomNameId.put(uomInst.Name.toLowerCase(), uomInst.Id);
        }
        for(Stock_Allocation__c stockAlloc : (List<Stock_Allocation__c>) Trigger.new) {
            if(stockAlloc.UOM_Name__c != null && mapUomNameId.containsKey(stockAlloc.UOM_Name__c.toLowerCase())) {
                stockAlloc.UOM__c = mapUomNameId.get(stockAlloc.UOM_Name__c.toLowerCase());
            }
            else {
                stockAlloc.addError('Please specify valid UOM for stock allocation.');
            }
        }
    }
    /*
    *Following method will populate the primary stock allocation records
    */
    /*public static void populatePrimaryStockLookups() {
        Set<String> primaryProds = new Set<String>();
        Map<String,String> primaryProdsMap = new Map<String,String>();
        for(Stock_Allocation__c stockAlloc : (List<Stock_Allocation__c>) Trigger.new) {
            if(stockAlloc.Primary_Material_Code__c != null) {
                primaryProdsMap.put(stockAlloc.Primary_Material_Code__c.toLowerCase(), null);
            }
        }
        if(!primaryProdsMap.isEmpty()) {
            for(Stock_Allocation__c primStockAlloc : [SELECT Id,
                                                             Product__r.SKU_Code__c
                                                      FROM Stock_Allocation__c
                                                      WHERE Product__r.SKU_Code__c IN :primaryProdsMap.keySet() ]) {
                                                          
                primaryProdsMap.put(primStockAlloc.Product__r.SKU_Code__c.toLowerCase(), primStockAlloc.Id);
            }
            for(Stock_Allocation__c stockAlloc : (List<Stock_Allocation__c>) Trigger.new) {
                if(stockAlloc.Primary_Material_Code__c != null && primaryProdsMap.get(stockAlloc.Primary_Material_Code__c.toLowercase()) != null) {
                    stockAlloc.Primary_Stock_Allocation__c = primaryProdsMap.get(stockAlloc.Primary_Material_Code__c.toLowercase());
                }
                else if(stockAlloc.Primary_Material_Code__c != null){
                    stockAlloc.addError('Primary Material Code '+stockAlloc.Primary_Material_Code__c+' not present.');
                }
            }
        }
    }*/
}