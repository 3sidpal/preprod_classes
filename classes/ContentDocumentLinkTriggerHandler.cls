public class ContentDocumentLinkTriggerHandler{
    
    public static void processUploadedFiles() {
        Map<String,ContentVersion> contentDocToVersion = new Map<String,ContentVersion>();   
        Set<Id> contentDocIds = new Set<Id>();
        
        for(ContentDocumentLink conDocLink : (List<ContentDocumentLink>) Trigger.new) {
            contentDocIds.add(conDocLink.ContentDocumentId);
        }
        
        for(ContentVersion contDocVersion : [SELECT Id,
                                                    ContentDocumentId,
                                                    FileType,
                                                    ContentSize,
                                                    PathOnClient
                                             FROM ContentVersion
                                             WHERE ContentDocumentId IN :contentDocIds]) {
            
            contentDocToVersion.put(contDocVersion.ContentDocumentId,
                                    contDocVersion);
        }
        
        String uploadJobKeyprefix = Upload_Job__c.sObjectType.getDescribe().getKeyPrefix();
        List<String> lstBatchIds = new List<String>();
        
        List<Upload_Job__c> uploadJobsToUpdate = new List<Upload_Job__c>();
        Map<String,String> uploadJobConVerIDs = new Map<String,String>();
        Map<String,Upload_Job__c> uploadJobsMap = new Map<String,Upload_Job__c>();
        
        for(ContentDocumentLink conDocLink : (List<ContentDocumentLink>) Trigger.new) {
            ContentVersion conVerInst = contentDocToVersion.get(conDocLink.ContentDocumentId);
            system.debug('=======conVerInst==='+Json.serializePretty(conVerInst));
            if(String.valueOf(conDocLink.LinkedEntityId).startsWith(uploadJobKeyprefix) && !conVerInst.PathOnClient.contains('Error')) {
                if(conVerInst.FileType != 'csv') {
                    conDocLink.addError('Please upload only .csv files');
                    break;
                }
                else if(conVerInst.ContentSize > 4288720){
                    conDocLink.addError('Please upload files till 4MB');
                    break;
                }
                else {
                    String batchId = Database.executeBatch(new ProcessCsvBatch(conVerInst.Id, conDocLink.LinkedEntityId, true, null, null));
                    system.debug('====batchId ==='+batchId);
                    uploadJobConVerIDs.put(conDocLink.LinkedEntityId, conVerInst.Id);
                }
            }
        }
        
        for(Upload_Job__c uploadJobInst : [SELECT Status__c 
                                           FROM Upload_Job__c
                                           WHERE Id IN :uploadJobConVerIDs.keySet()]) {
        
            uploadJobsMap.put(uploadJobInst.Id,uploadJobInst);
        }
        
        if(uploadJobConVerIDs.isEmpty() == false) {
            for(String uploadJobId : uploadJobConVerIDs.keySet()) {
                Map<String,Map<String,String>> uploadJobStatus = new Map<String,Map<String,String>>();
                if(!String.isBlank(uploadJobsMap.get(uploadJobId).Status__c)){
                    uploadJobStatus = (Map<String,Map<String,String>>)Json.deserialize(uploadJobsMap.get(uploadJobId).Status__c, Map<String,Map<String,String>>.class);
                }
                uploadJobStatus.put(uploadJobConVerIDs.get(uploadJobId),new Map<String,String>{'Status'=>'Inprogress','ErrorCSV'=>''});
                Upload_Job__c uploadJob = new Upload_Job__c(Id = uploadJobId,
                                                            Status__c = Json.serializePretty(uploadJobStatus));
                uploadJobsToUpdate.add(uploadJob);
            }
            if(uploadJobsToUpdate.isEmpty() == false) {
                update uploadJobsToUpdate;
            }
        }
    }

}