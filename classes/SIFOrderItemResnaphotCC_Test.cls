/**********************************************************
* @Author       Rozelle Villanueva
* @Date         10-20-2016
* @Description  Test class for SIFOrderItemResnaphotCC 
* @Revision(s)
**********************************************************/
@isTest
public class SIFOrderItemResnaphotCC_Test {
    static testmethod void testSnapshot() {
        Test.startTest();
        TestDataFactory tdf = new TestDataFactory();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);
        
        //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestTPOrder(1, customerAccount.Distributor__c, customerAccount.Id);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_Date__c = invoiceDate;
        update sif;
       
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(sif[0]);
        SIFOrderItemResnaphotCC cont = new SIFOrderItemResnaphotCC(std);
        cont.snapshotMonth = 2;
        cont.snapshotYear = '2016';
        cont.runSnapshot();
        cont.getmonthsList();        
        Test.stopTest();
    }    
    
    static testmethod void testSnapshotInvalid() {
        Test.startTest();
        TestDataFactory tdf = new TestDataFactory();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;        
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);
        
        //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestTPOrder(1, customerAccount.Distributor__c, customerAccount.Id);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_Date__c = invoiceDate;
        update sif;
       
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(sif[0]);
        SIFOrderItemResnaphotCC cont = new SIFOrderItemResnaphotCC(std);
        cont.snapshotMonth = 2;
        cont.snapshotYear = 'test';
        cont.runSnapshot();
        cont.getmonthsList();        
        Test.stopTest();
    }    
    static testmethod void testSnapshotNull() {
        Test.startTest();
        TestDataFactory tdf = new TestDataFactory();
        Id customerRecordTypeId = Schema.sObjectType.Account.getRecordTypeInfosByName().get('Distributor Customer').getRecordTypeId();
        // account record, 'Distributor Direct'
        list <Account> accList = tdf.createTestAccount(1, True, True, 'Distributor/ Direct',null);
        
        //AARP: Added Customer Account
        Account customerAccount = new Account(
            Name = 'TestDistributor',
            RecordTypeId = customerRecordTypeId ,
            Sales_Organization__c  = 'Feeds',
            BU_Feeds__c = true,
            Branded__c = true,
            GFS__c = true,
            BU_Poultry__c = true,
            Active__c = true,
            AccountNumber = 'ACT11',
            Distributor__c = accList[0].Id
        );        
        
        insert customerAccount;        
        
        String contDSPRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Distributor Personnel').getRecordTypeId();
        String contSASRT = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('SAS').getRecordTypeId();
        // contact record
        list <Contact> contList = TestDataFactory.createContact(2, accList[0].Id);
        contList[0].RecordtypeId = contSASRT ;
        contList[1].RecordtypeId = contDSPRT ;
        insert contList;
        
        Date mnth = System.Today().addMonths(-3);
        Integer last3Month = mnth.month();
        Date invoiceDate = Date.newInstance(System.Today().year(), last3Month, 30);
        
         //AARP: set Account used to Customer Account
        // sales information record
        list <Order__c> sif = tdf.createTestTPOrder(1, customerAccount.Distributor__c, customerAccount.Id);
        sif[0].RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Actual Sales').getRecordTypeId();
        sif[0].Invoice_Date__c = invoiceDate;
        update sif;
       
        Apexpages.Standardcontroller std = new Apexpages.Standardcontroller(sif[0]);
        SIFOrderItemResnaphotCC cont = new SIFOrderItemResnaphotCC(std);
        cont.runSnapshot();
        cont.getmonthsList();        
        Test.stopTest();
    }    
}