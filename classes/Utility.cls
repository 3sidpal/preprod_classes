global with sharing class Utility {
    
    //Method that will returns ID value of a specific record type by providing sObject and Record Type Name value.
    public Id getRecordTypeId(String ObjectType, String RecordTypeLabel) {   
    
    Schema.SObjectType Res = Schema.getGlobalDescribe().get(ObjectType); 
    Map<string, schema.recordtypeinfo> recordTypeInfo = Res.getDescribe().getRecordTypeInfosByName();

        if(recordTypeInfo.containsKey(RecordTypeLabel)) {
    
            return recordTypeInfo.get(RecordTypeLabel).getRecordTypeId();
        } else {
            return null;
        }
    }

    public List<selectOption> getPickValues(Sobject object_name, String field_name, String first_val) {
      List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
      if (first_val != null) { //if there is a first value being provided
         options.add(new selectOption(first_val, first_val)); //add the first option
      }
      Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         if(a.getValue() != 'unknown' && a.getValue() != 'Other' )         
            options.add(new selectOption(a.getValue(), a.getLabel())); //add the value and label to our final list
      }
      return options; //return the List
    }
}