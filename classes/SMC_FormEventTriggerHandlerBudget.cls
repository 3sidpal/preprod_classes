/*
 * -----------------------------------------------------------------
 *   PURPOSE : Scalable Match Key Criteria (Future Proof). Problem is
 *   Salesforce Out-of-the-box feature cannot anymore be handled by the
 *   Match Key Criteria.
 *-----------------------------------------------------------------
 */
public class SMC_FormEventTriggerHandlerBudget
{
    // GOAL - onAfterInsertBudgetMatchKey(List<ePAW_Budget2__c> ePAWBudget):
    // Within the given criteria entered by user - Match Key will automatic populate the data
    // in the Match_Key_Code__c field.
    public static void onInsertUpdateBudgetMatchKey(List<ePAW_Budget2__c> ePAWBudget)
    {
        System.debug('=== [SMC_FormEventTriggerHandlerBudget] onInsertUpdateBudgetMatchKey() has been initiated: ===');
        Set<Id> businessUnitId    = new Set<Id>();
        Set<Id> majorActivityId   = new Set<Id>();
        Set<Id> productCategoryId = new Set<Id>();
        Map<Id, Market__c> businessUnitMapping         = new Map<Id, Market__c>();
        Map<Id, Activity_ePAW__c> majorActivityMapping = new Map<Id, Activity_ePAW__c>();
        Map<Id, Category__c> productCategoryMapping    = new Map<Id, Category__c>();
        Map<String, String> tempMatchKey               = new Map<String, String>();

        String strBusinessUnit, strMajorActivity, strProductCategory;

        for(ePAW_Budget2__c getID : ePAWBudget)
        {
            businessUnitId.add(getID.Business_Unit__c);       // Get the ID of Business Unit
            majorActivityId.add(getID.Major_Activity__c);     // Get the ID of Major Activity
            productCategoryId.add(getID.Product_Category__c); // Get the ID of Product Category
        }

        businessUnitMapping    = new Map<Id, Market__c>([SELECT Name FROM Market__c WHERE Id =: businessUnitId]);
        majorActivityMapping   = new Map<Id, Activity_ePAW__c>([SELECT Name FROM Activity_ePAW__c WHERE Id =: majorActivityId]);
        productCategoryMapping = new Map<Id, Category__c>([SELECT Name FROM Category__c WHERE Id =: productCategoryId]);

        /*
            FOR REFERENCE (MAYBE UPDATED IN THE FUTURE):

            Match Key Criteria encompasses the following:
            1.  YEAR             - Start_Date__c or End_Date__c
                -> Business Process: According to SMC Budget should be within the year period only (Year Period Only means in between January 1, <Year> to December 31, <Year>).
            2.  BUDGET NAME      - Name
            3.  AREA             - Area__c
            4.  ACCOUNT CHARGE   - Account_Charge__c
            5.0 CUSTOMER GROUP   - Customer_Group__c
            5.1 ACCOUNT          - Account__c
            6.  BUSINESS UNIT    - Business_Unit__c
            7.  MAJOR ACTIVITY   - Major_Activity__c
            8.  PRODUCT CATEGORY - Product_Category__c
            9.  BUDGET TYPE      - Budget_Type__c
        */
        for(ePAW_Budget2__c matchKeyBudget : ePAWBudget)
        {
            strBusinessUnit    = businessUnitMapping.get(matchKeyBudget.Business_Unit__c).Name;       // Get the Name of Business Unit based from the Business Unit ID
            strMajorActivity   = majorActivityMapping.get(matchKeyBudget.Major_Activity__c).Name;     // Get the Name of Major Activity based from the Major Activity ID
            strProductCategory = productCategoryMapping.get(matchKeyBudget.Product_Category__c).Name; // Get the Name of Product Category based from the Product Category ID
            tempMatchKey       = generateEpawMatchKey(matchKeyBudget,
                                                      strBusinessUnit,
                                                      strMajorActivity,
                                                      strProductCategory);                            // Generate the Match Key needed for ePAW Budget.

            /*
            #######
            ## MATCH KEY CRITERIA DEPENDENCY
            ## See Documentation for Match Key Criteria.
            #######
            */
            matchKeyBudget.Match_Key_Uniqueness__c = tempMatchKey.get('UNIQUE_BUDGET');              // Ensuring Uniqueness via Budget Name; therefore, no duplicate.
            matchKeyBudget.Match_Key_Code__c       = tempMatchKey.get('AREA_OR_CHAIN_LEVEL_BUDGET');

            matchKeyBudget.Match_Key_TMG__c        = tempMatchKey.get('TMG_LEVEL_BUDGET');           // Budget for TMG USERS
            matchKeyBudget.Match_Key_WM__c         = tempMatchKey.get('WET_MARKET_LEVEL_BUDGET');    // Budget for WET MARKET (RM Direct User)
            matchKeyBudget.Match_Key_Sales__c      = tempMatchKey.get('SALES_LEVEL_BUDGET');         // Budget for SALES USER
            matchKeyBudget.Match_Key_MT__c         = tempMatchKey.get('MT_LEVEL_BUDGET');            // Budget for MT SALES User
            matchKeyBudget.Match_Key_MIC_Direct__c = tempMatchKey.get('MIC_DIRECT_BUDGET');          // Budget for MIC Direct User
            /*
            #######
            ## NOTHING FOLLOWS - MATCH KEY CRITERIA DEPENDENCY
            #######
            */
        }
        System.debug('>>> [SMC_FormEventTriggerHandlerBudget] onInsertUpdateBudgetMatchKey() has been terminated: <<<');
    }

    /*
    |-----------------------------------------------------------------
    | CUSTOM TRIGGER METHODS
    |-----------------------------------------------------------------
    */
    public static Map<String, String> generateEpawMatchKey(ePAW_Budget2__c ePAWBudget, String businessUnit, String majorActivity, String productCategory)
    {
        System.debug('=== [SMC_FormEventTriggerHandlerBudget] generateEpawMatchKey() has been initiated: ===');
        String generatedMatchKey, strYear, budgetName, areaCoverage, accountCharge, customerGroup, accountLevel, businessUnits, majorActivities,
               productCategories, budgetType, budgetDetermination, accountLevelBudget, matchKeyBudgetUniqueness, areaLevelBudget, tmgMatchKey,
               wetMarketMatchKey, salesMatchKey, mtMatchKey, micDirectMatchKey;
        Integer year;
        Map<String, String> generatedMatchKeyMapping = new Map<String, String>();

        year              = ePAWBudget.Start_Date__c.year();
        strYear           = String.valueOf(year); // Convert Integer (Year) to String for same interoperability.
        budgetName        = ePAWBudget.Name.toUpperCase();
        areaCoverage      = ePAWBudget.Area__c.toUpperCase();
        accountCharge     = ePAWBudget.Account_Charge__c;
        customerGroup     = (ePAWBudget.Customer_Group__c == null) ? '*' : ePAWBudget.Customer_Group__c;
        accountLevel      = (ePAWBudget.Account__c        == null) ? '*' : ePAWBudget.Account__c;
        businessUnits     = businessUnit;
        majorActivities   = majorActivity;
        productCategories = productCategory;
        budgetType        = ePAWBudget.Budget_Type__c;

        matchKeyBudgetUniqueness = budgetName;
        // MATCH KEY CODE DEPENDENCY
        // BUSINESS PROCESS OF SMC:
        // - If Customer Group has a Value then Match Key should be dependent on the Customer Group only else Account Level.
        // Example:
        //    Customer Group: 321
        //    Account       : Lateco Trading
        // Therefore your Match Key should only contain Customer Group.
        // Original Design By: Solutions Architect - Sherv

        // Match_Key_Code__c will be used.
        if(ePAWBudget.Customer_Group__c != null)
        {
            // Budget is Customer Group Level
            System.debug('BUDGET LEVEL: CUSTOMER GROUP');
            generatedMatchKey = strYear         + ' - ' + // YEAR
                                budgetName      + ' - ' + // BUDGET NAME
                                areaCoverage    + ' - ' + // AREA
                                accountCharge   + ' - ' + // ACCOUNT CHARGE
                                customerGroup   + ' - ' + // CUSTOMER GROUP
                                '*'             + ' - ' + // ACCOUNT (*) | Expectations: Account Level should return "*"
                                businessUnit    + ' - ' + // BUSINESS UNIT
                                majorActivity   + ' - ' + // MAJOR ACTIVITY
                                productCategory + ' - ' + // PRODUCT CATEGORY
                                budgetType;               // BUDGET TYPE
            generatedMatchKeyMapping.put('AREA_OR_CHAIN_LEVEL_BUDGET', generatedMatchKey);
        }
        else
        {
            System.debug('BUDGET LEVEL: ACCOUNT OR AREA LEVEL');
            generatedMatchKey = strYear         + ' - ' + // YEAR
                                budgetName      + ' - ' + // BUDGET NAME
                                areaCoverage    + ' - ' + // AREA
                                accountCharge   + ' - ' + // ACCOUNT CHARGE
                                '*'             + ' - ' + // CUSTOMER GROUP (*) Expectations: Customer Group Level should return "*"
                                accountLevel    + ' - ' + // ACCOUNT
                                businessUnit    + ' - ' + // BUSINESS UNIT
                                majorActivity   + ' - ' + // MAJOR ACTIVITY
                                productCategory + ' - ' + // PRODUCT CATEGORY
                                budgetType;               // BUDGET TYPE
            generatedMatchKeyMapping.put('AREA_OR_CHAIN_LEVEL_BUDGET', generatedMatchKey);
        }

        // Budget for TMG User
        // Match_Key_TMG__c will be used.
        tmgMatchKey        = budgetName      + ' - ' + // BUDGET NAME
                             accountCharge   + ' - ' + // ACCOUNT CHARGE
                             businessUnit    + ' - ' + // BUSINESS UNIT
                             majorActivity   + ' - ' + // MAJOR ACTIVITY
                             productCategory + ' - ' + // PRODUCT CATEGORY
                             budgetType;               // BUDGET TYPE

        wetMarketMatchKey = strYear         + ' - ' + // YEAR
                            budgetName      + ' - ' + // BUDGET NAME
                            areaCoverage    + ' - ' + // AREA
                            accountCharge   + ' - ' + // ACCOUNT CHARGE
                            '*'             + ' - ' + // CUSTOMER GROUP (*) Expectations: Customer Group Level should return "*"
                            '*'             + ' - ' + // ACCOUNT
                            businessUnit    + ' - ' + // BUSINESS UNIT
                            majorActivity   + ' - ' + // MAJOR ACTIVITY
                            productCategory + ' - ' + // PRODUCT CATEGORY
                            budgetType;               // BUDGET TYPE

        micDirectMatchKey = strYear         + ' - ' + // YEAR
                            budgetName      + ' - ' + // BUDGET NAME
                            areaCoverage    + ' - ' + // AREA
                            accountCharge   + ' - ' + // ACCOUNT CHARGE
                            '*'             + ' - ' + // CUSTOMER GROUP (*) Expectations: Customer Group Level should return "*"
                            '*'             + ' - ' + // ACCOUNT
                            businessUnit    + ' - ' + // BUSINESS UNIT
                            majorActivity   + ' - ' + // MAJOR ACTIVITY
                            productCategory + ' - ' + // PRODUCT CATEGORY
                            budgetType;               // BUDGET TYPE

        salesMatchKey =     strYear         + ' - ' + // YEAR
                            budgetName      + ' - ' + // BUDGET NAME
                            accountCharge   + ' - ' + // ACCOUNT CHARGE
                            customerGroup   + ' - ' + // CUSTOMER GROUP (*) Expectations: Customer Group Level should return "*"
                            accountLevel    + ' - ' + // ACCOUNT
                            businessUnit    + ' - ' + // BUSINESS UNIT
                            majorActivity   + ' - ' + // MAJOR ACTIVITY
                            productCategory + ' - ' + // PRODUCT CATEGORY
                            budgetType;               // BUDGET TYPE

        // For MT Sales User
        if(ePAWBudget.Customer_Group__c != null)
        {
            // Budget is Customer Group Level
            System.debug('BUDGET LEVEL: CUSTOMER GROUP');
            mtMatchKey =        strYear         + ' - ' + // YEAR
                                budgetName      + ' - ' + // BUDGET NAME
                                accountCharge   + ' - ' + // ACCOUNT CHARGE
                                customerGroup   + ' - ' + // CUSTOMER GROUP
                                '*'             + ' - ' + // ACCOUNT (*) | Expectations: Account Level should return "*"
                                businessUnit    + ' - ' + // BUSINESS UNIT
                                majorActivity   + ' - ' + // MAJOR ACTIVITY
                                productCategory + ' - ' + // PRODUCT CATEGORY
                                budgetType;               // BUDGET TYPE
            generatedMatchKeyMapping.put('MT_LEVEL_BUDGET', mtMatchKey);
        }
        else
        {
            System.debug('BUDGET LEVEL: ACCOUNT OR AREA LEVEL');
            mtMatchKey = strYear         + ' - ' + // YEAR
                                budgetName      + ' - ' + // BUDGET NAME
                                accountCharge   + ' - ' + // ACCOUNT CHARGE
                                '*'             + ' - ' + // CUSTOMER GROUP (*) Expectations: Customer Group Level should return "*"
                                accountLevel    + ' - ' + // ACCOUNT
                                businessUnit    + ' - ' + // BUSINESS UNIT
                                majorActivity   + ' - ' + // MAJOR ACTIVITY
                                productCategory + ' - ' + // PRODUCT CATEGORY
                                budgetType;               // BUDGET TYPE
            generatedMatchKeyMapping.put('MT_LEVEL_BUDGET', mtMatchKey);
        }

        generatedMatchKeyMapping.put('UNIQUE_BUDGET', matchKeyBudgetUniqueness);
        generatedMatchKeyMapping.put('TMG_LEVEL_BUDGET', tmgMatchKey);
        generatedMatchKeyMapping.put('WET_MARKET_LEVEL_BUDGET', wetMarketMatchKey);
        generatedMatchKeyMapping.put('SALES_LEVEL_BUDGET', salesMatchKey);
        generatedMatchKeyMapping.put('MT_LEVEL_BUDGET', mtMatchKey);
        generatedMatchKeyMapping.put('MIC_DIRECT_BUDGET', micDirectMatchKey);

        System.debug('>>> [SMC_FormEventTriggerHandlerBudget] generateEpawMatchKey() has been terminated. <<<');
        return generatedMatchKeyMapping;
    }
    /*
    |-----------------------------------------------------------------
    | [NOTHING FOLLOWS] CUSTOM TRIGGER METHODS
    |-----------------------------------------------------------------
    */
}