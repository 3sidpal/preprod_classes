public class OrderTrigger_Returns_Handler {

// Author: slcabali@gmail.com
// Description: BeforeInsert and Update of Returns
// Created Date: 2018-06-28

    public static void updateSIFViaInvoice(List<Order__c> newOrderItems) {
        
        //GAAC: Excluded for future use part of DI return process
        Set<String> qOrder = new Set<String>();
        Set<String> old = new Set<String>();
        Set<Id> accId = new Set<Id>(); 
              
        for(Order__c o : newOrderItems) {       
            if(o.Sales_Information_Form__c == null && o.Invoice_No__c != null && o.RecordTypeId == Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Return Order Form').getRecordTypeId()) {
                qOrder.add(o.Invoice_No__c);
                accId.add(o.Account__c);               
                old.add(o.Id);
            }
        }       
        
        Map<String,String> lkupOrder = new Map<String,String>();
        for(Order__c oList : [Select Id, Invoice_No__c, Account__r.Distributor__c from Order__c where Invoice_No__c IN: qOrder AND Account__c IN: accId AND RecordType.Name = 'Actual Sales']) {
            lkupOrder.put(oList.Invoice_No__c, oList.Id);
        }
              
        for(Order__c o : newOrderItems) {
                    
            if(old.contains(o.Id)) {
                o.Sales_Information_Form__c = lkupOrder.get(o.Invoice_No__c);
            }
        }
    }
}