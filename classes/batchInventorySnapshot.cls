global class batchInventorySnapshot implements Database.Batchable<sObject>, Schedulable{

    global void execute(SchedulableContext SC) {
        Database.executeBatch(new batchInventorySnapshot(), 2000);
    }

    global Database.querylocator start(Database.BatchableContext BC){
        String queryString = sObjectUtility_Library.buildQuery(SobjectType.Inventory_Line__c);
        queryString += ' WHERE Depleted__c = false';
        
        /**return Database.getQueryLocator([SELECT Age1__c,Age__c,Alt_UOM__c,Batch_Code__c,Begining_Balance__c,CreatedById,
                                       CreatedDate,Date_Manufactured__c,Date_Validated__c,Depleted__c,Ending_Balance__c,
                                       Incoming_Quantity__c,Inventory_Item__c,Inventory_Rate_Accuracy__c,IsDeleted,
                                       LastModifiedById,LastModifiedDate,MAA__c,Name,OwnerId,Product__c,Safety_Stock_Level__c,
                                       Shelf_Life_Days__c,SIF_Quantity__c,SystemModstamp,UOM__c,Updated__c,
                                       Validated_Incoming_Quantity__c,Validated_Quantity__c,Validate_Incoming_Quantity__c,
                                       Validate_Quantity__c, UOM_Conversion_Name__c, ALT_UOM_Name__c,Weight_Quantity__c, Weight_UOM__c, Weight_UOM_Name__c, Inventory_Item_Name__c, Inventory_Line_ID__c, Inventory_Item_ID__c, Account__c, LineKey__c, SIV_Value__c       
                                       FROM Inventory_Line__c
                                       Where  Depleted__c = false]);**/
        return Database.getQueryLocator(queryString);
    }//end method  
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Monthly_Inventory_Snapshot__c> customSettings = Monthly_Inventory_Snapshot__c.getall().values();
        List<Monthly_Inventory__c> snapshot = new List<Monthly_Inventory__c>();
        for(sObject s : scope){
            Inventory_Line__c line = (Inventory_Line__c)s;
            Monthly_Inventory__c inv = new Monthly_Inventory__c();
            for (Monthly_Inventory_Snapshot__c cs : customSettings){
                inv.put(cs.Target_Field__c, line.get(cs.Source_Field__c));        
            }
            snapshot.add(inv);          
        }  
        
        if (snapshot.size() > 0){
            insert snapshot;
        }
      
            
   }  // end method
   
   global void finish(Database.BatchableContext BC){     
   }
   
}