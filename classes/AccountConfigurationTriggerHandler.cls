public class AccountConfigurationTriggerHandler {

    public static String DAY_OF_WEEK = [SELECT GFS_Editable_Day__c
                                          FROM DIL_Variable__mdt
                                         WHERE DeveloperName = 'DIL_CONSTANT'].get(0).GFS_Editable_Day__c;

    public void computeMonthData(List<Account_Configuration__c> accConfigList) {

        List<String> monthList                       = new List<String>();
        List<String> yearList                        = new List<String>();
        List<Id> ownerList                           = new List<Id>();
        Map<Id, Month__c> taskIdVsMonthObjMap        = new Map<Id, Month__c>();
        Map<Id, Month__c> taskIdVsMonthObjUpdatedMap = new Map<Id, Month__c>();
        List<Month__c> monthCreateList               = new List<Month__c>();
        Map<Id, String> relatedToIdVsNameMap         = new Map<Id, String>();
        //Map<Id, Month__c> monthUpdateMap            = new Map<Id, Month__c>();


        // Iterate a for loop to create the map of Month__c object whether present in org or not.
        for(Account_Configuration__c accConfigItr : accConfigList) {

            if(  null == accConfigItr.StartDateTime__c
              || null == accConfigItr.User__c
              ) {
                continue;
            }

            String monthVar = TaskTriggerHandler.getStrMonth(accConfigItr.StartDateTime__c.month());
            String yearVar  = String.valueOf(accConfigItr.StartDateTime__c.year());
            Id ownerId      = accConfigItr.User__c;

            monthList.add(monthVar);
            yearList.add(yearVar);
            ownerList.add(ownerId);

            taskIdVsMonthObjMap.put( accConfigItr.Id
                                   , new Month__c(      Name = monthVar
                                                             + '-'
                                                             + yearVar
                                                        //+ '-'
                                                        //+ taskItr.User__r.Name
                                                  , Month__c = monthVar
                                                  ,  Year__c = yearVar
                                                  ,  OwnerId = ownerId
                                                  )
                                   );


        } // End of for.

        if(taskIdVsMonthObjMap.isEmpty()) {
            return;
        }

        // Create a list to get all the existing Month__c from the org.
        List<Month__c> monthQueryList = [SELECT Id
                                              , Month__c
                                              , Year__c
                                              , OwnerId
                                           FROM Month__c
                                          WHERE Month__c IN :monthList
                                            AND Year__c  IN :yearList
                                            AND OwnerId  IN :ownerList
                                        ];


        // Iterate for loop to map existing Month__c to the Tasks.
        // If Match not found the create a list of Month__c to be inserted to be mapped later.
        for(Id taskIdItr : taskIdVsMonthObjMap.keySet()) {

            Boolean matchFound = false;

            for(Month__c monthItr : monthQueryList) {

                if(  taskIdVsMonthObjMap.get(taskIdItr).Month__c == monthItr.Month__c
                  && taskIdVsMonthObjMap.get(taskIdItr).Year__c  == monthItr.Year__c
                  && taskIdVsMonthObjMap.get(taskIdItr).OwnerId  == monthItr.OwnerId
                  ) {

                    taskIdVsMonthObjUpdatedMap.put(taskIdItr, monthItr);
                    matchFound = true;

                }

            }

            if(!matchFound) {

                monthCreateList.add(taskIdVsMonthObjMap.get(taskIdItr));

            }

        }

        // If monthCreateList is not empty then insert the list and start the same mapping procedure as above.
        if(!monthCreateList.isEmpty()) {
            //Database.insert(monthCreateList, false);
            System.debug('---104---' + monthCreateList);
            insert monthCreateList;

            for(Month__c monthItr : monthCreateList) {

                for(Id taskIdItr : taskIdVsMonthObjMap.keySet()) {

                    if(  taskIdVsMonthObjMap.get(taskIdItr).Month__c == monthItr.Month__c
                      && taskIdVsMonthObjMap.get(taskIdItr).Year__c  == monthItr.Year__c
                      && taskIdVsMonthObjMap.get(taskIdItr).OwnerId  == monthItr.OwnerId
                      ) {

                        taskIdVsMonthObjUpdatedMap.put(taskIdItr, monthItr);

                    }

                }

            }

        }

        // Update the month lookup and update.
        for(Account_Configuration__c accConfigItr : accConfigList) {

            if(taskIdVsMonthObjUpdatedMap.containsKey(accConfigItr.Id)) {

                accConfigItr.Month__c = taskIdVsMonthObjUpdatedMap.get(accConfigItr.Id).Id;
            }
        }

        /******************************** Add error if month of task is submitteed **************************/
        Map<Id, Id> monthVsUserIdMap = new Map<Id, Id>();
        Map<Id, Month__c> monthStatusMap = new Map<Id, Month__c>();
        for(Account_Configuration__c accConfigItr : accConfigList) {
            if(accConfigItr.Month__c != null) {
                monthVsUserIdMap.put(accConfigItr.Month__c, null);
                monthStatusMap.put(accConfigItr.Month__c, null);
            }
        }

        for(Month__c monthItr : [SELECT Id, Status__c, OwnerId FROM Month__c WHERE Id IN :monthStatusMap.keySet()]) {
            monthVsUserIdMap.put(monthItr.Id,monthItr.OwnerId);
            monthStatusMap.put(monthItr.Id, monthItr);
        }

        Map<Id,User> mapUserIdToBu = new Map<Id,User>();
        for(User userItr : [SELECT Id, Business_Unit__c FROm User Where Id IN :monthVsUserIdMap.values()]) {
            mapUserIdToBu.put(userItr.Id, userItr);
        }

        for(Account_Configuration__c accConfigItr : accConfigList) {
            if(!StaticResources.changesMadeByWebService) {
                if(  monthStatusMap.containsKey(accConfigItr.Month__c)
                  && monthStatusMap.get(accConfigItr.Month__c).Status__c == 'Submitted'
                  ){

                   /*Id userId = monthVsUserIdMap.get(accConfigItr.Month__c);
                   User userVar = mapUserIdToBu.get(userId);
                   if(userVar.) {*/
                    accConfigItr.addError('Cannot create a task on Submitted Month !!');
                   //}
                }

                Id userId = monthVsUserIdMap.get(accConfigItr.Month__c);
                User userVar = mapUserIdToBu.get(userId);

                if(  userVar.Business_Unit__c != 'GFS'
                  && monthStatusMap.containsKey(accConfigItr.Month__c)
                  && monthStatusMap.get(accConfigItr.Month__c).Status__c == 'Approved'
                  ) {
                    accConfigItr.addError('Cannot create a task on Approved Month !!');
                }

                if(  monthStatusMap.containsKey(accConfigItr.Month__c)
                  && monthStatusMap.get(accConfigItr.Month__c).Status__c == 'Approved'
                  && userVar.Business_Unit__c == 'GFS'
                  && System.now().format('EEEE') != DAY_OF_WEEK
                  ) {
                    accConfigItr.addError('Can only edit on ' + DAY_OF_WEEK);
                }
            }
        }

    }


    public void computeOutletsCovered(List<Account_Configuration__c> accConfigList) {
        Set<Id> monthIdSet = new Set<Id>();
        Map<Id, List<Account_Configuration__c>> mapConfigMonth = new Map<Id, List<Account_Configuration__c>>();
        for(Account_Configuration__c accConfigObj : accConfigList) {
            monthIdSet.add(accConfigObj.Month__c);
        }

        for(Account_Configuration__c accConfigObj : [SELECT Id
                                                       , Accounts_Created__c
                                                       , StartDateTime__c
                                                       , Month__c
                                                    FROM Account_Configuration__c
                                                   WHERE Month__c IN: monthIdSet
                                                     AND Status__c != 'Cancelled']) {
            if(mapConfigMonth.containsKey(accConfigObj.Month__c)) {
                mapConfigMonth.get(accConfigObj.Month__c).add(accConfigObj);
            }
            else {
                mapConfigMonth.put(accConfigObj.Month__c, new List<Account_Configuration__c> {accConfigObj});
            }
        }

        /*List<Month__c> monthList = [SELECT Id
                                         , Outlets_Covered__c
                                         , Total_Work_Days_Completed__c
                                      FROM Month__c
                                     WHERE Id IN: mapConfigMonth.keySet()];

        for(Month__c monthObj : monthList) {
            Integer outletsCovered = 0;
            if(mapConfigMonth.containsKey(monthObj.Id)) {
                for(Account_Configuration__c accConfigObj : mapConfigMonth.get(monthObj.Id)) {
                    Date startDate = date.newinstance( accConfigObj.StartDateTime__c.year()
                                                     , accConfigObj.StartDateTime__c.month()
                                                     , accConfigObj.StartDateTime__c.day());
                    if(accConfigObj.Accounts_Created__c != null) {
                        outletsCovered += Integer.valueOf(accConfigObj.Accounts_Created__c);
                    }
                }
                monthObj.Outlets_Covered__c = outletsCovered;
            }
        }
        update monthList;*/


    }

    public void computeWorkHours(List<Account_Configuration__c> accConfigList) {
        Set<Id> monthIdSet = new Set<Id>();
        Map<Id, List<Task>> monthTaskMap = new Map<Id, List<Task>>();
        List<Task> taskToUpdate = new List<Task>();
        Map<Id, List<Account_Configuration__c>> mapConfigMonth = new Map<Id, List<Account_Configuration__c>>();
        for(Account_Configuration__c accConfigObj : accConfigList) {
            monthIdSet.add(accConfigObj.Month__c);
        }

        for(Account_Configuration__c accConfigObj : [SELECT Id
                                                       , Accounts_Created__c
                                                       , Month__c
                                                       , StartDateTime__c
                                                       , Status__c
                                                    FROM Account_Configuration__c
                                                   WHERE Month__c IN: monthIdSet]) {
            if(mapConfigMonth.containsKey(accConfigObj.Month__c)) {
                mapConfigMonth.get(accConfigObj.Month__c).add(accConfigObj);
            }
            else {
                mapConfigMonth.put(accConfigObj.Month__c, new List<Account_Configuration__c> {accConfigObj});
            }
        }

        Set<Date> configUniqueDateSet = new Set<Date>();
        Set<Date> configFieldWorkDateSet = new Set<Date>();
        List<Month__c> monthList = [SELECT Id
                                         , Outlets_Covered__c
                                         , Total_Work_Days_Completed__c
                                         , Field_Work_Days__c
                                         , Leave_Days__c
                                         , Total_Leave_Hours__c
                                         , Total_Work_Hours_Completed__c
                                      FROM Month__c
                                     WHERE Id IN: mapConfigMonth.keySet()];
        for(Task taskObj : [SELECT Id
                                 , StartDateTime__c
                                 , Work_Type__c
                                 , Month__c
                                 , Status
                              FROM Task
                             WHERE Month__c IN: mapConfigMonth.keySet()
                               AND StartDateTime__c != null
                               AND Status != 'Cancelled']) {
            System.debug('--204--' + taskObj);
            Date startDate = date.newinstance( taskObj.StartDateTime__c.year()
                                             , taskObj.StartDateTime__c.month()
                                             , taskObj.StartDateTime__c.day());
            configUniqueDateSet.add(startDate);
            if(taskObj != null) {
                if(monthTaskMap.containsKey(taskObj.Month__c)) {
                    monthTaskMap.get(taskObj.Month__c).add(taskObj);
                }
                else {
                    monthTaskMap.put(taskObj.Month__c, new List<Task> {taskObj});
                }
            }

            if(taskObj.Work_Type__c == 'Field work') {
                configFieldWorkDateSet.add(startDate);
            }
        }
        for(Month__c monthObj : monthList) {
            if(mapConfigMonth.containsKey(monthObj.Id)) {
                for(Account_Configuration__c accConfigObj : mapConfigMonth.get(monthObj.Id)) {
                    if(accConfigObj.Status__c != 'Cancelled') {
                        Date startDate = date.newinstance( accConfigObj.StartDateTime__c.year()
                                                     , accConfigObj.StartDateTime__c.month()
                                                     , accConfigObj.StartDateTime__c.day());
                        configUniqueDateSet.add(startDate);
                        configFieldWorkDateSet.add(startDate);
                    }

                }
                monthObj.Total_Work_Days_Completed__c = configUniqueDateSet.size();
                monthObj.Field_Work_Days__c = configFieldWorkDateSet.size();
                //System.debug('-----------'+monthTaskMap.get(monthObj.Id).size());
                System.debug('-----------'+monthTaskMap);
                if(monthTaskMap.get(monthObj.Id) == null) {
                    monthObj.Leave_Days__c = 0;
                    monthObj.Total_Leave_Hours__c = 0;
                    monthObj.Total_Work_Hours_Completed__c = 0;
                }
            }
        }
        update monthList;

    }



}