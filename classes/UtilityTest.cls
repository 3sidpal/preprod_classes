@isTest
public class UtilityTest {
    /* Method to test if correct record Id is fetched when we pass Object type and Record Type Label Values to the method getRecordTypeId() */
    public static testMethod void getRecordTypeIdTestMethod() {
        String objectType = 'contact';
        String recordTypeLabel = 'Distributor Personnel';
        Utility utilityObj = new Utility();
        Test.startTest();
        Id expectedResult = Schema.getGlobalDescribe().get(objectType).getDescribe().getRecordTypeInfosByName().get(recordTypeLabel).getRecordTypeId();
        Id actualResult = utilityObj.getRecordTypeId(objectType,recordTypeLabel);
        Test.stopTest();
        System.assertEquals(expectedResult,actualResult);
    }

    /* Method to test if null value is returned when we pass Record Type Label Value which is not avaiable to the method getRecordTypeId() */
    public static testMethod void noRecordTypeTestMethod() {
        String objectType = 'contact';
        String recordTypeLabel = 'Distributor Single';
        Utility utilityObj = new Utility();
        Test.startTest();
        Id actualResult = utilityObj.getRecordTypeId(objectType,recordTypeLabel);
        Test.stopTest();
        System.assertEquals(null,actualResult);
    }

    /* Method to test if correct picklist options are returned by the method getPickValues() */
    public static testMethod void getPickValuesTestMethod() {
        Sobject objectType = new Contact();
        String fieldName = 'Classification__c';
        String firstVal = 'CLassifications';
        Utility utilityObj = new Utility();
        List<SelectOption> expectedOptionsList = new List<SelectOption>();
        Test.startTest();
        if (FirstVal != null) {
            expectedOptionsList.add(new selectOption(FirstVal, FirstVal));
        }
        Schema.DescribeFieldResult fieldResult = Contact.Classification__c.getDescribe();
        List<Schema.PicklistEntry> piclistValues = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry piclistValue : piclistValues){
            expectedOptionsList.add(new selectOption(piclistValue.getLabel(), piclistValue.getValue()));
        }
        List<SelectOption> actualOptionsList = utilityObj.getPickValues(objectType,FieldName,FirstVal);
        Test.stopTest();
        System.assertEquals(expectedOptionsList,actualOptionsList);
    }

    /* Method to test if correct picklist options are returned by the method getPickValues() if first value is not provided*/
    public static testMethod void firstValueNullTestMethod() {
        Sobject objectType = new Contact();
        String fieldName = 'Classification__c';
        String firstVal;
        Utility utilityObj = new Utility();
        List<SelectOption> expectedOptionsList = new List<SelectOption>();
        Test.startTest();
        if (FirstVal != null) {
            expectedOptionsList.add(new selectOption(FirstVal, FirstVal));
        }
        Schema.DescribeFieldResult fieldResult =  Contact.Classification__c.getDescribe();
        List<Schema.PicklistEntry> piclistValues = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry piclistValue : piclistValues){
            expectedOptionsList.add(new selectOption(piclistValue.getLabel(), piclistValue.getValue()));
        }
        List<SelectOption> actualOptionsList = utilityObj.getPickValues(objectType,FieldName,FirstVal);
        Test.stopTest();
        System.assertEquals(expectedOptionsList,actualOptionsList);
    }

}