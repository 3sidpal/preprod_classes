@isTest(seeAllData=true)

public class TestOrderProductCX {
    /*
    static testMethod void unitTestOrderProduct_InactiveAccount_AND_withoutPCBCX() {
    
    TestDataFactory data = new TestDataFactory();     

    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('Case','Case'));

    //Create multiple custom products TestDataFactory.createTestAccount(Account Count, Active?, with province,barangay,city? RecordType Name)
    //Record Types: Distributor/ Direct AND Distributor Customer
    Account[] distAcnts = data.createTestAccount(1, True, True, 'Distributor/ Direct', null);
    Account[] actAcnts = data.createTestAccount(1, True, True, 'Distributor Customer', distAcnts[0].Id);      
    
    //Create multiple custom products TestDataFactory.createTestProducts(Product Count, Brand Name, Category Name)
    Custom_Product__c[] prods = data.createTestProducts(4);
    
    Product_Assignment__c[] prodAsn = data.testAssignProd(prods, actAcnts[0].Id);
    
    Order__c[] nOrder = data.createTestOrder(1, actAcnts[0].id);
    Order__c nOrd = new Order__c();
       
    Order_Item__c[] nOrdItem = data.createTestOrderItem(prods, nOrder[0].Id);
    
    Pricing_Condition_Data__c[] npCondData = data.createTestPricingCondition(prods, actAcnts[0].Id, distAcnts[0].Id);
    
    OrderProductCX.resultWrapper rwpr = new OrderProductCX.resultWrapper(true, prods[0], 1, 100.00, options, 'Case', 
                            '1.00', '', true, '1', '1.00', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
    List<OrderProductCX.resultWrapper> listrwpr = new List<OrderProductCX.resultWrapper>();
    listrwpr.add(rwpr);
    
    PageReference pageRef = Page.OrderProductPage;
    Test.setCurrentPage(pageRef);

    Test.StartTest(); 
    
        OrderProductCX.oiWrapper oiW = new OrderProductCX.oiWrapper(nOrdItem[0], 1, 1.00, '5');
    
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(nOrd);
        OrderProductCX controller = new OrderProductCX(sc);
        controller.searchResultWrapper = listrwpr;
        
        controller.TheOrder = nOrder[0];
        controller.filterByName = 'Test';
        controller.filterByBU = 'Test';
        controller.filterBySKU = 'Test';
        controller.filterByBrand = 'Test';
        controller.filterByCategory = 'Test';
        controller.searchOrderItem();
        controller.getOrderItemRecords();
        controller.proceedAction();
        PageReference pageRefRR = Page.OrderProductPage;
        Test.setCurrentPage(pageRefRR);
        ApexPages.currentPage().getParameters().put('rowToBeDeleted', '0');
        controller.removeRow();
        controller.backToPage1();
        controller.proceedAction();
        controller.recomputeTotalPriceSearchResult();
        controller.recomputeTotalPriceSelected();
        listrwpr[0].uom1 = 'Bag';
        controller.searchResultWrapper = listrwpr;
        controller.recomputeTotalPriceSearchResult();
        listrwpr[0].uom2 = '25';
        controller.searchResultWrapper = listrwpr;
        controller.recomputeTotalPriceSearchResult();
        listrwpr[0].uom3 = 'KG';
        controller.searchResultWrapper = listrwpr;
        controller.recomputeTotalPriceSearchResult();
        listrwpr[0].uom4 = 'PC';
        controller.searchResultWrapper = listrwpr;
        
        controller.recomputeTotalPriceSearchResult();
        controller.recomputeTotalPriceSelected();
        controller.saveRecord();
        
    Test.StopTest();
    }


    static testMethod void unitTestOrderProductCX() {
    
    TestDataFactory data = new TestDataFactory();     

    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('Case','Case'));

    //Create multiple custom products TestDataFactory.createTestAccount(Account Count, Active?, with province,barangay,city? RecordType Name)
    //Record Types: Distributor/ Direct AND Distributor Customer
    Account[] distAcnts = data.createTestAccount(1, True, True, 'Distributor/ Direct', null);
    Account[] actAcnts = data.createTestAccount(1, True, True, 'Distributor Customer', distAcnts[0].Id);  
    Account[] woutPCBAcnts = data.createTestAccount(1, True, False, 'Distributor/ Direct', null);
    Account[] inactAcnts = data.createTestAccount(1, False, True, 'Distributor/ Direct', null);      
    
    //Create multiple custom products TestDataFactory.createTestProducts(Product Count, Brand Name, Category Name)
    Custom_Product__c[] prods = data.createTestProducts(4);
    
    Product_Assignment__c[] prodAsn = data.testAssignProd(prods, actAcnts[0].Id);
    
    Order__c[] nOrder = data.createTestOrder(1, actAcnts[0].id);
    Order__c[] nOrder1 = data.createTestOrder(1, inactAcnts[0].id);
    Order__c[] nOrder2 = data.createTestOrder(1, woutPCBAcnts[0].id);
    Order__c nOrd = new Order__c();
        
    Order_Item__c[] nOrdItem = data.createTestOrderItem(prods, nOrder[0].Id);
    
    Pricing_Condition_Data__c[] npCondData = data.createTestPricingCondition(prods, actAcnts[0].Id, distAcnts[0].Id);
           
    PageReference pageRef = Page.OrderProductPage;
    Test.setCurrentPage(pageRef);

    Test.StartTest(); 
        
        ApexPages.Standardcontroller sc0 = new ApexPages.Standardcontroller(nOrder[0]);
        OrderProductCX controller0 = new OrderProductCX(sc0);
        
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(nOrder1[0]);
        OrderProductCX controller1 = new OrderProductCX(sc1);

        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(nOrder2[0]);
        OrderProductCX controller2 = new OrderProductCX(sc2);
    
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(nOrd);
        OrderProductCX controller = new OrderProductCX(sc);

        controller.TheOrder = nOrder[0];
        controller.searchOrderItem();
        controller.filterByName = 'Test';
        controller.filterByBU = 'Test';
        controller.filterByBrand = 'Test';
        controller.filterByCategory = 'Test';
        controller.resetFilter();
        OrderProductCX.oiWrapper oiW = new OrderProductCX.oiWrapper(nOrdItem[0], 1, 1.00, '5');
        OrderProductCX.resultWrapper rwpr = new OrderProductCX.resultWrapper(true, prods[0], 1, 100.00, options, 'Case', 
                            '1.00', '', true, '1', '1.00', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        OrderProductCX.resultWrapper rwpr1 = new OrderProductCX.resultWrapper(true, prods[1], 1, 100.00, options, 'Case', 
                            '', '1', true, '1', '', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        OrderProductCX.resultWrapper rwpr2 = new OrderProductCX.resultWrapper(true, prods[1], 1, 100.00, options, 'Case', 
                            '1.00', '', true, '', '', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        List<OrderProductCX.resultWrapper> listrwpr = new List<OrderProductCX.resultWrapper>();
        listrwpr.add(rwpr);
        listrwpr.add(rwpr1);
        
        Map<id, OrderProductCX.oiWrapper> mapoiw = new Map<id, OrderProductCX.oiWrapper>();
        //Map<Id, Discount__c> mapDiscount0 = new Map<Id, Discount__c>();
        //mapDiscount0.put(prods[0].id, discs[0]);
        mapoiw.put(nOrdItem[0].id, oiW);
        controller.mapUpdateOI = new Map<Id, Order_Item__c>();
        controller.mapUpdateOI.put(prods[0].id, nOrdItem[0]);
        controller.mapUpdateOI.put(prods[1].id, nOrdItem[1]);
        //controller.searchOrderItem()0;
        
        controller.searchResultWrapper = listrwpr;
        
        controller.proceedAction();
        controller.selectedProductWrapper = listrwpr;
        controller.updateOrderItems();
        //controller.saveRecord();
        
        //controller 0
        controller0.TheOrder = nOrder[0];
        controller0.searchOrderItem();
        
        nOrder1[0].id = null;
        
        controller0.TheOrder = nOrder1[0];
        controller0.searchOrderItem();
        PageReference pageRefSA = Page.OrderProductPage;
        Test.setCurrentPage(pageRefSA);
        ApexPages.currentPage().getParameters().put('SaveAction', 'savenew');
        controller0.saveRecord();
        
        controller0.TheOrder = nOrder[0];
        controller0.searchOrderItem();
        
        nOrder1[0].id = null;
        
        controller0.TheOrder = nOrder1[0];
        controller0.searchOrderItem();
        PageReference pageRefSO = Page.OrderProductPage;
        Test.setCurrentPage(pageRefSO);
        ApexPages.currentPage().getParameters().put('SaveAction', 'saveOnly');
        controller0.saveRecord();
        
        PageReference pageRefSOb = Page.OrderProductPage;
        Test.setCurrentPage(pageRefSOb);
        ApexPages.currentPage().getParameters().put('SaveAction', '');
        controller0.saveRecord();
        
        controller0.TheOrder = nOrder[0];
        controller0.searchOrderItem();
        
        //nOrder1[0].id = null;
        
        controller0.TheOrder = nOrder1[0];
        controller0.searchOrderItem();
        
        controller0.saveRecord();
        
    Test.StopTest();
    
    }
    static testMethod void unitTestOrderProductCX_LP() {
    
    TestDataFactory data = new TestDataFactory();     

    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('Case','Case'));

    //Create multiple custom products TestDataFactory.createTestAccount(Account Count, Active?, with province,barangay,city? RecordType Name)
    //Record Types: Distributor/ Direct AND Distributor Customer
    Account[] distAcnts = data.createTestAccount(1, True, True, 'Distributor/ Direct', null);
    Account[] cusAcnts = data.createTestAccount(1, True, True, 'Distributor Customer', distAcnts[0].Id);  
    
    Custom_Product__c[] prods = data.createTestProducts(4);
    
    Product_Assignment__c[] disprodAsn = data.testAssignProd(prods, distAcnts[0].Id);
    Product_Assignment__c[] cusprodAsn = data.testAssignProd(prods, cusAcnts[0].Id);
    
    Pricing_Condition_Data__c[] npCondData = data.createTestPricingCondition(prods, distAcnts[0].Id, distAcnts[0].Id);
    
    Pricing_Sequence_Condition__c[] pscData = data.createTestPricingSeqCondition(3, 'Net', cusAcnts[0].id);
    Discount__c[] discs = data.createTestDiscount(2, cusAcnts[0].Id, prods[0]);
    Order__c[] nOrder = data.createTestOrder(1, cusAcnts[0].id);
    Order__c nOrd = new Order__c();
    
    Order_Item__c[] nOrdItem = data.createTestOrderItem(prods, nOrder[0].Id);
    
    PageReference pageRef = Page.OrderProductPage;
    Test.setCurrentPage(pageRef);
    
    Test.StartTest(); 
        ApexPages.Standardcontroller sc0 = new ApexPages.Standardcontroller(nOrder[0]);
        OrderProductCX controller0 = new OrderProductCX(sc0);
        
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(nOrder[0]);
        OrderProductCX controller1 = new OrderProductCX(sc1);
        
        controller0.TheOrder = nOrder[0];
        controller0.searchOrderItem();
        controller0.filterByName = 'Test';
        controller0.filterByBU = 'Test';
        controller0.filterByBrand = 'Test';
        controller0.filterByCategory = 'Test';
        controller0.resetFilter();
        controller0.listSequenceDiscount = new List<Pricing_Sequence_Condition__c>();
        controller0.listSequenceDiscount = pscData;
        
        controller1.TheOrder = nOrder[0];
        controller1.searchOrderItem();
        controller1.filterByName = 'Test';
        controller1.filterByBU = 'Test';
        controller1.filterByBrand = 'Test';
        controller1.filterByCategory = 'Test';
        controller1.resetFilter();
        controller1.listSequenceDiscount = new List<Pricing_Sequence_Condition__c>();
        controller1.listSequenceDiscount = pscData;
        OrderProductCX.oiWrapper oiW = new OrderProductCX.oiWrapper(nOrdItem[0], 1, 1.00, '5');
        OrderProductCX.resultWrapper rwpr = new OrderProductCX.resultWrapper(true, prods[0], 1, 100.00, options, 'Case', 
                            '1.00', '1', true, '1', '', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        OrderProductCX.resultWrapper rwpr1 = new OrderProductCX.resultWrapper(true, prods[0], 0, 100.00, options, 'Case', 
                            null, '1', true, '1', '1.00', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        //Controller0
        List<OrderProductCX.resultWrapper> listrwpr = new List<OrderProductCX.resultWrapper>();
        listrwpr.add(rwpr);
        
        Map<id, OrderProductCX.oiWrapper> mapoiw = new Map<id, OrderProductCX.oiWrapper>();
        mapoiw.put(nOrdItem[0].id, oiW);
        controller0.mapUpdateOI = new Map<Id, Order_Item__c>();
        controller0.mapUpdateOI.put(prods[0].id, nOrdItem[0]);
        controller0.mapUpdateOI.put(prods[1].id, nOrdItem[1]);
        controller0.searchResultWrapper = listrwpr;
        controller0.selectedProductWrapper = listrwpr;
        controller0.proceedAction();
        controller0.saveRecord();
        //Controller1
        List<OrderProductCX.resultWrapper> listrwpr1 = new List<OrderProductCX.resultWrapper>();
        listrwpr1.add(rwpr1);
        
        Map<id, OrderProductCX.oiWrapper> mapoiw1 = new Map<id, OrderProductCX.oiWrapper>();
        mapoiw1.put(nOrdItem[0].id, oiW);
        controller1.mapUpdateOI = new Map<Id, Order_Item__c>();
        controller1.mapUpdateOI.put(prods[0].id, nOrdItem[0]);
        controller1.mapUpdateOI.put(prods[1].id, nOrdItem[1]);
        controller1.searchResultWrapper = listrwpr1;
        controller1.selectedProductWrapper = listrwpr1;
        controller1.proceedAction();
        controller1.backToPage1();
        controller1.searchResultWrapper = listrwpr1;
        controller1.selectedProductWrapper = listrwpr1;
        controller1.proceedAction();
        PageReference pageRefpsc = Page.OrderProductPage;
        Test.setCurrentPage(pageRefpsc);
        ApexPages.currentPage().getParameters().put('SaveAction', '');
        controller1.saveRecord();
        
    Test.StopTest();
    
    }
    
    static testMethod void unitTestOrderProductCX_Else() {
    
        TestDataFactory data = new TestDataFactory();     

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Case','Case'));
    
        Account[] distAcnts = data.createTestAccount(1, True, True, 'Distributor/ Direct', null);
        Account[] cusAcnts = data.createTestAccount(1, True, True, 'Distributor Customer', distAcnts[0].Id);  
        
        Custom_Product__c[] prods = data.createTestProducts(4);
        
        Product_Assignment__c[] disprodAsn = data.testAssignProd(prods, distAcnts[0].Id);
        Product_Assignment__c[] cusprodAsn = data.testAssignProd(prods, cusAcnts[0].Id);
        
        Order__c[] nOrder = data.createTestOrder(1, cusAcnts[0].id);
        Order__c nOrd = new Order__c();
        
        Order_Item__c[] nOrdItem = data.createTestOrderItem(prods, nOrder[0].Id);
        
        PageReference pageRef = Page.OrderProductPage;
        Test.setCurrentPage(pageRef);
        
        Test.StartTest(); 
        ApexPages.Standardcontroller sc0 = new ApexPages.Standardcontroller(nOrder[0]);
        OrderProductCX controller0 = new OrderProductCX(sc0);
        
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(nOrder[0]);
        OrderProductCX controller1 = new OrderProductCX(sc1);
        
        controller0.TheOrder = nOrder[0];
        controller0.searchOrderItem();
        controller0.filterByName = 'Test';
        controller0.filterByBU = 'Test';
        controller0.filterByBrand = 'Test';
        controller0.filterByCategory = 'Test';
        controller0.resetFilter();
        
        OrderProductCX.oiWrapper oiW = new OrderProductCX.oiWrapper(nOrdItem[0], 1, 1.00, '5');
        OrderProductCX.resultWrapper rwpr = new OrderProductCX.resultWrapper(true, prods[0], 1, 100.00, options, 'Case', 
                            '1.00', '1', true, '1', '', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        OrderProductCX.resultWrapper rwpr1 = new OrderProductCX.resultWrapper(true, prods[0], 0, 100.00, options, 'Case', 
                            null, '1', true, '1', '1.00', '1.00', '1.00', 'Case', 'Case', 'Case', 'Case', 
                            '1.00', '1.00', '1.00', '1.00');
        //Controller0
        List<OrderProductCX.resultWrapper> listrwpr = new List<OrderProductCX.resultWrapper>();
        listrwpr.add(rwpr);
        
        Map<id, OrderProductCX.oiWrapper> mapoiw = new Map<id, OrderProductCX.oiWrapper>();
        mapoiw.put(nOrdItem[0].id, oiW);
        controller0.mapUpdateOI = new Map<Id, Order_Item__c>();
        controller0.mapUpdateOI.put(prods[0].id, nOrdItem[0]);
        controller0.mapUpdateOI.put(prods[1].id, nOrdItem[1]);
        controller0.searchResultWrapper = listrwpr;
        controller0.selectedProductWrapper = listrwpr;
        controller0.proceedAction();
        PageReference pageRefRR = Page.OrderProductPage;
        Test.setCurrentPage(pageRefRR);
        ApexPages.currentPage().getParameters().put('rowToBeDeleted', '2');
        controller0.removeRow();
        controller0.TheOrder = nOrder[0];
        controller0.saveRecord();
        PageReference pageRefE = Page.OrderProductPage;
        Test.setCurrentPage(pageRefE);
        ApexPages.currentPage().getParameters().put('SaveAction', 'saveOnly');
        controller0.saveRecord();
        
        PageReference pageRefE2 = Page.OrderProductPage;
        Test.setCurrentPage(pageRefE2);
        ApexPages.currentPage().getParameters().put('SaveAction', 'saveNew');
        controller0.saveRecord();
        
    Test.StopTest();
    }
    */
}